import $ from 'jquery'
$(document).ready(function () {
  // Mobile menu toggle
  if ($('.navbar-burger').length) {
    $('.navbar-burger').on('click', function () {
      var menuId = $(this).attr('data-target')
      $(this).toggleClass('is-active')
      $('#' + menuId).toggleClass('is-active')
      $('.navbar.is-light').toggleClass('is-dark-mobile')
    })
  }

  $('.navbar-item').click(function () {
    $('[class^="navbar-"]').each(function (index, el) {
      $(el).removeClass('is-active')
    })
  })
  $('.has-dropdown').hover(
    function () { $(this).addClass('is-active') },
    function () { $(this).removeClass('is-active') }
  )

  // Sidebar menu
  if ($('.sidebar').length) {
    $('.sidebar-menu > li.have-children > a').on('click', function (i) {
      i.preventDefault()
      if (!$(this).parent().hasClass('active')) {
        $('.sidebar-menu li ul').slideUp()
        $(this).next().slideToggle()
        $('.sidebar-menu li').removeClass('active')
        $(this).parent().addClass('active')
      } else {
        $(this).next().slideToggle()
        $('.sidebar-menu li').removeClass('active')
      }
    })
  }

  $(window).scroll(function () {
    if ($(window).scrollTop() > 75) {
      $('.navbar').addClass('navbar-fixed')
    } else {
      $('.navbar').removeClass('navbar-fixed')
    }
  })

  // reveal elements on scroll so animations trigger the right way
  var $window = $(window)
  var winHeightPadded = $window.height() * 1.1

  $window.on('scroll', revealOnScroll)

  function revealOnScroll () {
    var scrolled = $window.scrollTop()
    $('.revealOnScroll:not(.animated)').each(function () {
      var $this = $(this)
      var offsetTop = $this.offset().top

      if (scrolled + winHeightPadded > offsetTop) {
        if ($this.data('timeout')) {
          window.setTimeout(function () {
            $this.addClass('animated ' + $this.data('animation'))
          }, parseInt($this.data('timeout'), 10))
        } else {
          $this.addClass('animated ' + $this.data('animation'))
        }
      }
    })
  }

  // Back to Top button behaviour
  var pxShow = 400
  var scrollSpeed = 500
  $(window).scroll(function () {
    if ($(window).scrollTop() >= pxShow) {
      $('#backtotop').addClass('visible')
    } else {
      $('#backtotop').removeClass('visible')
    }
  })
  $('#backtotop a').on('click', function () {
    $('html, body').animate({
      scrollTop: 0
    }, scrollSpeed)
    return false
  })
})
