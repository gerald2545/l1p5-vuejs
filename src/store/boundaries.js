/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import carbonService from '@/services/carbonService'

function initialState () {
  return {
    activeModule: 'introduction',
    modulei: {
      isValid: false,
      erreur: ''
    },
    module: {
      isValid: false,
      isModified: false,
      erreur: ''
    },
    allBGES: [],
    bges: {
      pk: null,
      uuid: null,
      buildingsSubmitted: false,
      commutesSubmitted: false,
      devicesSubmitted: false,
      travelsSubmitted: false,
      vehiclesSubmitted: false,
      purchasesSubmitted: false,
      annee: null,
      surveyMessage: null,
      nbChercheurs: 0,
      nbEnseignants: 0,
      nbITA: 0,
      nbPostDoc: 0,
      budget: 0
    }
  }
}

const state = initialState()

const getters = {
  activeModule: state => state.activeModule,
  module: state => state.module,
  modulei: state => state.modulei,
  bges: state => state.bges,
  bgesAnnee: state => state.bges.annee,
  bgesUUID: state => state.bges.uuid,
  buildingsSubmitted: state => state.bges.buildingsSubmitted,
  commutesSubmitted: state => state.bges.commutesSubmitted,
  devicesSubmitted: state => state.bges.devicesSubmitted,
  travelsSubmitted: state => state.bges.travelsSubmitted,
  vehiclesSubmitted: state => state.bges.vehiclesSubmitted,
  purchasesSubmitted: state => state.bges.purchasesSubmitted,
  nbChercheurs: state => state.bges.nbChercheurs,
  nbEnseignants: state => state.bges.nbEnseignants,
  nbITA: state => state.bges.nbITA,
  nbPostDoc: state => state.bges.nbPostDoc,
  budget: state => state.bges.budget,
  allBGES: state => state.allBGES
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  activateBGESSurvey ({ commit }, bges) {
    return carbonService.activateBGESSurvey({ 'bges_id': state.bges.pk })
      .then(data => {
        commit('UPDATE_BGES', JSON.parse(JSON.stringify(data)))
        return data
      })
      .catch(error => {
        throw error
      })
  },
  saveBGES ({ commit, getters }, bges) {
    return carbonService.saveBGES(bges)
      .then(data => {
        let bgesAnnee = getters['allBGES'].filter(obj => parseInt(obj.annee) === bges.annee)
        // si le bges est nouveau, pas dans les allBGES, ajoute le
        if (bgesAnnee.length === 0) {
          commit('ADD_BGES', data)
        }
        commit('UPDATE_BGES', JSON.parse(JSON.stringify(data)))
        return data
      })
      .catch(error => {
        throw error
      })
  },
  deleteBGES ({ commit, getters }, bgesID) {
    return carbonService.deleteBGES({ 'bges_id': bgesID })
      .then(data => {
        commit('UPDATE_ALL_BGES', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  submitData ({ dispatch, rootGetters, commit, state }, module) {
    return carbonService.submitData({
      'bges_id': state.bges.pk,
      'module': module
    })
      .then(data => {
        commit('UPDATE_BGES', JSON.parse(JSON.stringify(data)))
        return data
      })
      .catch(error => {
        throw error
      })
  },
  getAllBGES ({ dispatch, rootGetters, commit, state }) {
    if (rootGetters['authentication/isAuthenticated']) {
      return carbonService.getAllBGES()
        .then(data => {
          commit('UPDATE_ALL_BGES', data)
          return data
        })
        .catch(error => {
          throw error
        })
    }
  },
  setCurrentBGES ({ dispatch, rootGetters, commit, state }, bgesID) {
    let bges = rootGetters['boundaries/allBGES'].filter(obj => obj.pk === bgesID)[0]
    commit('UPDATE_BGES', JSON.parse(JSON.stringify(bges)))
    return carbonService.getBGESConsommations({ 'bges_id': bges.pk })
      .then(data => {
        dispatch('vehicles/updateAll', data['vehicules'], { root: true })
        dispatch('buildings/updateAll', data['batiments'], { root: true })
        dispatch('commutes/updateAll', data['deplacements_dt'], { root: true })
        dispatch('missions/updateAll', data['missions'], { root: true })
        dispatch('computerdevices/updateAll', data['devices'], { root: true })
        dispatch('purchases/updateAll', data['purchases'], { root: true })
        return data
      })
      .catch(error => {
        throw error
      })
  },
  resetBGES ({ dispatch, rootGetters, commit, state }) {
    commit('RESET_BGES')
    dispatch('vehicles/resetState', null, { root: true })
    dispatch('buildings/resetState', null, { root: true })
    dispatch('missions/resetState', null, { root: true })
    dispatch('commutes/resetState', null, { root: true })
    dispatch('computerdevices/resetState', null, { root: true })
    dispatch('purchases/resetState', null, { root: true })
  },
  forceModulesCumputation ({ dispatch, rootGetters, commit, state }) {
    // when year modified, force calculation to get the right emissions factors
    dispatch('buildings/forceComputation', null, { root: true })
    dispatch('commutes/forceComputation', null, { root: true })
    dispatch('missions/forceComputation', null, { root: true })
    dispatch('vehicles/forceComputation', null, { root: true })
    dispatch('computerdevices/forceComputation', null, { root: true })
    dispatch('purchases/forceComputation', null, { root: true })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  UPDATE_BGES (state, bges) {
    let bgesIndex = state.allBGES.findIndex(obj => obj.pk === bges.pk)
    state.bges = bges
    state.allBGES[bgesIndex] = bges
  },
  ADD_BGES (state, bges) {
    state.allBGES.unshift(bges)
  },
  RESET_BGES (state) {
    state.bges = {
      pk: null,
      annee: null,
      nbChercheurs: 0,
      nbEnseignants: 0,
      nbITA: 0,
      nbPostDoc: 0,
      budget: 0
    }
  },
  UPDATE_YEAR (state, year) {
    state.bges.annee = year
  },
  UPDATE_MESSAGE (state, surveyMessage) {
    state.bges.surveyMessage = surveyMessage
  },
  UPDATE_NBCHERCHEURS (state, nbChercheurs) {
    state.bges.nbChercheurs = nbChercheurs
  },
  UPDATE_NBENSEIGNANTS (state, nbEnseignants) {
    state.bges.nbEnseignants = nbEnseignants
  },
  UPDATE_NBITA (state, nbITA) {
    state.bges.nbITA = nbITA
  },
  UPDATE_NBPOSTDOC (state, nbPostDoc) {
    state.bges.nbPostDoc = nbPostDoc
  },
  UPDATE_BUDGET (state, budget) {
    state.bges.budget = budget
  },
  UPDATE_ALL_BGES (state, allBGES) {
    state.allBGES = allBGES
  },
  SET_ACTIVE_MODULE (state, value) {
    state.activeModule = value
  },
  INTROISVALID (state, value) {
    state.modulei.isValid = value
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  ISMODIFIED (state, value) {
    state.module.isModified = value
  },
  ERREUR (state, value) {
    state.module.erreur = value
  },
  RESET_MODULE (state) {
    state.module.isValid = false
    state.module.isModified = false
    state.module.erreur = ''
    state.modulei.isValid = false
    state.modulei.erreur = ''
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
