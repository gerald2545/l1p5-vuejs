/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import carbonService from '@/services/carbonService'
import Commute from '@/models/Commute.js'

function initialState () {
  return {
    module: {
      isValid: false,
      isModified: false,
      erreur: '',
      computed: false
    },
    items: [],
    results: {}
  }
}

const state = initialState()

const getters = {
  module: state => state.module,
  items: state => state.items,
  hasItems: state => state.items.length > 0,
  results: state => state.results,
  computed: state => state.module.computed,
  submitOk: state => state.items.length > 0
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  add ({ commit }, commute) {
    commit('ADD', commute)
  },
  updateAll ({ commit }, commutes) {
    commit('UPDATE_ALL', commutes)
  },
  delete ({ commit }, commute) {
    commit('DELETE', commute)
  },
  deleteUndelete ({ commit }, commute) {
    commit('DELETE_UNDELETE', commute)
  },
  saveSurveyMessage ({ dispatch, rootGetters, commit, state }, surveyMessage) {
    let allData = {
      'surveyMessage': surveyMessage,
      'bges_id': rootGetters['boundaries/bges']['pk']
    }
    return carbonService.saveSurveyMessage(allData)
      .then(data => {
        return data
      })
      .catch(error => {
        throw error
      })
  },
  getBGESSurveyInfo ({ commit }, uuid) {
    return carbonService.getBGESSurveyInfo(uuid)
      .then(data => {
        return data
      })
      .catch(error => {
        throw error
      })
  },
  save ({ dispatch, rootGetters, commit, state }) {
    let deplacementsDTDatabase = []
    for (let deplacement of state.items) {
      deplacementsDTDatabase.unshift(deplacement.toDatabase())
    }
    let allData = {
      'deplacementsDT': deplacementsDTDatabase,
      'bges_id': rootGetters['boundaries/bges']['pk']
    }
    return carbonService.saveCommutes(allData)
      .then(data => {
        commit('UPDATE_ALL', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  compute ({ dispatch, rootGetters, commit, state }) {
    commit('UPDATE_RESULTS', {})
    let emissions = Commute.compute(
      state.items,
      rootGetters['user/tailleAglomeration'],
      rootGetters['boundaries/nbChercheurs'],
      rootGetters['boundaries/nbEnseignants'],
      rootGetters['boundaries/nbITA'],
      rootGetters['boundaries/nbPostDoc'],
      rootGetters['boundaries/bgesAnnee']
    )
    commit('COMPUTED', true)
    commit('UPDATE_RESULTS', emissions)
  },
  forceComputation ({ commit }) {
    commit('COMPUTED', false)
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  ADD (state, commute) {
    let cte = Commute.createFromObj(commute)
    state.items.push(cte)
  },
  UPDATE_ALL (state, commutes) {
    state.items = []
    for (let commute of commutes) {
      state.items.unshift(Commute.createFromObj(commute))
    }
  },
  DELETE (state, commute) {
    let index = null
    index = state.items.findIndex(obj => obj.seqID === commute.seqID)
    state.items.splice(index, 1)
  },
  DELETE_UNDELETE (state, commute) {
    let index = null
    index = state.items.findIndex(obj => obj.seqID === commute.seqID)
    state.items[index].deleted = !state.items[index].deleted
  },
  UPDATE_RESULTS (state, emissions) {
    state.results = emissions
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  ISMODIFIED (state, value) {
    state.module.isModified = value
    state.module.computed = false
  },
  ERREUR (state, value) {
    state.module.erreur = value
  },
  COMPUTED (state, value) {
    state.module.computed = value
  },
  RESET_MODULE (state) {
    state.module.isValid = false
    state.module.isModified = false
    state.module.erreur = ''
    state.module.computed = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
