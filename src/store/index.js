import Vue from 'vue'
import Vuex from 'vuex'

import authentication from './authentication'
import membres from './membres'
import initiatives from './initiatives'
import gesadmin from './gesadmin'
import boundaries from './boundaries'
import buildings from './buildings'
import computerdevices from './computerdevices'
import commutes from './commutes'
import missions from './missions'
import vehicles from './vehicles'
import tutelles from './tutelles'
import purchases from './purchases'
import utils from './utils'
import user from './user'
import admin from './admin'
import simulations from './simulations'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    authentication,
    membres,
    initiatives,
    gesadmin,
    boundaries,
    buildings,
    commutes,
    computerdevices,
    missions,
    vehicles,
    tutelles,
    purchases,
    utils,
    user,
    admin,
    simulations
  }
})
