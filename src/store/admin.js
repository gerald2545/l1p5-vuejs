
import carbonService from '@/services/carbonService'
import apiService from '@/services/apiService'
import userService from '@/services/userService'
import Mission from '@/models/Mission.js'
import Vehicle from '@/models/Vehicle.js'
import Building from '@/models/Building.js'
import Commute from '@/models/Commute.js'
import ComputerDevice from '@/models/ComputerDevice.js'
import Purchase from '@/models/Purchase.js'

function initialState () {
  return {
    allBGES: [],
    allInitiatives: [],
    allGDRMembres: []
  }
}

const state = initialState()

const getters = {
  allBGES: state => state.allBGES,
  allInitiatives: state => state.allInitiatives,
  allGDRMembres: state => state.allGDRMembres
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  getAllBGES ({ commit }, data) {
    return carbonService.getBGESAdmin(data)
      .then(allBGES => {
        commit('SET_ALL_BGES', allBGES)
        return allBGES
      })
      .catch(error => {
        throw error
      })
  },
  getAllGDRMembres ({ commit }) {
    return userService.getAllGDRMembres()
      .then(allGDRMembres => {
        commit('SET_ALL_GDR_MEMBRES', allGDRMembres)
        return allGDRMembres
      })
      .catch(error => {
        throw error
      })
  },
  validateMembreGDR ({ commit }, membre) {
    return userService.validateMembreGDR({ 'email': membre.email })
      .then(allGDRMembres => {
        commit('SET_ALL_GDR_MEMBRES', allGDRMembres)
        return allGDRMembres
      })
      .catch(error => {
        throw error
      })
  },
  unacceptMembreGDR ({ commit }, membre) {
    return userService.unacceptMembreGDR({ 'email': membre.email })
      .then(allGDRMembres => {
        commit('SET_ALL_GDR_MEMBRES', allGDRMembres)
        return allGDRMembres
      })
      .catch(error => {
        throw error
      })
  },
  getAllInitiatives ({ commit }) {
    return apiService.getInitiativesAdmin()
      .then(allInitiatives => {
        commit('SET_ALL_INITIATIVES', allInitiatives)
        return allInitiatives
      })
      .catch(error => {
        throw error
      })
  },
  valdidateInitiative ({ commit }, initiative) {
    return apiService.validateInitiative(initiative)
      .then(allInitiatives => {
        commit('SET_ALL_INITIATIVES', allInitiatives)
        return allInitiatives
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  SET_ALL_BGES (state, allBGES) {
    state.allBGES = []
    for (let bges of allBGES) {
      // convert missions
      let allMissions = []
      for (let mission of bges.missions) {
        allMissions.unshift(Mission.createFromObj(mission))
      }
      bges.missions = allMissions
      // convert vehicles to object
      let allVehicles = []
      for (let vehicle of bges.vehicules) {
        allVehicles.unshift(Vehicle.createFromObj(vehicle))
      }
      bges.vehicules = allVehicles
      // convert buildings to object
      let allBuildings = []
      for (let building of bges.batiments) {
        allBuildings.unshift(Building.createFromObj(building))
      }
      bges.batiments = allBuildings
      // convert commutes
      let allDeplacements = []
      for (let deplacement of bges.deplacements_dt) {
        allDeplacements.unshift(Commute.createFromObj(deplacement))
      }
      bges.deplacements_dt = allDeplacements
      // convert device
      let allComputerDevices = []
      for (let device of bges.devices) {
        allComputerDevices.unshift(ComputerDevice.createFromObj(device))
      }
      bges.devices = allComputerDevices
      // convert purchases
      let allPurchases = []
      for (let purchase of bges.purchases) {
        allPurchases.unshift(Purchase.createFromObj(purchase))
      }
      bges.purchases = allPurchases
      bges.emissions = {}
      try {
        bges.emissions.vehicles = Vehicle.compute(
          allVehicles,
          bges.annee
        )
      } catch (error) {
        bges.emissions.vehicles = null
      }
      try {
        bges.emissions.missions = Mission.compute(
          allMissions,
          bges.annee
        )
      } catch (error) {
        bges.emissions.missions = null
      }
      try {
        bges.emissions.deplacements_dt = Commute.compute(
          allDeplacements,
          bges.laboratoire.tailleAglomeration,
          bges.nbChercheurs,
          bges.nbEnseignants,
          bges.nbITA,
          bges.nbPostDoc,
          bges.annee
        )
      } catch (error) {
        bges.emissions.deplacements_dt = null
      }
      try {
        bges.emissions.buildings = Building.compute(
          allBuildings,
          bges.laboratoire.adminName,
          bges.laboratoire.pays,
          bges.annee
        )
      } catch (error) {
        bges.emissions.buildings = null
      }
      try {
        bges.emissions.devices = ComputerDevice.compute(allComputerDevices)
      } catch (error) {
        bges.emissions.devices = null
      }
      try {
        bges.emissions.purchases = Purchase.compute(allPurchases)
      } catch (error) {
        bges.emissions.purchases = null
      }
      state.allBGES.unshift(bges)
    }
  },
  SET_ALL_GDR_MEMBRES (state, allGDRMembres) {
    state.allGDRMembres = allGDRMembres
  },
  SET_ALL_INITIATIVES (state, allInitiatives) {
    state.allInitiatives = allInitiatives
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
