import apiService from '@/services/apiService'

const state = {
  tutelles: []
}

const getters = {
  tutelles: state => state.tutelles
}

const actions = {
  getTutelles ({ commit }) {
    apiService.getTutelles()
      .then(tutelles => {
        commit('getTutelles', tutelles)
      })
  },
  addTutelle ({ commit }, tutelle) {
    apiService.addTutelle(tutelle)
      .then(() => {
        commit('addTutelle', tutelle)
      })
  }
}

const mutations = {
  getTutelles (state, tutelles) {
    state.tutelles = tutelles
  },
  addTutelle (state, tutelle) {
    state.tutelles.push(tutelle)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
