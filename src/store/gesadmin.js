
import carbonService from '@/services/carbonService'
import Mission from '@/models/Mission.js'
import Vehicle from '@/models/Vehicle.js'
import Building from '@/models/Building.js'
import Commute from '@/models/Commute.js'
import ComputerDevice from '@/models/ComputerDevice.js'
import Purchase from '@/models/Purchase.js'

function initialState () {
  return {
    allBGES: []
  }
}

const state = initialState()

const getters = {
  allBGES: state => state.allBGES
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  getAllBGESFull ({ commit }) {
    return carbonService.getAllBGESFull()
      .then(allBGES => {
        commit('SET_ALL_BGES', allBGES)
        commit('COMPUTE_ALL_BGES')
        return allBGES
      })
      .catch(error => {
        throw error
      })
  },
  computeAllBGES ({ commit }, year) {
    commit('COMPUTE_ALL_BGES', year)
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  COMPUTE_ALL_BGES (state, year = null) {
    let cyear = year
    for (let bges of state.allBGES) {
      if (year === null) {
        cyear = bges.annee
      } else {
        cyear = year
      }
      bges.emissions = {
        vehicles: Vehicle.compute(bges.vehicules, cyear),
        missions: Mission.compute(bges.missions, cyear),
        deplacements_dt: Commute.compute(
          bges.deplacements_dt,
          bges.laboratoire.tailleAglomeration,
          bges.nbChercheurs,
          bges.nbEnseignants,
          bges.nbITA,
          bges.nbPostDoc,
          cyear
        ),
        buildings: Building.compute(
          bges.batiments,
          bges.laboratoire.adminName,
          bges.laboratoire.pays,
          cyear
        ),
        devices: ComputerDevice.compute(bges.devices),
        purchases: Purchase.compute(bges.purchases)
      }
    }
  },
  SET_ALL_BGES (state, allBGES) {
    state.allBGES = []
    for (let bges of allBGES) {
      // convert missions
      let allMissions = []
      for (let mission of bges.missions) {
        allMissions.unshift(Mission.createFromObj(mission))
      }
      bges.missions = allMissions
      // convert vehicules
      let allVehicles = []
      for (let vehicle of bges.vehicules) {
        allVehicles.unshift(Vehicle.createFromObj(vehicle))
      }
      bges.vehicules = allVehicles
      // convert batiments
      let allBuildings = []
      for (let building of bges.batiments) {
        allBuildings.unshift(Building.createFromObj(building))
      }
      bges.batiments = allBuildings
      // convert deplacementDT
      let allDeplacements = []
      for (let deplacement of bges.deplacements_dt) {
        allDeplacements.unshift(Commute.createFromObj(deplacement))
      }
      bges.deplacements_dt = allDeplacements
      // convert computer devices
      let allComputerDevices = []
      for (let device of bges.devices) {
        allComputerDevices.unshift(ComputerDevice.createFromObj(device))
      }
      bges.devices = allComputerDevices
      // convert purchases
      let allPurchases = []
      for (let purchase of bges.purchases) {
        allPurchases.unshift(Purchase.createFromObj(purchase))
      }
      bges.purchases = allPurchases
      state.allBGES.unshift(bges)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
