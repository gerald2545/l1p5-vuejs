import apiService from '@/services/apiService'

const state = {
  equipes: [],
  disciplines: [],
  userDisciplines: [],
  genres: [],
  statuts: []
}

const getters = {
  equipes: state => state.equipes,
  disciplines: state => state.disciplines,
  userDisciplines: state => state.userDisciplines,
  genres: state => state.genres,
  statuts: state => state.statuts
}

const actions = {
  getDisciplines ({ commit }) {
    apiService.getDisciplines()
      .then(disciplines => {
        commit('SET_DISCIPLINES', disciplines)
      })
  },
  getUserDisciplines ({ commit }) {
    apiService.getUserDisciplines()
      .then(disciplines => {
        commit('SET_USER_DISCIPLINES', disciplines)
      })
  },
  getStatuts ({ commit }) {
    apiService.getStatuts()
      .then(statuts => {
        commit('SET_STATUTS', statuts)
      })
  },
  getGenres ({ commit }) {
    apiService.getGenres()
      .then(genres => {
        commit('SET_GENRES', genres)
      })
  }
}

const mutations = {
  SET_EQUIPES (state, equipes) {
    state.equipes = equipes
  },
  SET_DISCIPLINES (state, disciplines) {
    state.disciplines = disciplines
  },
  SET_USER_DISCIPLINES (state, disciplines) {
    state.userDisciplines = disciplines
  },
  SET_STATUTS (state, statuts) {
    state.statuts = statuts
  },
  SET_GENRES (state, genres) {
    state.genres = genres
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
