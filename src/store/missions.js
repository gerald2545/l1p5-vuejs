/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import carbonService from '@/services/carbonService'
import Mission from '@/models/Mission.js'

function initialState () {
  return {
    module: {
      isValid: false,
      isModified: false,
      erreur: '',
      computed: false
    },
    items: [],
    results: {}
  }
}

const state = initialState()

const getters = {
  module: state => state.module,
  items: state => state.items,
  hasMissions: state => state.items.length > 0,
  results: state => state.results,
  computed: state => state.module.computed,
  hasMotifsOption: (state) => (source = null) => {
    let nbMissions = 0
    let nbWithMotifs = 0
    if (source === null) {
      nbMissions = state.items.length
      nbWithMotifs = state.items.filter(mission => mission.motifDeplacement !== null).length
    } else {
      nbMissions = state.items.filter(mission => mission.source === source).length
      nbWithMotifs = state.items.filter(mission => mission.motifDeplacement !== null && mission.source === source).length
    }

    return nbMissions === nbWithMotifs
  },
  hasOneMotif: (state) => {
    return state.items.filter(mission => mission.motifDeplacement !== null).length !== 0
  },
  hasStatutsOption: (state) => (source = null) => {
    let nbMissions = 0
    let nbWithStatuts = 0
    if (source === null) {
      nbMissions = state.items.length
      nbWithStatuts = state.items.filter(mission => mission.statut !== null).length
    } else {
      nbMissions = state.items.filter(mission => mission.source === source).length
      nbWithStatuts = state.items.filter(mission => mission.statut !== null && mission.source === source).length
    }
    return nbMissions === nbWithStatuts
  },
  hasOneStatut: (state) => {
    return state.items.filter(mission => mission.statut !== null).length !== 0
  },
  invalidMissions: state => {
    return state.items.filter(mission => !mission.isValid)
  },
  nbValidMissions: (state) => (source) => {
    let validMissions = []
    if (source === null) {
      validMissions = state.items.filter(mission => mission.isValid)
    } else {
      validMissions = state.items.filter(mission => mission.isValid && mission.source === source)
    }
    let nbValidMissions = 0
    if (validMissions.length > 0) {
      nbValidMissions = validMissions.map(obj => obj.nbEffectue).reduce((a, b) => a + b)
    }
    return nbValidMissions
  },
  nbMissions: (state) => (source) => {
    let missions = []
    if (source === null) {
      missions = state.items
    } else {
      missions = state.items.filter(mission => mission.source === source)
    }
    let nbMissions = 0
    if (missions.length > 0) {
      nbMissions = missions.map(obj => obj.nbEffectue).reduce((a, b) => a + b)
    }
    return nbMissions
  },
  missionsLoadedFromDatabase: state => {
    return state.items.filter(mission => mission.isFromDatabase()).length !== 0
  },
  submitOk: state => state.items.length > 0
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  add ({ commit }, mission) {
    commit('ADD', mission)
  },
  update ({ commit }, obj) {
    commit('UPDATE', obj)
  },
  updateAll ({ commit }, missions) {
    commit('UPDATE_ALL', missions)
  },
  deleteInvalid ({ commit }) {
    commit('DELETE_INVALID')
  },
  deleteSource ({ commit }, source) {
    commit('DELETE_SOURCE', source)
  },
  delete ({ commit }, mission) {
    commit('DELETE', mission)
  },
  save ({ dispatch, rootGetters, commit, state }) {
    let missionsDatabase = []
    for (let mission of state.items) {
      missionsDatabase.unshift(mission.toDatabase())
    }
    let allData = {
      'missions': missionsDatabase,
      'bges_id': rootGetters['boundaries/bges']['pk']
    }
    return carbonService.saveMissions(allData)
      .then(data => {
        commit('UPDATE_ALL', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  compute ({ dispatch, rootGetters, commit, state }) {
    commit('UPDATE_RESULTS', {})
    let emissions = Mission.compute(state.items, rootGetters['boundaries/bgesAnnee'])
    commit('COMPUTED', true)
    commit('UPDATE_RESULTS', emissions)
  },
  forceComputation ({ commit }) {
    commit('COMPUTED', false)
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  ADD (state, mission) {
    let missionAModifier = state.items.filter(obj => obj.isEqualTo(mission))
    if (missionAModifier.length === 0) {
      state.items.unshift(Mission.createFromObj(mission))
    } else {
      missionAModifier[0].add(mission.idMission)
    }
  },
  UPDATE (state, iobj) {
    const item = state.items.find(obj => obj.isEqualTo(iobj.from))
    Object.assign(item, Mission.createFromObj(iobj.to))
  },
  UPDATE_ALL (state, missions) {
    state.items = []
    for (let mission of missions) {
      state.items.unshift(Mission.createFromObj(mission))
    }
  },
  DELETE_INVALID (state) {
    let invalidIndex = []
    for (let index in state.items) {
      if (!state.items[index].isValid) {
        invalidIndex.unshift(index)
      }
    }
    for (let index of invalidIndex) {
      state.items.splice(index, 1)
    }
  },
  DELETE_SOURCE (state, source) {
    if (source) {
      let indexes = []
      for (let index in state.items) {
        if (state.items[index].source === source) {
          indexes.unshift(index)
        }
      }
      for (let index of indexes) {
        state.items.splice(index, 1)
      }
    } else {
      state.items = []
    }
  },
  DELETE (state, mission) {
    let index = state.items.findIndex(obj => obj.isEqualTo(mission))
    state.items.splice(index, 1)
  },
  UPDATE_RESULTS (state, missions) {
    state.results = missions
  },
  ISVALID (state, value) {
    state.module.isValid = value
  },
  ISMODIFIED (state, value) {
    state.module.isModified = value
    state.module.computed = false
  },
  ERREUR (state, value) {
    state.module.erreur = value
  },
  COMPUTED (state, value) {
    state.module.computed = value
  },
  RESET_MODULE (state) {
    state.module.isValid = false
    state.module.isModified = false
    state.module.erreur = ''
    state.module.computed = false
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
