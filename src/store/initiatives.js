import apiService from '@/services/apiService'

const state = {
  initiatives: []
}

const getters = {
  initiatives: state => {
    return state.initiatives
  }
}

const actions = {
  getInitiatives ({ commit }) {
    return apiService.getInitiatives()
      .then(initiatives => {
        commit('getInitiatives', initiatives)
      })
      .catch(error => {
        throw error.response.data
      })
  },
  addInitiative ({ commit }, initiative) {
    apiService.addInitiative(initiative)
      .then(() => {
        commit('addInitiative', initiative)
      })
  }
}

const mutations = {
  getInitiatives (state, initiatives) {
    state.initiatives = initiatives
  },
  addInitiative (state, initiative) {
    state.initiative.push(initiative)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
