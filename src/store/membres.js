import userService from '@/services/userService'

const state = {
  membresLocations: [],
  membresEEmpreinte: [],
  membresEEnquete: [],
  membresEReflexion: [],
  membresEExpe: [],
  membresECom: [],
  membresETechnique: [],
  membresEStatus: [],
  membresEEnseignement: [],
  membresDisciplines: []
}

const getters = {
  membresLocations: state => state.membresLocations,
  membresEEmpreinte: state => state.membresEEmpreinte,
  membresEEnquete: state => state.membresEEnquete,
  membresEReflexion: state => state.membresEReflexion,
  membresEExpe: state => state.membresEExpe,
  membresECom: state => state.membresECom,
  membresETechnique: state => state.membresETechnique,
  membresEStatus: state => state.membresEStatus,
  membresEEnseignement: state => state.membresEEnseignement,
  membresDisciplines: state => state.membresDisciplines
}

const actions = {
  getMembresLocations ({ commit }) {
    return userService.getMembresLocations()
      .then(membres => {
        commit('setMembresLocations', membres)
      })
      .catch(error => {
        throw error
      })
  },
  getMembresEquipes ({ commit }, equipe) {
    return userService.getMembresEquipes(equipe)
      .then(membres => {
        if (equipe.equipe === 'Empreinte') {
          commit('setMembresEEmpreinte', membres)
        } else if (equipe.equipe === 'Enquête') {
          commit('setMembresEEnquete', membres)
        } else if (equipe.equipe === 'Réflexion') {
          commit('setMembresEReflexion', membres)
        } else if (equipe.equipe === 'Expérimentation') {
          commit('setMembresEExpe', membres)
        } else if (equipe.equipe === 'Communication') {
          commit('setMembresECom', membres)
        } else if (equipe.equipe === 'Technique') {
          commit('setMembresETechnique', membres)
        } else if (equipe.equipe === 'Enseignement') {
          commit('setMembresEEnseignement', membres)
        }
      })
      .catch(error => {
        throw error
      })
  },
  getMembresDisciplines ({ commit }) {
    return userService.getMembresDisciplines()
      .then(membres => {
        commit('setMembresDisciplines', membres)
      })
      .catch(error => {
        throw error
      })
  },
  addMembreNewsletter ({ commit }, membre) {
    return userService.addMembreNewsletter(membre)
      .then(result => {
        return result.data
      })
      .catch(error => {
        throw error
      })
  },
  addMembreGDR ({ commit }, membre) {
    return userService.addMembreGDR(membre)
      .then(result => {
        return result
      })
      .catch(error => {
        throw error
      })
  },
  addMembreReflexion ({ commit }, membre) {
    return userService.addMembreReflexion(membre)
      .then(result => {
        return result.data
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  setMembresLocations (state, membres) {
    state.membresLocations = membres
  },
  setMembresDisciplines (state, membres) {
    state.membresDisciplines = membres
  },
  setMembresEEmpreinte (state, membres) {
    state.membresEEmpreinte = membres
  },
  setMembresEEnquete (state, membres) {
    state.membresEEnquete = membres
  },
  setMembresEReflexion (state, membres) {
    state.membresEReflexion = membres
  },
  setMembresEExpe (state, membres) {
    state.membresEExpe = membres
  },
  setMembresECom (state, membres) {
    state.membresECom = membres
  },
  setMembresETechnique (state, membres) {
    state.membresETechnique = membres
  },
  setMembresEStatus (state, membres) {
    state.membresEStatus = membres
  },
  setMembresEEnseignement (state, membres) {
    state.membresEEnseignement = membres
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
