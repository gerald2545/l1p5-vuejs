/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import userService from '@/services/userService'
import apiService from '@/services/apiService'
import carbonService from '@/services/carbonService'

function initialState () {
  return {
    membre: {
      id: null,
      equipes: [],
      email: '',
      nom: '',
      prenom: '',
      ville: '',
      pays: '',
      discipline: '',
      disciplineCNU: '',
      commentaire: '',
      latitude: 46.75,
      longitude: 2.32,
      password1: null,
      password2: null,
      genre: null,
      anneeNaissance: '',
      laboratoire: '',
      tutelles: [],
      statut: null,
      engagementAssociatif: '',
      engagementInstitutionel: '',
      engagementAutres: '',
      msgCommunication: '',
      msgEmpreinte: '',
      msgEnquete: '',
      msgExperimentation: '',
      msgReflexion: '',
      msgTechnique: '',
      msgEnseignement: ''
    },
    isSuperUser: false,
    laboratoire: {
      pk: null,
      nom: '',
      sitePrincipal: '',
      pays: '',
      adminName: '',
      tutelles: [],
      tailleAglomeration: null,
      latitude: 46.75,
      longitude: 2.32,
      budget: 0,
      discipline: '',
      sites: []
    },
    vehicles: [],
    buildings: []
  }
}

const state = initialState()

const getters = {
  user: state => state.user,
  isSuperUser: state => state.isSuperUser,
  laboratoire: state => state.laboratoire,
  tailleAglomeration: state => state.laboratoire.tailleAglomeration,
  sites: state => state.laboratoire.sites,
  sitePrincipal: state => state.laboratoire.sitePrincipal,
  adminName: state => state.laboratoire.adminName,
  pays: state => state.laboratoire.pays,
  vehicles: state => state.vehicles,
  buildings: state => state.buildings,
  userNom: state => state.membre.nom,
  userLab: state => state.laboratoire.nom,
  userLabTutelles: state => state.laboratoire.tutelles
}

const actions = {
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  getMembre ({ commit }) {
    return userService.getMembre()
      .then(membre => {
        commit('SET_MEMBRE', membre[0])
        return membre[0]
      })
      .catch(error => {
        throw error
      })
  },
  saveMembre ({ commit }, membre) {
    return userService.saveMembre(membre)
      .then(membre => {
        commit('SET_MEMBRE', membre)
        return membre
      })
      .catch(error => {
        throw error
      })
  },
  isSuperUser ({ commit }) {
    return userService.isSuperUser()
      .then(data => {
        commit('IS_SUPER_USER', data.is_super_user)
        return data.is_super_user
      })
      .catch(error => {
        commit('IS_SUPER_USER', false)
        throw error
      })
  },
  saveLaboratoire ({ commit }, laboratoire) {
    return apiService.saveLaboratoire(laboratoire)
      .then(data => {
        data['sites'] = data.sites.map(function (obj) { return obj.nom })
        commit('UPDATE_LABORATOIRE', data)
        return data
      })
      .catch(error => {
        throw error
      })
  },
  getLaboratoire ({ commit }) {
    return apiService.getLaboratoire()
      .then(data => {
        if (data) {
          data['sites'] = data.sites.map(function (obj) { return obj.nom })
          commit('UPDATE_LABORATOIRE', data)
        }
        return data
      })
      .catch(error => {
        throw error
      })
  },
  getAllVehicles ({ commit }) {
    return carbonService.getAllVehicles()
      .then(data => {
        if (data) {
          commit('UPDATE_VEHICLES', data)
        }
        return data
      })
      .catch(error => {
        throw error
      })
  },
  getAllBuildings ({ commit }) {
    return carbonService.getAllBuildings()
      .then(data => {
        if (data) {
          commit('UPDATE_BUILDINGS', data)
        }
        return data
      })
      .catch(error => {
        throw error
      })
  }
}

const mutations = {
  RESET_STATE (state) {
    const iObj = initialState()
    Object.keys(iObj).forEach(key => {
      state[key] = iObj[key]
    })
  },
  SET_MEMBRE (state, membre) {
    state.membre = membre
  },
  UPDATE_LABORATOIRE (state, laboratoire) {
    state.laboratoire = laboratoire
  },
  UPDATE_VEHICLES (state, vehicles) {
    state.vehicles = vehicles
  },
  UPDATE_BUILDINGS (state, buildings) {
    state.buildings = buildings
  },
  IS_SUPER_USER (state, value) {
    state.isSuperUser = value
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
