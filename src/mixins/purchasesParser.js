import * as d3 from 'd3-dsv'

export const purchasesParser = {
  methods: {
    loadFile: function (dataTxt) {
      // Find separator from first line
      let headerLine = dataTxt.split('\n')[0]
      let separators = [',', ';', '\t']
      let bestSep = null
      let bestSepCount = 0
      for (let sep of separators) {
        let count = (headerLine.match(new RegExp(sep, 'gi')) || []).length
        if (count > bestSepCount) {
          bestSepCount = count
          bestSep = sep
        }
      }
      if (!bestSep) {
        return [[], {}, 'bad separator']
      }

      let psv = d3.dsvFormat(bestSep)
      let data = psv.parse(dataTxt)

      let headerMap = this.parseHeaders(data)

      if (!(headerMap.icode || headerMap.iamount)) {
        return [[], {}, 'no code or amount']
      }

      // csv_data = this.csv_merge_raw_items(csv_data, header_map)
      return [data, headerMap, 'ok']
    },

    // Search for code nacre and amount columns
    // within data.columns, and return the matches as
    // {icode,iamount}
    parseHeaders: function (data) {
      let findHeader = function (regex) {
        for (let k in data.columns) {
          var name = data.columns[k]
          if (name.search(regex) >= 0) {
            return name
          }
        }
        return null
      }
      return {
        icode: findHeader(/(code|nacre|code.nacre|codenacre|nacrecode)/i),
        iamount: findHeader(/(montant|amount|somme|total|€)/i)
      }
    }
  }
}
