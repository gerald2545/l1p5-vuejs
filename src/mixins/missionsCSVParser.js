import Mission from '@/models/Mission.js'

export const missionsCSVParser = {
  methods: {
    getCSVFormat (file) {
      return new Promise((resolve, reject) => {
        let reader = new FileReader()
        reader.addEventListener('load', function (event) {
          let header = event.target.result.split('\n')[0].replace(/["]/g, '')
          if (header.startsWith('Groupe labo\tNuméro mission\tDate de départ\tVille de départ\tPays de départ\tVille de destination\tPays de destination\tMoyens de transport\tNb de pers. dans la voiture\tAller / Retour\tMotif du déplacement\tStatut agent\tDate de retour')) {
            resolve('GESLAB_FORMAT')
          } else if (header.toLowerCase().startsWith('# mission') || header.toLowerCase().startsWith('mission')) {
            resolve('GES1POINT5_FORMAT')
          } else if (header.toLowerCase().startsWith('# trip') || header.toLowerCase().startsWith('trip')) {
            resolve('GES1POINT5_FORMAT')
          } else {
            resolve('UNKNOWN_FORMAT')
          }
        })
        reader.readAsText(file)
      })
    },
    parseCSVLine (line, csvFormat) {
      let deplacement = null
      if (csvFormat === 'GESLAB_FORMAT') {
        deplacement = this.parseGeslabCSVLine(line)
      } else if (csvFormat === 'GES1POINT5_FORMAT') {
        deplacement = this.parseGES1point5CSVLine(line)
      }
      return deplacement
    },
    parseGES1point5CSVLine (line) {
      let parts = line.split('\t')
      if (parts.length >= 9) {
        let statut = ''
        let motif = ''
        if (parts.length === 10) {
          motif = parts[9].replace('"', '').replace('"', '').trim()
        } else if (parts.length === 11) {
          motif = parts[9].replace('"', '').replace('"', '').trim()
          statut = parts[10].replace('"', '').replace('"', '').trim()
        }
        return new Mission(
          parts[0].replace('"', '').replace('"', '').trim(),
          parts[1].replace('"', '').replace('"', ''),
          parts[2].replace('"', '').replace('"', '').trim(),
          parts[3].replace('"', '').replace('"', '').trim(),
          parts[4].replace('"', '').replace('"', '').trim(),
          parts[5].replace('"', '').replace('"', '').trim(),
          null,
          parts[6].replace('"', '').replace('"', '').trim(),
          parts[7].replace('"', '').replace('"', '').trim(),
          motif,
          statut,
          parts[8].replace('"', '').replace('"', '').trim()
        )
      } else {
        return null
      }
    },
    convertGeslabModeDeplacement (modeDeplacement) {
      let finalMode = ''
      if (modeDeplacement === 'Avion') {
        finalMode = Mission.MODE_PLANE
      } else if (modeDeplacement === 'Bateau') {
        finalMode = Mission.MODE_FERRY
      } else if (modeDeplacement === 'Bus') {
        finalMode = Mission.MODE_BUS
      } else if (modeDeplacement === 'Divers') {
        throw new Error('Mode de déplacement « Divers » non pris en compte.')
      } else if (modeDeplacement === 'Location de véhicule') {
        finalMode = Mission.MODE_CAR
      } else if (modeDeplacement === 'Metro') {
        finalMode = Mission.MODE_SUBWAY
      } else if (modeDeplacement === 'Passager') {
        throw new Error('Mode de déplacement « Passager » non pris en compte.')
      } else if (modeDeplacement === 'Rer') {
        finalMode = Mission.MODE_RER
      } else if (modeDeplacement === 'Taxi') {
        finalMode = Mission.MODE_CAB
      } else if (modeDeplacement === 'Train') {
        finalMode = Mission.MODE_TRAIN
      } else if (modeDeplacement === 'Véhicule administratif') {
        throw new Error('Mode de déplacement « Véhicule administratif » non pris en compte.')
      } else if (modeDeplacement === 'Véhicule personnel') {
        finalMode = Mission.MODE_CAR
      }
      return finalMode
    },
    getDominantModeDeplacement (allModes) {
      let finalMode = ''
      let modesOrder = [
        Mission.MODE_PLANE,
        Mission.MODE_TRAIN,
        Mission.MODE_FERRY,
        Mission.MODE_BUS,
        Mission.MODE_CAR,
        Mission.MODE_RER,
        Mission.MODE_SUBWAY,
        Mission.MODE_CAB
      ]
      for (let mode of modesOrder) {
        if (allModes.includes(mode)) {
          finalMode = mode
          break
        }
      }
      return finalMode
    },
    getGeslabModeDeplacement (modeDeplacement) {
      let allModes = modeDeplacement.split(',')
      let finalMode = ''
      if (allModes.length === 1) {
        finalMode = this.convertGeslabModeDeplacement(modeDeplacement)
      } else {
        let convertedAllModes = allModes.map(value => {
          try {
            return this.convertGeslabModeDeplacement(value)
          } catch {
            return ''
          }
        })
        // test if only blank value obtained from catch
        let nbBlank = 0
        for (let value of convertedAllModes) {
          if (value === '') {
            nbBlank += 1
          }
        }
        if (nbBlank === convertedAllModes.length) {
          throw new Error('Modes de déplacement « ' + modeDeplacement + ' » non reconnus.')
        }
        finalMode = this.getDominantModeDeplacement(convertedAllModes)
      }
      finalMode = Mission.getTravelsMode(finalMode)
      if (finalMode === null) {
        throw new Error('Mode de déplacement « ' + modeDeplacement + ' » non reconnu.')
      }
      return finalMode
    },
    getGeslabMotifDeplacement (motifDeplacement) {
      let md = ''
      if (motifDeplacement === 'Acq nouv connaissances&techniq') {
        md = Mission.PURPOSE_SEMINAR
      } else if (motifDeplacement === 'Administration de la recherche') {
        md = Mission.PURPOSE_RESEARCH_MANAGEMENT
      } else if (motifDeplacement === 'Autres') {
        md = Mission.PURPOSE_OTHER
      } else if (motifDeplacement === 'Colloques et congrés') {
        md = Mission.PURPOSE_CONFERENCE
      } else if (motifDeplacement === 'Enseignement dispensé') {
        md = Mission.PURPOSE_TEACHING
      } else if (motifDeplacement === 'Rech. doc. ou sur terrain') {
        md = Mission.PURPOSE_FIELD_STUDY
      } else if (motifDeplacement === 'Rech. en équipe, collaboration') {
        md = Mission.PURPOSE_COLLABORATION
      } else if (motifDeplacement === 'Visite, contact pour projet') {
        md = Mission.PURPOSE_VISIT
      }
      return md
    },
    parseGeslabCSVLine (line) {
      let parts = line.split('\t')
      let modeDeplacement = null
      try {
        modeDeplacement = this.getGeslabModeDeplacement(parts[7])
      } catch (error) {
        modeDeplacement = parts[7]
      }
      let motifDeplacement = null
      try {
        motifDeplacement = this.getGeslabMotifDeplacement(parts[10])
      } catch (error) {
        motifDeplacement = parts[10]
      }
      return new Mission(
        parts[1],
        parts[2],
        parts[3],
        parts[4],
        parts[5],
        parts[6],
        null,
        modeDeplacement,
        parts[8],
        motifDeplacement,
        '',
        parts[9]
      )
    }
  }
}
