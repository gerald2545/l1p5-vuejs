import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

import MainNavbar from '@/layout/MainNavbar.vue'
import OnlyHeader from '@/layout/OnlyHeader.vue'
import MainFooter from '@/layout/MainFooter.vue'

import '../../node_modules/nprogress/nprogress.css'

Vue.use(VueRouter)

const requireAuthenticated = (to, from, next) => {
  if (!store.getters['authentication/isAuthenticated']) {
    next('/')
  } else {
    next()
  }
}

// const requireUnauthenticated = (to, from, next) => {
//   if (store.getters['authentication/isAuthenticated']) {
//     next('/')
//   } else {
//     next()
//   }
// }

const router = new VueRouter({
  mode: 'history',
  hashbang: false,
  linkExactActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: () => import('@/views/Home.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Réduire l\'empreinte de nos activités de recherche sur l\'environnement',
        titreEN: 'Reducing the environmental footprint of our research activities',
        soustitreFR: 'Labos 1point5 est un collectif de membres du monde académique, de toutes disciplines et sur tout le territoire, partageant un objectif commun : mieux comprendre et réduire l\'impact des activités de recherche scientifique sur l\'environnement, en particulier sur le climat.',
        soustitreEN: 'Labos 1point5 is an international, cross-disciplinary collective of academic researchers who share a common goal: to better understand and reduce the environmental impact of research, especially on the Earth’s climate.',
        home: true
      }
    },
    {
      path: '/nos-objectifs',
      name: 'nos-objectifs',
      components: {
        default: () => import('@/views/NosObjectifs.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Nos objectifs',
        soustitreFR: ''
      }
    },
    {
      path: '/calendar',
      name: 'calendar',
      components: {
        default: () => import('@/views/Calendar.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Agenda',
        titreEN: 'Calendar',
        soustitreFR: ''
      }
    },
    {
      path: '/le-gdr',
      name: 'le-gdr',
      components: {
        default: () => import('@/views/LeGDR.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Le GDR',
        soustitreFR: ''
      }
    },
    {
      path: '/reflexion',
      name: 'reflexion',
      components: {
        default: () => import('@/views/Reflexion.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'L\'équipe réflexion',
        soustitreFR: ''
      }
    },

    {
      path: '/reflexion/:name',
      name: 'reflexion-named',
      components: {
        default: () => import('@/views/Reflexion.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'L\'équipe réflexion',
        soustitreFR: ''
      }
    },
    {
      path: '/on-parle-de-nous',
      name: 'on-parle-de-nous',
      components: {
        default: () => import('@/views/OnParleDeNous.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'On parle de nous',
        soustitreFR: ''
      }
    },
    {
      path: '/les-initiatives',
      name: 'les-initiatives',
      components: {
        default: () => import('@/views/LesInitiatives.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Les initiatives',
        soustitreFR: ''
      }
    },
    {
      path: '/la-litterature',
      name: 'la-litterature',
      components: {
        default: () => import('@/views/LaLitterature.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'La litterature',
        soustitreFR: ''
      }
    },
    {
      path: '/les-infographies/:name',
      name: 'les-infographies-named',
      components: {
        default: () => import('@/views/LesInfographies.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Les infographies',
        titreEN: 'The infographics',
        soustitreFR: ''
      }
    },
    {
      path: '/les-infographies',
      name: 'les-infographies',
      components: {
        default: () => import('@/views/LesInfographies.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Les infographies',
        titreEN: 'The infographics',
        soustitreFR: ''
      }
    },
    {
      path: '/les-decryptages/:name',
      name: 'les-decryptages-named',
      components: {
        default: () => import('@/views/LesDecryptages.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Les décryptages',
        titreEN: 'The decypherings',
        soustitreFR: ''
      }
    },
    {
      path: '/les-decryptages',
      name: 'les-decryptages',
      components: {
        default: () => import('@/views/LesDecryptages.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Les décryptages',
        titreEN: 'The decypherings',
        soustitreFR: ''
      }
    },
    {
      path: '/administration/',
      name: 'administration',
      components: {
        default: () => import('@/views/Administration.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      beforeEnter: requireAuthenticated,
      meta: {
        titreFR: 'Administration',
        titreEN: 'Administration',
        soustitreFR: ''
      }
    },
    {
      path: '/administration/:name',
      name: 'administration-named',
      components: {
        default: () => import('@/views/Administration.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Administration',
        titreEN: 'Administration',
        soustitreFR: ''
      }
    },
    {
      path: '/ges-1point5/',
      name: 'ges-1point5',
      components: {
        default: () => import('@/views/GES1point5.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'GES 1point5',
        titreEN: 'GES 1point5',
        soustitreFR: ''
      }
    },
    {
      path: '/ges-1point5/:id',
      name: 'ges-1point5-id',
      components: {
        default: () => import('@/views/GES1point5.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      beforeEnter: requireAuthenticated,
      meta: {
        titreFR: 'GES 1point5',
        titreEN: 'GES 1point5',
        soustitreFR: ''
      }
    },
    {
      path: '/data-access-form',
      name: 'data-access-form',
      components: {
        default: () => import('@/views/DataAccessForm.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Données GES 1point5',
        soustitreFR: ''
      }
    },
    {
      path: '/les-enquetes/:name',
      name: 'les-enquetes-named',
      components: {
        default: () => import('@/views/LesEnquetes.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Nos enquêtes',
        soustitreFR: ''
      }
    },
    {
      path: '/les-enquetes',
      name: 'les-enquetes',
      components: {
        default: () => import('@/views/LesEnquetes.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Nos enquêtes',
        soustitreFR: ''
      }
    },
    {
      path: '/rejoindre-gdr',
      name: 'rejoindre-gdr',
      components: {
        default: () => import('@/views/RejoindreGDR.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Rejoindre le GDR',
        soustitreFR: ''
      }
    },
    {
      path: '/rejoindre-reflexion',
      name: 'rejoindre-reflexion',
      components: {
        default: () => import('@/views/RejoindreReflexion.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Rejoindre l\'équipe réflexion',
        soustitreFR: ''
      }
    },
    {
      path: '/politique-de-confidentialite',
      name: 'politique-de-confidentialite',
      components: {
        default: () => import('@/views/PolitiqueDeConfidentialite.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Politique de confidentialité',
        soustitreFR: ''
      }
    },
    {
      path: '/terms-of-use',
      name: 'terms-of-use',
      components: {
        default: () => import('@/views/TermsOfUse.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Charte d\'utilisation',
        soustitreFR: ''
      }
    },
    {
      path: '/info-news/:name',
      name: 'info-news-named',
      components: {
        default: () => import('@/views/InfoNews.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Info et news',
        soustitreFR: '',
        titreEN: 'Info and news'
      }
    },
    {
      path: '/info-news',
      name: 'info-news',
      components: {
        default: () => import('@/views/InfoNews.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Info et news',
        soustitreFR: '',
        titreEN: 'Info and news'
      }
    },
    {
      path: '/merci',
      name: 'merci',
      components: {
        default: () => import('@/views/Merci.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Merci',
        soustitreFR: ''
      }
    },
    {
      path: '/labos1point5-charter',
      name: 'labos1point5-charter',
      components: {
        default: () => import('@/views/Labos1point5Charter.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Charte Labos 1point5',
        titreEN: 'Labos 1point5 charter',
        soustitreFR: ''
      }
    },
    {
      path: '/commutes-survey/:uuid',
      name: 'commutes-survey',
      components: {
        default: () => import('@/views/CommutesSurvey.vue'),
        header: OnlyHeader,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Questionnaire domicile / travail',
        titreEN: 'Commute survey',
        soustitreFR: ''
      }
    },
    {
      path: '/commutes-simulator',
      name: 'commutes-simulator',
      components: {
        default: () => import('@/views/CommutesSimulator.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Simulateur',
        titreEN: 'Commutes simulator',
        soustitreFR: ''
      }
    },
    {
      path: '/travels-simulator',
      name: 'travels-simulator',
      components: {
        default: () => import('@/views/TravelsSimulator.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Simulateur',
        titreEN: 'Travels simulator',
        soustitreFR: ''
      }
    },
    {
      path: '/reset-password',
      name: 'reset-password',
      components: {
        default: () => import('@/views/ResetPassword.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Mot de passe oublié ?',
        titreEN: 'Forgot your password ?',
        soustitreFR: ''
      }
    },
    {
      path: '/l-enseignement',
      name: 'l-enseignement',
      components: {
        default: () => import('@/views/LesEnseignements.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'L\'enseignement',
        soustitreFR: ''
      }
    },
    {
      path: '/les-seminaires',
      name: 'les-seminaires',
      components: {
        default: () => import('@/views/LesSeminaires.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Les séminaires',
        soustitreFR: ''
      }
    },
    {
      path: '/reset-password/accounts/reset/:id/:token',
      name: 'reset-password-link',
      components: {
        default: () => import('@/views/ResetPassword.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Mot de passe oublié ?',
        titreEN: 'Forgot your password ?',
        soustitreFR: ''
      }
    },
    {
      path: '/activation-compte/accounts/activate_account/:id/:token',
      name: 'activation-compte',
      components: {
        default: () => import('@/views/ActivationCompte.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Activation de compte',
        soustitreFR: ''
      }
    },
    {
      path: '/desabonnement-liste/:liste',
      name: 'desabonnement-liste',
      components: {
        default: () => import('@/views/DesabonnementListe.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Désabonnement',
        soustitreFR: ''
      }
    },
    {
      path: '*',
      name: 'not-found',
      components: {
        default: () => import('@/views/404.vue'),
        header: MainNavbar,
        footer: MainFooter
      },
      meta: {
        titreFR: 'Not found',
        soustitreFR: ''
      }
    }
  ]
})

router.beforeResolve((to, from, next) => {
  if (to.name && from.name !== null) {
    router.app.$nprogress.start()
  }
  next()
})

router.afterEach((to, from) => {
  setTimeout(() => router.app.$nprogress.done(), 500)
})

export default router
