/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Mission from '@/models/Mission'
import TravelSection from '@/models/TravelSection'

export default class Travel {
  constructor (
    id, dateDepart, isAllerRetour, sections
  ) {
    this.id = id
    this.dateDepart = dateDepart
    this.isAllerRetour = isAllerRetour
    this.sections = sections.map((obj) => TravelSection.createFromObj(obj))
  }

  addSection (section) {
    this.sections.push(TravelSection.createFromObj(section))
  }

  isComplete () {
    let isComplete = true
    for (let section of this.sections) {
      if (section.villeDepart === null || section.villeDestination === null || section.modeDeplacement === null) {
        isComplete = false
      }
    }
    return isComplete
  }

  getDistance () {
    let distance = 0
    for (let section of this.sections) {
      if (section.villeDepart !== null && section.villeDestination !== null && section.modeDeplacement !== null) {
        let nSection = TravelSection.createFromObj(section)
        distance += nSection.getCorrectedDistance()
      }
    }
    return distance
  }

  getCarbonIntensity (bgesYear) {
    let total = 0
    let totali = 0
    let totalnr = 0
    let totalnri = 0
    for (let section of this.sections) {
      if (section.villeDepart !== null && section.villeDestination !== null && section.modeDeplacement !== null) {
        let nSection = TravelSection.createFromObj(section)
        let emi = nSection.getCarbonIntensity(bgesYear, this.isAllerRetour)
        total += emi.total
        totali += emi.totali
        totalnr += emi.totalnr
        totalnri += emi.totalnri
      }
    }
    return {
      'total': total,
      'totali': totali,
      'totalnr': totalnr,
      'totalnri': totalnri
    }
  }

  static get modesDeplacements () {
    return Mission.modesDeplacements
  }
}
