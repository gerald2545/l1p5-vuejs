/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Mission from '@/models/Mission.js'
import { filterEFByYear } from '@/mixins/utils'
import VEHICLES_FACTORS from '@/../data/vehiclesFactors.json'
import TRANSPORTS_FACTORS from '@/../data/transportsFactors.json'

const MODES_DEPLACEMENTS = {
  [Mission.MODE_PLANE]: 1.0,
  [Mission.MODE_TRAIN]: 1.2,
  [Mission.MODE_CAR]: 1.3,
  [Mission.MODE_CAB]: 1.3,
  [Mission.MODE_BUS]: 1.5,
  [Mission.MODE_TRAM]: 1.5,
  [Mission.MODE_RER]: 1.2,
  [Mission.MODE_SUBWAY]: 1.7,
  [Mission.MODE_FERRY]: 1.0
}

export default class TravelSection {
  constructor (
    modeDeplacement = null, villeDepart = null,
    paysDepart = null, villeDestination = null, paysDestination = null,
    nbPersonneVoiture = 1, distance = 0, villeDepartLat = 0.0,
    villeDepartLng = 0.0, villeDestinationLat = 0.0,
    villeDestinationLng = 0.0, missionType = null
  ) {
    this.modeDeplacement = modeDeplacement
    this.villeDepart = villeDepart
    this.paysDepart = paysDepart
    this.villeDestination = villeDestination
    this.paysDestination = paysDestination
    this.nbPersonneVoiture = nbPersonneVoiture
    this.villeDepartLat = villeDepartLat
    this.villeDepartLng = villeDepartLng
    this.villeDestinationLat = villeDestinationLat
    this.villeDestinationLng = villeDestinationLng
    this.distance = distance
    if (missionType !== null) {
      this.missionType = missionType
    } else {
      this.computeType()
    }
  }

  updateDistance () {
    this.distance = TravelSection.geodesicDist(
      this.villeDepartLat,
      this.villeDepartLng,
      this.villeDestinationLat,
      this.villeDestinationLng
    )
  }

  getCorrectedDistance () {
    this.updateDistance()
    let cdistance = this.distance
    if (this.modeDeplacement === Mission.MODE_PLANE) {
      cdistance += 95
    }
    // facteur multiplicateur en fonction du mode de deplacement
    cdistance = cdistance * MODES_DEPLACEMENTS[this.modeDeplacement]
    return cdistance
  }

  computeType () {
    this.missionType = 'IN'
    if (this.paysDepart === 'FR' && this.paysDestination === 'FR') {
      this.missionType = 'NA'
    } else if (this.paysDepart === 'FR' || this.paysDestination === 'FR') {
      this.missionType = 'MX'
    }
  }

  getEmissionFactor (distance, bgesYear, reglementaire = true) {
    let ef = null
    this.computeType()
    if (reglementaire && this.modeDeplacement === Mission.MODE_PLANE) {
      if (distance < 1000) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['shorthaul'], bgesYear)
      } else if (distance < 3501) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['mediumhaul'], bgesYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['longhaul'], bgesYear)
      }
    } else if (!reglementaire && this.modeDeplacement === Mission.MODE_PLANE) {
      if (distance < 1000) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['shorthaul.contrails'], bgesYear)
      } else if (distance < 3501) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['mediumhaul.contrails'], bgesYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['longhaul.contrails'], bgesYear)
      }
    } else if (this.modeDeplacement === Mission.MODE_CAR || this.modeDeplacement === Mission.MODE_CAB) {
      ef = filterEFByYear(VEHICLES_FACTORS['car']['unknown.engine'], bgesYear)
    } else if (this.modeDeplacement === Mission.MODE_BUS) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.bigcity'], bgesYear)
    } else if (this.modeDeplacement === Mission.MODE_TRAIN) {
      if (this.missionType === 'NA') {
        if (distance < 200) {
          ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.shortdistance'], bgesYear)
        } else {
          ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.longdistance'], bgesYear)
        }
      } else if (this.missionType === 'MX') {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.mixed'], bgesYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.international'], bgesYear)
      }
    } else if (this.modeDeplacement === Mission.MODE_TRAM) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['tram.bigcity'], bgesYear)
    } else if (this.modeDeplacement === Mission.MODE_RER) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['rer'], bgesYear)
    } else if (this.modeDeplacement === Mission.MODE_SUBWAY) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['subway'], bgesYear)
    } else if (this.modeDeplacement === Mission.MODE_FERRY) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['boat']['ferry'], bgesYear)
    }
    return ef
  }

  getCarbonIntensity (bgesYear, isAllerRetour) {
    let cdistance = this.getCorrectedDistance()
    if (this.modeDeplacement === Mission.MODE_CAB) {
      cdistance = cdistance * (1 + 1 / this.nbPersonneVoiture)
    } else if (this.modeDeplacement === Mission.MODE_CAR) {
      cdistance = cdistance / this.nbPersonneVoiture
    }
    // choix du fe d'emission selon la distance corrigée
    let ef = this.getEmissionFactor(cdistance, bgesYear)
    let efnr = this.getEmissionFactor(cdistance, bgesYear, false)
    if (isAllerRetour) {
      cdistance = cdistance * 2
    }
    let total = cdistance * ef.total.total
    let totali = cdistance * ef.total.total * ef.total.uncertainty
    let totalnr = total
    let totalnri = totali
    // pour l'aviation, calcul avec et sans trainée
    if (this.modeDeplacement === Mission.MODE_PLANE) {
      totalnr = cdistance * efnr.total.total
      totalnri = cdistance * efnr.total.total * efnr.total.uncertainty
    }
    return {
      'total': total,
      'totali': totali,
      'totalnr': totalnr,
      'totalnri': totalnri,
      'distance': cdistance
    }
  }

  static createFromObj (section) {
    return new TravelSection(
      section.modeDeplacement,
      section.villeDepart,
      section.paysDepart,
      section.villeDestination,
      section.paysDestination,
      section.nbPersonneVoiture,
      section.distance,
      section.villeDepartLat,
      section.villeDepartLng,
      section.villeDestinationLat,
      section.villeDestinationLng,
      section.missionType
    )
  }

  static geodesicDist (lat1, lon1, lat2, lon2) {
    if ((lat1 === lat2) && (lon1 === lon2)) {
      return 0
    } else {
      var radlat1 = Math.PI * lat1 / 180
      var radlat2 = Math.PI * lat2 / 180
      var theta = lon1 - lon2
      var radtheta = Math.PI * theta / 180
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
      if (dist > 1) {
        dist = 1
      }
      dist = Math.acos(dist)
      dist = dist * 180 / Math.PI
      dist = dist * 60 * 1.1515
      dist = dist * 1.609344
      return dist
    }
  }
}
