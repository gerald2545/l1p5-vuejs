/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { filterEFByYear } from '@/mixins/utils'
import PURCHASES_FACTORS from '@/../data/purchasesFactors.json'

const MODULE_NAME = 'purchases'
const TRAVELLING_EXPENSES_CODES = ['xa01', 'xa02', 'xa11', 'xa12']
const WARNING_RATE = 20

export default class Purchase {
  constructor (pk, code, amount, source = null) {
    this.score = 1
    this.pk = pk
    this.code = this.setCode(code)
    this.amount = this.setAmount(amount)
    this.source = source
    this.module = this.setModule()
  }

  setCode (value) {
    let lvalue = value.toLowerCase().replace('.', '').trim()
    if (!Object.keys(PURCHASES_FACTORS).includes(lvalue)) {
      this.score = 2
    }
    return lvalue
  }

  setAmount (value) {
    let amount = value
    if (typeof amount === 'string') {
      amount = amount.replace(',', '.').trim().replace(' ', '')
    }
    if (!isNaN(amount)) {
      amount = parseFloat(amount)
    } else {
      this.score = 2
    }
    return amount
  }

  setModule () {
    let module = null
    try {
      let emission = this.getCarbonIntensity(2020)
      module = emission.module
      if (emission.module !== Purchase.MODULE_NAME) {
        this.score = 0
      }
    } catch {
      this.score = 2
    }
    return module
  }

  getCarbonIntensity (bgesYear) {
    let ci = this.getEmissionFactor(this.code, bgesYear)
    return {
      'module': this.getFactorModule(),
      'scope3': this.getScope3(),
      'category': this.getCategory(),
      'description': this.getDescription(),
      'total': this.amount * ci.total.total,
      'totali': this.amount * ci.total.total * ci.total.uncertainty
    }
  }

  getEmissionFactor (code, year) {
    return filterEFByYear(PURCHASES_FACTORS[code], year)
  }

  getFactorDescription () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].description_FR
    } else {
      return '-'
    }
  }

  getFactorModule () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].module
    } else {
      return null
    }
  }

  getScope3 () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].scope3
    } else {
      return null
    }
  }

  getCategory () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].category
    } else {
      return null
    }
  }

  getDescription () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].description_FR
    } else {
      return null
    }
  }

  getCodeFromDescription (value) {
    let lvalue = value.toLowerCase()
    let fcode = null
    for (let code of Object.keys(PURCHASES_FACTORS)) {
      if (PURCHASES_FACTORS[code].description_FR.toLowerCase() === lvalue) {
        fcode = code
      }
    }
    return fcode
  }

  isValid () {
    return Object.keys(PURCHASES_FACTORS).includes(this.code) && !isNaN(this.amount)
  }

  toDatabase () {
    return {
      'code': this.code,
      'amount': this.amount
    }
  }

  static getTravellingExpensesCodes () {
    return TRAVELLING_EXPENSES_CODES
  }

  static getWarningRate () {
    return WARNING_RATE
  }

  static getScope3 () {
    let scope3 = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].scope3
    })
    return Array.from(new Set(scope3)).filter(val => val !== null)
  }

  static getCodes () {
    let codes = Object.keys(PURCHASES_FACTORS)
    return Array.from(new Set(codes))
  }

  static getDescriptions () {
    let descriptions = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].description_FR
    })
    return Array.from(new Set(descriptions))
  }

  static get MODULE_NAME () {
    return MODULE_NAME
  }

  static createFromObj (item) {
    return new Purchase(
      item.pk,
      item.code,
      item.amount,
      item.source
    )
  }

  static compute (purchases, bgesYear) {
    let total = 0
    let totali = 0
    let meta = []
    let scope3 = {}
    let categories = {}
    let totalXA = 0
    for (let purchase of purchases) {
      let emission = purchase.getCarbonIntensity(bgesYear)
      if (emission.module === Purchase.MODULE_NAME) {
        total += emission.total
        totali += emission.totali
        if (emission.scope3 == null) {
          emission.scope3 = '9'
        }
        if (Object.keys(scope3).includes(emission.scope3.toString())) {
          scope3[emission.scope3.toString()].total += emission.total
          scope3[emission.scope3.toString()].totali += emission.totali
        } else {
          scope3[emission.scope3.toString()] = {
            'total': emission.total,
            'totali': emission.totali
          }
        }
        if (Object.keys(categories).includes(emission.category)) {
          categories[emission.category].total += emission.total
          categories[emission.category].totali += emission.totali
        } else {
          categories[emission.category] = {
            'total': emission.total,
            'totali': emission.totali
          }
        }
        if (TRAVELLING_EXPENSES_CODES.includes(purchase.code)) {
          totalXA += emission.total
        }
        meta.unshift({
          'code': purchase.code,
          'scope3': emission.scope3,
          'category': emission.category,
          'description': emission.description,
          'amount': purchase.amount,
          'total': emission.total,
          'totali': emission.totali
        })
      }
    }
    return {
      'total': total,
      'totali': totali,
      'meta': meta,
      'scope3': scope3,
      'categories': categories,
      'totalXA': totalXA
    }
  }
}
