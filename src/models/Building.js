/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { filterEFByYear } from '@/mixins/utils'
import HEATING_FACTORS from '@/../data/heatingFactors.json'
import ELECTRICITY_FACTORS from '@/../data/electricityFactors.json'
import REFRIGERANTS_FACTORS from '@/../data/refrigerantsFactors.json'

const URBAN_NETWORK = 'urban.network'

export default class Building {
  constructor (pk, nom, superficie, occupation, isOwnedByLab, chauffageElectrique, typeChauffage, chauffage,
    gazRefrigerant, reseauChauffageUrbain, electricite, autoProduction, autoconsommation, site) {
    this.pk = pk
    this.nom = nom
    this.superficie = superficie
    this.occupation = occupation
    this.isOwnedByLab = isOwnedByLab
    this.chauffageElectrique = chauffageElectrique
    this.typeChauffage = typeChauffage
    this.chauffage = chauffage
    this.gazRefrigerant = gazRefrigerant
    this.reseauChauffageUrbain = reseauChauffageUrbain
    this.electricite = electricite
    this.autoProduction = autoProduction
    this.autoconsommation = autoconsommation
    this.site = site
  }

  getHeatingConsumption () {
    let cTotale = 0
    if (this.chauffage.isMensuelle) {
      cTotale += parseInt(this.chauffage.cJanvier) + parseInt(this.chauffage.cFevrier)
      cTotale += parseInt(this.chauffage.cMars) + parseInt(this.chauffage.cAvril)
      cTotale += parseInt(this.chauffage.cMai) + parseInt(this.chauffage.cJuin)
      cTotale += parseInt(this.chauffage.cJuillet) + parseInt(this.chauffage.cAout)
      cTotale += parseInt(this.chauffage.cSeptembre) + parseInt(this.chauffage.cOctobre)
      cTotale += parseInt(this.chauffage.cNovembre) + parseInt(this.chauffage.cDecembre)
    } else {
      cTotale = this.chauffage.cTotale
    }
    return cTotale * this.occupation / 100
  }

  getElectricityConsumption () {
    let cTotale = 0
    if (this.electricite.isMensuelle) {
      cTotale += parseInt(this.electricite.cJanvier) + parseInt(this.electricite.cFevrier)
      cTotale += parseInt(this.electricite.cMars) + parseInt(this.electricite.cAvril)
      cTotale += parseInt(this.electricite.cMai) + parseInt(this.electricite.cJuin)
      cTotale += parseInt(this.electricite.cJuillet) + parseInt(this.electricite.cAout)
      cTotale += parseInt(this.electricite.cSeptembre) + parseInt(this.electricite.cOctobre)
      cTotale += parseInt(this.electricite.cNovembre) + parseInt(this.electricite.cDecembre)
    } else {
      cTotale = this.electricite.cTotale
    }
    if (this.autoProduction) {
      cTotale = (cTotale - this.autoconsommation) * this.occupation / 100
    } else {
      cTotale = cTotale * this.occupation / 100
    }
    return cTotale
  }

  getEmissionFactor (type, subtype, year) {
    let ef = null
    if (type === 'heating') {
      if (subtype === 'naturalgas' || subtype === 'propane') {
        ef = filterEFByYear(HEATING_FACTORS['gas'][subtype], year)
      } else if (subtype === 'heating.oil') {
        ef = filterEFByYear(HEATING_FACTORS['heating.oil'][subtype], year)
      } else if (subtype === 'biomethane') {
        ef = filterEFByYear(HEATING_FACTORS['biomethane'][subtype], year)
      } else if (subtype.startsWith('wood')) {
        ef = filterEFByYear(HEATING_FACTORS['wood.heating'][subtype], year)
      } else if (subtype.startsWith('network')) {
        ef = filterEFByYear(HEATING_FACTORS['urban.network'][subtype], year)
      }
    } else if (type === 'electricity') {
      ef = filterEFByYear(ELECTRICITY_FACTORS[subtype], year)
    } else if (type === 'refrigerants') {
      ef = filterEFByYear(REFRIGERANTS_FACTORS[subtype], year)
    }
    return ef
  }

  getHeatingCarbonIntensity (bgesYear) {
    let consommation = this.getHeatingConsumption()
    let emission = {
      'co2c': 0.00,
      'ch4c': 0.00,
      'n2oc': 0.00,
      'autrec': 0.00,
      'totalc': 0.00,
      'incertitudec': 0.00,
      'totala': 0.00,
      'incertitudea': 0.00
    }
    if (this.typeChauffage === Building.urbanNetwork) {
      let ef = this.getEmissionFactor('heating', this.reseauChauffageUrbain, bgesYear)
      emission = {
        'co2c': 0.00,
        'ch4c': 0.00,
        'n2oc': 0.00,
        'autrec': 0.00,
        'totalc': consommation * ef.total.total,
        'incertitudec': ef.total.uncertainty,
        'totala': 0.00,
        'incertitudea': 0.00
      }
    } else if (this.typeChauffage) {
      let ef = this.getEmissionFactor('heating', this.typeChauffage, bgesYear)
      emission = {
        'co2c': consommation * ef.combustion.co2,
        'ch4c': consommation * ef.combustion.ch4,
        'n2oc': consommation * ef.combustion.n2o,
        'autrec': consommation * ef.combustion.others,
        'totalc': consommation * ef.combustion.total,
        'incertitudec': ef.combustion.uncertainty,
        'totala': consommation * ef.upstream.total,
        'incertitudea': ef.upstream.uncertainty
      }
    }
    return emission
  }

  getElectricityCarbonIntensity (adminName, pays, bgesYear) {
    let ef = null
    if (pays === 'Saint-Martin') {
      ef = this.getEmissionFactor('electricity', 'st.martin', bgesYear)
    } else if (pays === 'Saint-Barthélémy') {
      ef = this.getEmissionFactor('electricity', 'st.barthelemy', bgesYear)
    } else if (pays === 'Polynésie Française') {
      if (adminName === 'Îles du Vent') {
        ef = this.getEmissionFactor('electricity', 'tahiti', bgesYear)
      } else {
        ef = this.getEmissionFactor('electricity', 'polynesie.outside.tahiti', bgesYear)
      }
    } else if (pays === 'Mayotte') {
      ef = this.getEmissionFactor('electricity', 'mayotte', bgesYear)
    } else if (pays === 'Guyane') {
      ef = this.getEmissionFactor('electricity', 'guyane', bgesYear)
    } else if (pays === 'Saint-Pierre et Miquelon') {
      ef = this.getEmissionFactor('electricity', 'st.pierre.et.miquelon', bgesYear)
    } else if (pays === 'Wallis-et-Futuna') {
      // wallis et f === st pierre et miquelon
      ef = this.getEmissionFactor('electricity', 'st.pierre.et.miquelon', bgesYear)
    } else if (pays === 'Terres australes françaises') {
      // TAAF === st pierre et miquelon
      ef = this.getEmissionFactor('electricity', 'st.pierre.et.miquelon', bgesYear)
    } else if (pays === 'Réunion') {
      ef = this.getEmissionFactor('electricity', 'reunion', bgesYear)
    } else if (pays === 'Guadeloupe') {
      ef = this.getEmissionFactor('electricity', 'guadeloupe', bgesYear)
    } else if (pays === 'Nouvelle-Calédonie') {
      // nouvelle caledonie === guadeloupe
      ef = this.getEmissionFactor('electricity', 'guadeloupe', bgesYear)
    } else if (pays === 'Martinique') {
      ef = this.getEmissionFactor('electricity', 'martinique', bgesYear)
    } else if (adminName === 'Corse') {
      ef = this.getEmissionFactor('electricity', 'corse', bgesYear)
    } else {
      ef = this.getEmissionFactor('electricity', 'france.continentale', bgesYear)
    }
    let consommation = this.getElectricityConsumption()
    return {
      'totalc': consommation * ef.combustion.total,
      'incertitudec': ef.combustion.uncertainty,
      'totala': consommation * ef.upstream.total,
      'incertitudea': ef.upstream.uncertainty
    }
  }

  getRefrigerantsCarbonIntensity (bgesYear) {
    let ef = null
    let total = 0
    let totali = 0
    for (let gaz of this.gazRefrigerant) {
      ef = this.getEmissionFactor('refrigerants', gaz.nom, bgesYear)
      total += gaz.cTotale * ef.total.total
      totali += gaz.cTotale * ef.total.total * ef.total.uncertainty
    }
    return {
      'total': total,
      'totali': totali
    }
  }

  static get urbanNetwork () {
    return URBAN_NETWORK
  }

  static getRefrigerants () {
    let refrigerants = Object.keys(REFRIGERANTS_FACTORS)
    return Array.from(new Set(refrigerants))
  }

  static getUrbanNetworks () {
    let urbanNetworks = []
    for (let network of Object.keys(HEATING_FACTORS['urban.network'])) {
      urbanNetworks.push({
        'network': network,
        'description_FR': HEATING_FACTORS['urban.network'][network]['description_FR'],
        'description_EN': HEATING_FACTORS['urban.network'][network]['description_EN']
      })
    }
    return urbanNetworks
  }

  static getHeatings () {
    let heatings = []
    for (let subtype of Object.keys(HEATING_FACTORS)) {
      if (subtype !== 'urban.network') {
        for (let subsubtype of Object.keys(HEATING_FACTORS[subtype])) {
          heatings.push(subsubtype)
        }
      }
    }
    heatings.unshift(URBAN_NETWORK)
    return Array.from(new Set(heatings))
  }

  static getHeatingUnit (type) {
    let unit = null
    if (type === 'naturalgas' || type === 'propane') {
      unit = HEATING_FACTORS['gas'][type].unit
    } else if (type === 'heating.oil') {
      unit = HEATING_FACTORS['heating.oil'][type].unit
    } else if (type === 'biomethane') {
      unit = HEATING_FACTORS['biomethane'][type].unit
    } else if (type.startsWith('wood')) {
      unit = HEATING_FACTORS['wood.heating'][type].unit
    } else if (type.startsWith('network')) {
      unit = HEATING_FACTORS['urban.network'][type].unit
    }
    return unit
  }

  static getDescription (type, subtype, lang = 'FR') {
    let desc = ''
    if (type === 'heating') {
      if (subtype === 'naturalgas' || subtype === 'propane') {
        desc = HEATING_FACTORS['gas'][subtype]['description_' + lang]
      } else if (subtype === 'heating.oil') {
        desc = HEATING_FACTORS['heating.oil'][subtype]['description_' + lang]
      } else if (subtype === 'biomethane') {
        desc = HEATING_FACTORS['biomethane'][subtype]['description_' + lang]
      } else if (subtype.startsWith('wood')) {
        desc = HEATING_FACTORS['wood.heating'][subtype]['description_' + lang]
      } else if (subtype.startsWith('network')) {
        desc = HEATING_FACTORS['urban.network'][subtype]['description_' + lang]
      }
    } else if (type === 'electricity') {
      desc = ELECTRICITY_FACTORS[subtype]['description_' + lang]
    } else if (type === 'refrigerants') {
      desc = REFRIGERANTS_FACTORS[subtype]['description_' + lang]
    }
    return desc
  }

  static createConsumption () {
    return {
      pk: null,
      cJanvier: 0,
      cFevrier: 0,
      cMars: 0,
      cAvril: 0,
      cMai: 0,
      cJuin: 0,
      cJuillet: 0,
      cAout: 0,
      cSeptembre: 0,
      cOctobre: 0,
      cNovembre: 0,
      cDecembre: 0,
      cTotale: 0,
      isMensuelle: true
    }
  }

  static createFromObj (building) {
    let heating = Building.createConsumption()
    if ('chauffage' in building) {
      heating = building.chauffage
    }
    return new Building(
      building.pk,
      building.nom,
      building.superficie,
      building.occupation,
      building.isOwnedByLab,
      building.chauffageElectrique,
      building.typeChauffage,
      heating,
      building.gazRefrigerant,
      building.reseauChauffageUrbain,
      building.electricite,
      building.autoProduction,
      building.autoconsommation,
      building.site
    )
  }

  static compute (buildings, adminName, pays, bgesYear) {
    let ccontroled = {
      'co2c': 0,
      'ch4c': 0,
      'n2oc': 0,
      'autrec': 0,
      'totalc': 0,
      'totalci': 0,
      'totala': 0,
      'totalai': 0,
      'total': 0,
      'totali': 0
    }
    let cuncontroled = {
      'co2c': 0,
      'ch4c': 0,
      'n2oc': 0,
      'autrec': 0,
      'totalc': 0,
      'totalci': 0,
      'totala': 0,
      'totalai': 0,
      'total': 0,
      'totali': 0
    }
    let emissions = {
      'chauffage': {
        'controled': ccontroled,
        'uncontroled': cuncontroled,
        'total': 0,
        'totali': 0
      },
      'electricite': {
        'totalc': 0,
        'totalci': 0,
        'totala': 0,
        'totalai': 0,
        'total': 0,
        'totali': 0
      },
      'gazRefrigerant': {
        'total': 0,
        'totali': 0
      },
      'meta': []
    }
    for (let building of buildings) {
      // emissions chauffage
      let eChauffage = building.getHeatingCarbonIntensity(bgesYear)
      if (building.isOwnedByLab) {
        emissions.chauffage.controled.co2c += eChauffage.co2c
        emissions.chauffage.controled.ch4c += eChauffage.ch4c
        emissions.chauffage.controled.n2oc += eChauffage.n2oc
        emissions.chauffage.controled.autrec += eChauffage.autrec
        emissions.chauffage.controled.totalc += eChauffage.totalc
        emissions.chauffage.controled.totalci += eChauffage.totalc * eChauffage.incertitudec
        emissions.chauffage.controled.totala += eChauffage.totala
        emissions.chauffage.controled.totalai += eChauffage.totala * eChauffage.incertitudea
      } else {
        emissions.chauffage.uncontroled.totalc += eChauffage.totalc
        emissions.chauffage.uncontroled.totalci += eChauffage.totalc * eChauffage.incertitudec
        emissions.chauffage.uncontroled.totala += eChauffage.totala
        emissions.chauffage.uncontroled.totalai += eChauffage.totala * eChauffage.incertitudea
      }
      emissions.chauffage.total += eChauffage.totalc + eChauffage.totala
      emissions.chauffage.totali += eChauffage.totalc * eChauffage.incertitudec + eChauffage.totala * eChauffage.incertitudea

      // emissions electricite
      let eElectricite = building.getElectricityCarbonIntensity(adminName, pays, bgesYear)
      emissions.electricite.totalc += eElectricite.totalc
      emissions.electricite.totalci += eElectricite.totalc * eElectricite.incertitudec
      emissions.electricite.totala += eElectricite.totala
      emissions.electricite.totalai += eElectricite.totala * eElectricite.incertitudea
      emissions.electricite.total += eElectricite.totalc + eElectricite.totala
      emissions.electricite.totali += eElectricite.totalc * eElectricite.incertitudec + eElectricite.totala * eElectricite.incertitudea

      // emissions gaz refrigerants
      let eGaz = building.getRefrigerantsCarbonIntensity(bgesYear)
      emissions.gazRefrigerant.total += eGaz.total
      emissions.gazRefrigerant.totali += eGaz.totali

      // all data for export
      emissions.meta.unshift({
        'nom': building.nom,
        'site': building.site,
        'superficie': building.superficie,
        'occupation': building.occupation,
        'isOwnedByLab': building.isOwnedByLab,
        'chauffageElectrique': building.chauffageElectrique,
        'typeChauffage': building.typeChauffage,
        'reseauChauffageUrbain': building.reseauChauffageUrbain,
        'autoProduction': building.autoProduction,
        'autoconsommation': building.autoconsommation,
        'htotal': eChauffage.totalc + eChauffage.totala,
        'htotali': eChauffage.totalc * eChauffage.incertitudec + eChauffage.totala * eChauffage.incertitudea,
        'etotal': eElectricite.totalc + eElectricite.totala,
        'etotali': eElectricite.totalc * eElectricite.incertitudec + eElectricite.totala * eElectricite.incertitudea,
        'rtotal': eGaz.total,
        'rtotali': eGaz.totali
      })
    }
    return emissions
  }
}
