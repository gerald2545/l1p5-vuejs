
/* eslint-disable camelcase */

import { device_utils } from '@/ext/ecodiag/device_utils.js'

export default class ComputerDevice {
  constructor (pk, type, model, amount, acquisitionYear, ecodiagObject = undefined) {
    this.pk = pk
    this.type = type
    this.model = model
    this.amount = amount
    this.acquisitionYear = parseInt(acquisitionYear)
    this.ecodiagObject = ecodiagObject
  }

  toEcodiag () {
    if (!this.ecodiagObject) {
      this.ecodiagObject = device_utils.methods.create_device_item(this.type, {
        pk: this.pk,
        model: this.model,
        nb: this.amount,
        year: this.acquisitionYear,
        score: 2
      })
    }
    return this.ecodiagObject
  }

  toDatabase () {
    return {
      'type': this.type,
      'model': this.model,
      'amount': this.amount,
      'acquisitionYear': this.acquisitionYear
    }
  }

  getEmissions () {
    let up = [90]
    let emissions = device_utils.methods.compute_device_co2e(this.toEcodiag(), 'flux', up)
    let incertitude = (emissions.sups[0] - emissions.infs[0]) / 2.0
    return {
      'totala': emissions.grey,
      'incertitudea': incertitude
    }
  }

  static createFromObj (device) {
    return new ComputerDevice(
      device.pk,
      device.type,
      device.model,
      device.amount,
      device.acquisitionYear,
      device.ecodiagObject
    )
  }

  static compute (devices) {
    let total = 0
    let totali = 0
    let meta = []
    for (let device of devices) {
      let emissions = device.getEmissions()
      total += emissions.totala
      totali += emissions.incertitudea
      meta.unshift({
        'type': device.type,
        'model': device.model,
        'amount': device.amount,
        'total': emissions.totala,
        'totali': emissions.incertitudea
      })
    }
    return {
      'total': total,
      'totali': totali,
      'meta': meta
    }
  }

  static fromEcodiag (ecodiagObject) {
    return new ComputerDevice(
      null,
      ecodiagObject.type,
      ecodiagObject.model,
      ecodiagObject.nb,
      ecodiagObject.year,
      ecodiagObject
    )
  }
}
