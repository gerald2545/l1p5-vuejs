/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { filterEFByYear } from '@/mixins/utils'
import VEHICLES_FACTORS from '@/../data/vehiclesFactors.json'
import TRANSPORTS_FACTORS from '@/../data/transportsFactors.json'

const TRAVELS_MODES = [
  'marche',
  'velo',
  'veloElectrique',
  'trotinetteElectrique',
  'deRouesMotorise',
  'voiture',
  'bus',
  'tramway',
  'train',
  'rer',
  'metro'
]

const MAX_DISTANCES = {
  'marche': 60,
  'velo': 200,
  'veloElectrique': 200,
  'trotinetteElectrique': 200,
  'deRouesMotorise': 2000,
  'voiture': 2000,
  'bus': 2000,
  'tramway': 300,
  'train': 2000,
  'rer': 2000,
  'metro': 250
}

const ICONS = {
  'marche': 'walk',
  'velo': 'bicycle',
  'veloElectrique': 'electric-bike',
  'trotinetteElectrique': 'electric-scooter',
  'deRouesMotorise': 'motorcycle',
  'voiture': 'car',
  'bus': 'bus',
  'tramway': 'tram',
  'train': 'train',
  'rer': 'rer',
  'metro': 'subway'
}

const ICONS_PACK = {
  'marche': 'icomoon',
  'velo': 'sui',
  'veloElectrique': 'icomoon',
  'trotinetteElectrique': 'icomoon',
  'deRouesMotorise': 'icomoon',
  'voiture': 'sui',
  'bus': 'sui',
  'tramway': 'icomoon',
  'train': 'sui',
  'rer': 'icomoon',
  'metro': 'icomoon'
}

const NUMBER_OF_WORKED_WEEK = {
  default: 41,
  2020: 27,
  2021: 37
}

export default class Commute {
  constructor (seqID, statut, nbJourTravail, message, marche, velo, veloElectrique, trotinetteElectrique,
    deRouesMotorise, voiture, bus, tramway, train, rer, metro, nbPersonne2roues, nbPersonneVoiture, motorisation,
    has2TypeDay, nbDayType2, marche2, velo2, veloElectrique2, trotinetteElectrique2, deRouesMotorise2, voiture2,
    bus2, tramway2, train2, rer2, metro2, nbPersonne2roues2, nbPersonneVoiture2, motorisation2, deleted) {
    this.seqID = seqID
    this.statut = statut
    this.nbJourTravail = nbJourTravail
    this.message = message

    this.marche = marche
    this.velo = velo
    this.veloElectrique = veloElectrique
    this.trotinetteElectrique = trotinetteElectrique
    this.deRouesMotorise = deRouesMotorise
    this.voiture = voiture
    this.bus = bus
    this.tramway = tramway
    this.train = train
    this.rer = rer
    this.metro = metro
    this.nbPersonne2roues = nbPersonne2roues
    this.nbPersonneVoiture = nbPersonneVoiture
    this.motorisation = motorisation

    this.has2TypeDay = has2TypeDay
    this.nbDayType2 = nbDayType2
    this.marche2 = marche2
    this.velo2 = velo2
    this.veloElectrique2 = veloElectrique2
    this.trotinetteElectrique2 = trotinetteElectrique2
    this.deRouesMotorise2 = deRouesMotorise2
    this.voiture2 = voiture2
    this.bus2 = bus2
    this.tramway2 = tramway2
    this.train2 = train2
    this.rer2 = rer2
    this.metro2 = metro2
    this.nbPersonne2roues2 = nbPersonne2roues2
    this.nbPersonneVoiture2 = nbPersonneVoiture2
    this.motorisation2 = motorisation2

    this.deleted = deleted
    this.isValid = true
  }

  getNumberOfWorkedWeek (year) {
    let nbWeek = NUMBER_OF_WORKED_WEEK.default
    if (year in NUMBER_OF_WORKED_WEEK) {
      nbWeek = NUMBER_OF_WORKED_WEEK[year]
    }
    return nbWeek
  }

  isCarpooling () {
    let isCarpooling = false
    if (this.voiture > 0 && this.nbPersonneVoiture > 1) {
      isCarpooling = true
    } else if (this.voiture2 > 0 && this.nbPersonneVoiture2 > 1) {
      isCarpooling = true
    } else if (this.deRouesMotorise > 0 && this.nbPersonne2roues > 1) {
      isCarpooling = true
    } else if (this.deRouesMotorise2 > 0 && this.nbPersonne2roues2 > 1) {
      isCarpooling = true
    }
    return isCarpooling
  }

  getNbDayType1 () {
    let nbDayType1 = this.nbJourTravail
    if (this.has2TypeDay) {
      nbDayType1 = this.nbJourTravail - this.nbDayType2
    }
    return nbDayType1
  }

  getRawDay1Distance () {
    let total = 0
    for (let mode of TRAVELS_MODES) {
      if (!isNaN(this[mode])) {
        total += parseFloat(this[mode])
      }
    }
    if (this.has2TypeDay) {
      total = total * (this.nbJourTravail - this.nbDayType2)
    } else {
      total = total * this.nbJourTravail
    }
    return total
  }

  getRawDay2Distance () {
    let total = 0
    if (this.has2TypeDay) {
      for (let mode of TRAVELS_MODES) {
        let mkey = mode + '2'
        if (!isNaN(this[mkey])) {
          total += parseFloat(this[mkey])
        }
      }
    }
    return total * this.nbDayType2
  }

  getRawWeekDistance () {
    return this.getRawDay1Distance() + this.getRawDay2Distance()
  }

  getDistanceTotale (bgesYear = null) {
    return Object.values(this.getCarbonIntensity(0, bgesYear)['distance']).reduce((a, b) => a + b)
  }

  getEmissionsTotale (bgesYear = null) {
    return Object.values(this.getCarbonIntensity(0, bgesYear)['total']).reduce((a, b) => a + b)
  }

  getEmissionsITotale (bgesYear = null) {
    return Object.values(this.getCarbonIntensity(0, bgesYear)['totali']).reduce((a, b) => a + b)
  }

  getEmissionFactor (mode, motorisation, tailleAglomeration, bgesYear) {
    let ef = null
    if (mode === 'marche') {
      ef = {
        'total': {
          'co2': 0.0000,
          'ch4': 0.0000,
          'n2o': 0.0000,
          'others': 0.0000,
          'unit': 'kgCO2e/passager.km',
          'total': 0.0000,
          'uncertainty': 0.0
        }
      }
    } else if (mode === 'velo') {
      ef = filterEFByYear(VEHICLES_FACTORS['bike']['muscular'], bgesYear)
    } else if (mode === 'veloElectrique') {
      ef = filterEFByYear(VEHICLES_FACTORS['bike']['electric'], bgesYear)
    } else if (mode === 'trotinetteElectrique') {
      ef = filterEFByYear(VEHICLES_FACTORS['scooter']['electric'], bgesYear)
    } else if (mode === 'deRouesMotorise') {
      ef = filterEFByYear(VEHICLES_FACTORS['motorbike']['gasoline'], bgesYear)
    } else if (mode === 'voiture') {
      ef = filterEFByYear(VEHICLES_FACTORS['car'][motorisation], bgesYear)
    } else if (mode === 'bus') {
      if (tailleAglomeration === 0) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.smallcity'], bgesYear)
      } else if (tailleAglomeration === 1) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.mediumcity'], bgesYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.bigcity'], bgesYear)
      }
    } else if (mode === 'train') {
      // The survey asks for dailyDistance, to choose the emission
      // factor, we considere its go and back
      let commuteDistance = this[mode] / 2
      if (commuteDistance < 200) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.shortdistance'], bgesYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.longdistance'], bgesYear)
      }
    } else if (mode === 'tramway') {
      if (tailleAglomeration === 0) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['tram.mediumcity'], bgesYear)
      } else if (tailleAglomeration === 1) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['tram.mediumcity'], bgesYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['tram.bigcity'], bgesYear)
      }
    } else if (mode === 'rer') {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['rer'], bgesYear)
    } else if (mode === 'metro') {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['subway'], bgesYear)
    }
    return ef
  }

  getCarbonIntensity (tailleAglomeration, bgesYear = null) {
    let emissionsParModes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let emissionsIParModes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let distancesParModes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let rawDistancesParModes1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let rawDistancesParModes2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let index = 0
    if (!this.deleted) {
      for (let mode of TRAVELS_MODES) {
        if (this[mode] !== 0 && !isNaN(this[mode])) {
          let ci = this.getEmissionFactor(mode, this.motorisation, tailleAglomeration, bgesYear)
          let distance = this[mode]
          if (mode === 'voiture') {
            distance = distance / this.nbPersonneVoiture
          } else if (mode === 'deRouesMotorise') {
            distance = distance / this.nbPersonne2roues
          }
          let faAnnuel = this.nbJourTravail * this.getNumberOfWorkedWeek(bgesYear)
          if (this.has2TypeDay) {
            faAnnuel = (this.nbJourTravail - this.nbDayType2) * this.getNumberOfWorkedWeek(bgesYear)
          }
          rawDistancesParModes1[index] += this[mode]
          distancesParModes[index] += this[mode] * faAnnuel
          emissionsParModes[index] += distance * faAnnuel * ci.total.total
          emissionsIParModes[index] += distance * faAnnuel * ci.total.total * ci.total.uncertainty
        }
        if (this.has2TypeDay) {
          if (this[mode + '2'] !== 0 && !isNaN(this[mode + '2'])) {
            let ci2 = this.getEmissionFactor(mode, this.motorisation2, tailleAglomeration, bgesYear)
            let distance2 = this[mode + '2']
            if (mode === 'voiture') {
              distance2 = distance2 / this.nbPersonneVoiture2
            } else if (mode === 'deRouesMotorise') {
              distance2 = distance2 / this.nbPersonne2roues2
            }
            let faAnnuel2 = this.nbDayType2 * this.getNumberOfWorkedWeek(bgesYear)
            rawDistancesParModes2[index] += this[mode + '2']
            distancesParModes[index] += this[mode + '2'] * faAnnuel2
            emissionsParModes[index] += distance2 * faAnnuel2 * ci2.total.total
            emissionsIParModes[index] += distance2 * faAnnuel2 * ci2.total.total * ci2.total.uncertainty
          }
        }
        index += 1
      }
    }
    return ({
      'total': emissionsParModes,
      'totali': emissionsIParModes,
      'distance': distancesParModes,
      'rawDistance1': rawDistancesParModes1,
      'rawDistance2': rawDistancesParModes2
    })
  }

  toDatabase () {
    let nbDayType2 = 0
    if (this.nbDayType2 !== null) {
      nbDayType2 = this.nbDayType2
    }
    return {
      'seqID': this.seqID,
      'statut': this.statut,
      'nbJourTravail': this.nbJourTravail,
      'message': this.message,
      'marche': this.marche,
      'velo': this.velo,
      'veloElectrique': this.veloElectrique,
      'trotinetteElectrique': this.trotinetteElectrique,
      'deRouesMotorise': this.deRouesMotorise,
      'voiture': this.voiture,
      'bus': this.bus,
      'tramway': this.tramway,
      'train': this.train,
      'rer': this.rer,
      'metro': this.metro,
      'nbPersonne2roues': this.nbPersonne2roues,
      'nbPersonneVoiture': this.nbPersonneVoiture,
      'motorisation': this.motorisation,
      'has2TypeDay': this.has2TypeDay,
      'nbDayType2': nbDayType2,
      'marche2': this.marche2,
      'velo2': this.velo2,
      'veloElectrique2': this.veloElectrique2,
      'trotinetteElectrique2': this.trotinetteElectrique2,
      'deRouesMotorise2': this.deRouesMotorise2,
      'voiture2': this.voiture2,
      'bus2': this.bus2,
      'tramway2': this.tramway2,
      'train2': this.train2,
      'rer2': this.rer2,
      'metro2': this.metro2,
      'nbPersonne2roues2': this.nbPersonne2roues2,
      'nbPersonneVoiture2': this.nbPersonneVoiture2,
      'motorisation2': this.motorisation2,
      'deleted': this.deleted
    }
  }

  static get travelsModes () {
    return TRAVELS_MODES
  }

  static getIcon (mode) {
    return ICONS[mode]
  }

  static getIconPack (mode) {
    return ICONS_PACK[mode]
  }

  static getMaxDistance (mode) {
    return MAX_DISTANCES[mode]
  }

  static createFromObj (commute) {
    return new Commute(
      commute.seqID,
      commute.statut,
      commute.nbJourTravail,
      commute.message,
      commute.marche,
      commute.velo,
      commute.veloElectrique,
      commute.trotinetteElectrique,
      commute.deRouesMotorise,
      commute.voiture,
      commute.bus,
      commute.tramway,
      commute.train,
      commute.rer,
      commute.metro,
      commute.nbPersonne2roues,
      commute.nbPersonneVoiture,
      commute.motorisation,
      commute.has2TypeDay,
      commute.nbDayType2,
      commute.marche2,
      commute.velo2,
      commute.veloElectrique2,
      commute.trotinetteElectrique2,
      commute.deRouesMotorise2,
      commute.voiture2,
      commute.bus2,
      commute.tramway2,
      commute.train2,
      commute.rer2,
      commute.metro2,
      commute.nbPersonne2roues2,
      commute.nbPersonneVoiture2,
      commute.motorisation2,
      commute.deleted
    )
  }

  static compute (commutes, tailleAglomeration, nbChercheurs, nbEnseignants, nbITA, nbPostDoc, bgesYear) {
    let emissionsTotal = {
      'researcher': new Array(Commute.travelsModes.length).fill(0),
      'engineer': new Array(Commute.travelsModes.length).fill(0),
      'student': new Array(Commute.travelsModes.length).fill(0)
    }
    let iemissionsTotal = {
      'researcher': new Array(Commute.travelsModes.length).fill(0),
      'engineer': new Array(Commute.travelsModes.length).fill(0),
      'student': new Array(Commute.travelsModes.length).fill(0)
    }
    let distancesTotal = {
      'researcher': new Array(Commute.travelsModes.length).fill(0),
      'engineer': new Array(Commute.travelsModes.length).fill(0),
      'student': new Array(Commute.travelsModes.length).fill(0)
    }
    let cdays = {
      'researcher': 0,
      'engineer': 0,
      'student': 0
    }
    let totalDpt = []
    let totalDpti = []
    let rawDistance1 = []
    let rawDistance2 = []
    let nbResearcherAnsw = 0
    let nbEngineerAnsw = 0
    let nbStudentAnsw = 0
    let meta = []
    for (let commute of commutes) {
      if (!commute.deleted) {
        let emDi = commute.getCarbonIntensity(tailleAglomeration, bgesYear)
        totalDpt.unshift([commute.seqID].concat(emDi.total))
        totalDpti.unshift([commute.seqID].concat(emDi.totali))
        rawDistance1.unshift(emDi.rawDistance1)
        rawDistance2.unshift(emDi.rawDistance2)
        meta.unshift({
          'seqID': commute.seqID,
          'nbJourTravail': commute.nbJourTravail,
          'statut': commute.statut,
          'nbDayType2': commute.nbDayType2
        })
        for (let index = 0; index < emDi.total.length; index++) {
          if (commute.statut === 'researcher') {
            nbResearcherAnsw += 1
            emissionsTotal.researcher[index] += emDi.total[index]
            iemissionsTotal.researcher[index] += emDi.totali[index]
            distancesTotal.researcher[index] += emDi.distance[index]
            cdays.researcher += commute.nbJourTravail * commute.getNumberOfWorkedWeek(bgesYear)
          } else if (commute.statut === 'engineer') {
            nbEngineerAnsw += 1
            emissionsTotal.engineer[index] += emDi.total[index]
            iemissionsTotal.engineer[index] += emDi.totali[index]
            distancesTotal.engineer[index] += emDi.distance[index]
            cdays.engineer += commute.nbJourTravail * commute.getNumberOfWorkedWeek(bgesYear)
          } else {
            nbStudentAnsw += 1
            emissionsTotal.student[index] += emDi.total[index]
            iemissionsTotal.student[index] += emDi.totali[index]
            distancesTotal.student[index] += emDi.distance[index]
            cdays.student += commute.nbJourTravail * commute.getNumberOfWorkedWeek(bgesYear)
          }
        }
      }
    }
    nbResearcherAnsw = nbResearcherAnsw / 11
    nbEngineerAnsw = nbEngineerAnsw / 11
    nbStudentAnsw = nbStudentAnsw / 11
    // normalise les emissions par le nombre de personnes dans le labo
    for (let index = 0; index < emissionsTotal.researcher.length; index++) {
      if (nbResearcherAnsw !== 0) {
        emissionsTotal.researcher[index] = emissionsTotal.researcher[index] * (nbChercheurs + nbEnseignants) / nbResearcherAnsw
        iemissionsTotal.researcher[index] = iemissionsTotal.researcher[index] * (nbChercheurs + nbEnseignants) / nbResearcherAnsw
        distancesTotal.researcher[index] = distancesTotal.researcher[index] * (nbChercheurs + nbEnseignants) / nbResearcherAnsw
      } else {
        emissionsTotal.researcher[index] = 0
        iemissionsTotal.researcher[index] = 0
        distancesTotal.researcher[index] = 0
      }
      if (nbEngineerAnsw !== 0) {
        emissionsTotal.engineer[index] = emissionsTotal.engineer[index] * nbITA / nbEngineerAnsw
        iemissionsTotal.engineer[index] = iemissionsTotal.engineer[index] * nbITA / nbEngineerAnsw
        distancesTotal.engineer[index] = distancesTotal.engineer[index] * nbITA / nbEngineerAnsw
      } else {
        emissionsTotal.engineer[index] = 0
        iemissionsTotal.engineer[index] = 0
        distancesTotal.engineer[index] = 0
      }
      if (nbStudentAnsw !== 0) {
        emissionsTotal.student[index] = emissionsTotal.student[index] * nbPostDoc / nbStudentAnsw
        iemissionsTotal.student[index] = iemissionsTotal.student[index] * nbPostDoc / nbStudentAnsw
        distancesTotal.student[index] = distancesTotal.student[index] * nbPostDoc / nbStudentAnsw
      } else {
        emissionsTotal.student[index] = 0
        iemissionsTotal.student[index] = 0
        distancesTotal.student[index] = 0
      }
    }
    let total = 0
    let totali = 0
    for (let index = 0; index < emissionsTotal.researcher.length; index++) {
      total += emissionsTotal.researcher[index] + emissionsTotal.engineer[index] + emissionsTotal.student[index]
      totali += iemissionsTotal.researcher[index] + iemissionsTotal.engineer[index] + iemissionsTotal.student[index]
    }
    return {
      'total': total,
      'totali': totali,
      'totalStatuts': emissionsTotal,
      'totalStatutsi': iemissionsTotal,
      'distanceStatuts': distancesTotal,
      'totalDpt': totalDpt,
      'totalDpti': totalDpti,
      'rawDistance1': rawDistance1,
      'rawDistance2': rawDistance2,
      'meta': meta,
      'nbAnswers': {
        'researcher': nbResearcherAnsw,
        'engineer': nbEngineerAnsw,
        'student': nbStudentAnsw
      },
      'cdays': cdays
    }
  }
}
