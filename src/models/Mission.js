/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { filterEFByYear } from '@/mixins/utils'
import VEHICLES_FACTORS from '@/../data/vehiclesFactors.json'
import TRANSPORTS_FACTORS from '@/../data/transportsFactors.json'

import ISO3166 from '../../data/ISO3166.json'

const PURPOSE_FIELD_STUDY = 'field.study'
const PURPOSE_CONFERENCE = 'conference'
const PURPOSE_SEMINAR = 'seminar'
const PURPOSE_TEACHING = 'teaching'
const PURPOSE_COLLABORATION = 'collaboration'
const PURPOSE_VISIT = 'visit'
const PURPOSE_RESEARCH_MANAGEMENT = 'research.management'
const PURPOSE_OTHER = 'other'

const STATUS_GUEST = 'guest'
const STATUS_RESEARCHER = 'researcher'
const STATUS_ENGINEER = 'engineer'
const STATUS_STUDENT = 'student'

const MODE_PLANE = 'plane'
const MODE_TRAIN = 'train'
const MODE_CAR = 'car'
const MODE_CAB = 'cab'
const MODE_BUS = 'bus'
const MODE_TRAM = 'tram'
const MODE_RER = 'rer'
const MODE_SUBWAY = 'subway'
const MODE_FERRY = 'ferry'

const MODES_DEPLACEMENTS = {
  [MODE_PLANE]: 1.0,
  [MODE_TRAIN]: 1.2,
  [MODE_CAR]: 1.3,
  [MODE_CAB]: 1.3,
  [MODE_BUS]: 1.5,
  [MODE_TRAM]: 1.5,
  [MODE_RER]: 1.2,
  [MODE_SUBWAY]: 1.7,
  [MODE_FERRY]: 1.0
}

const PURPOSE_DICTIONARY = {
  [PURPOSE_FIELD_STUDY]: PURPOSE_FIELD_STUDY,
  'field study': PURPOSE_FIELD_STUDY,
  'etude de terrain': PURPOSE_FIELD_STUDY,
  'etude terrain': PURPOSE_FIELD_STUDY,
  [PURPOSE_CONFERENCE]: PURPOSE_CONFERENCE,
  'colloque-congres': PURPOSE_CONFERENCE,
  'colloque': PURPOSE_CONFERENCE,
  'congres': PURPOSE_CONFERENCE,
  [PURPOSE_SEMINAR]: PURPOSE_SEMINAR,
  'seminaire': PURPOSE_SEMINAR,
  [PURPOSE_TEACHING]: PURPOSE_TEACHING,
  'enseignement': PURPOSE_TEACHING,
  [PURPOSE_COLLABORATION]: PURPOSE_COLLABORATION,
  [PURPOSE_VISIT]: PURPOSE_VISIT,
  'visite': PURPOSE_VISIT,
  [PURPOSE_RESEARCH_MANAGEMENT]: PURPOSE_RESEARCH_MANAGEMENT,
  'research management': PURPOSE_RESEARCH_MANAGEMENT,
  'administration de la recherche': PURPOSE_RESEARCH_MANAGEMENT,
  'administration': PURPOSE_RESEARCH_MANAGEMENT,
  [PURPOSE_OTHER]: PURPOSE_OTHER,
  'autre': PURPOSE_OTHER
}

const STATUS_DICTIONARY = {
  [STATUS_GUEST]: STATUS_GUEST,
  'personne invitee': STATUS_GUEST,
  'invited person': STATUS_GUEST,
  [STATUS_RESEARCHER]: STATUS_RESEARCHER,
  'chercheur.e-ec': STATUS_RESEARCHER,
  [STATUS_ENGINEER]: STATUS_ENGINEER,
  'ita': STATUS_ENGINEER,
  'engineers-technicians-administrative staff': STATUS_ENGINEER,
  'engineer': STATUS_ENGINEER,
  'technician': STATUS_ENGINEER,
  'administrative': STATUS_ENGINEER,
  [STATUS_STUDENT]: STATUS_STUDENT,
  'doc-post doc': STATUS_STUDENT,
  'phd-post doctoral fellow': STATUS_STUDENT,
  'phd': STATUS_STUDENT,
  'post-doc': STATUS_STUDENT,
  'post doc': STATUS_STUDENT
}

const MODE_DICTIONARY = {
  'avion': MODE_PLANE,
  'plane': MODE_PLANE,
  'train': MODE_TRAIN,
  'voiture': MODE_CAR,
  'location de vehicule': MODE_CAR,
  'vehicule personnel': MODE_CAR,
  'car': MODE_CAR,
  'taxi': MODE_CAB,
  'cab': MODE_CAB,
  'bus': MODE_BUS,
  'tram': MODE_TRAM,
  'tramway': MODE_TRAM,
  'rer': MODE_RER,
  'metro': MODE_SUBWAY,
  'subway': MODE_SUBWAY,
  'ferry': MODE_FERRY
}

const ROUND_TRIP_DICTIONARY = {
  'oui': true,
  'o': true,
  'yes': true,
  'y': true,
  'non': false,
  'no': false,
  'n': false
}

const ICONS = {
  [MODE_PLANE]: 'plane',
  [MODE_TRAIN]: 'train',
  [MODE_CAR]: 'car',
  [MODE_CAB]: 'cab',
  [MODE_BUS]: 'bus',
  [MODE_TRAM]: 'tram',
  [MODE_RER]: 'rer',
  [MODE_SUBWAY]: 'subway',
  [MODE_FERRY]: 'ship'
}

const ICONS_PACK = {
  [MODE_PLANE]: 'sui',
  [MODE_TRAIN]: 'sui',
  [MODE_CAR]: 'sui',
  [MODE_CAB]: 'sui',
  [MODE_BUS]: 'sui',
  [MODE_TRAM]: 'icomoon',
  [MODE_RER]: 'icomoon',
  [MODE_SUBWAY]: 'icomoon',
  [MODE_FERRY]: 'sui'
}

export default class Mission {
  constructor (idMission, dateDepart, villeDepart, paysDepart, villeDestination,
    paysDestination, localisation, modeDeplacement, nbPersonneVoiture,
    motifDeplacement, statut, isAllerRetour, villeDepartLat = null,
    villeDepartLng = null, villeDestinationLat = null, villeDestinationLng = null,
    distance = null, missionType = null, nbEffectue = 1, forceInvalid = false,
    source = null) {
    if (Array.isArray(idMission)) {
      this.idMission = idMission
    } else {
      this.idMission = [idMission]
    }
    this.isValid = true
    this.setDateDepart(dateDepart)
    this.villeDepart = villeDepart
    this.setPaysDepart(paysDepart)
    this.villeDestination = villeDestination
    this.setPaysDestination(paysDestination)
    this.localisation = localisation
    this.setTravelsMode(modeDeplacement)
    this.setNbPersonneVoiture(nbPersonneVoiture)
    this.setTravelPurpose(motifDeplacement)
    this.setStatus(statut)
    this.setIsRoundTrip(isAllerRetour)
    this.villeDepartLat = villeDepartLat
    this.villeDepartLng = villeDepartLng
    this.villeDestinationLat = villeDestinationLat
    this.villeDestinationLng = villeDestinationLng
    this.distance = distance
    if (missionType !== null) {
      this.missionType = missionType
    } else {
      this.computeType()
    }
    this.nbEffectue = nbEffectue
    this.source = source
    if (forceInvalid) {
      this.isValid = false
    }
    if (this.isFromDatabase()) {
      this.isValid = true
      this.source = 'database'
    }
  }

  setDateDepart (value) {
    /* let dateDepart = null
    try {
      let dateParts = value.split('/')
      if (dateParts.length === 3) {
        dateDepart = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0]
      } else {
        dateDepart = value
      }
      let d = new Date(dateDepart)
      if (isNaN(d.getTime())) {
        this.isValid = false
      }
    } catch (error) {
      this.isValid = false
    }
    this.dateDepart = dateDepart */
    this.dateDepart = value
  }

  setTravelsMode (value) {
    this.modeDeplacement = Mission.getTravelsMode(value)
    if (this.modeDeplacement === null) {
      this.isValid = false
    }
  }

  setTravelPurpose (value) {
    this.motifDeplacement = Mission.getTravelPurpose(value)
  }

  setStatus (value) {
    this.statut = Mission.getStatus(value)
  }

  setPaysDepart (value) {
    this.paysDepart = Mission.paysISO3166(value)
    if (this.paysDepart === null) {
      this.isValid = false
    }
  }

  setPaysDestination (value) {
    this.paysDestination = Mission.paysISO3166(value)
    if (this.paysDestination === null) {
      this.isValid = false
    }
  }

  setIsRoundTrip (value) {
    if (typeof value === 'boolean') {
      this.isAllerRetour = value
    } else {
      this.isAllerRetour = Mission.getIsRoundTrip(value)
      if (typeof this.isAllerRetour !== 'boolean') {
        this.isValid = false
      }
    }
  }

  setNbPersonneVoiture (value) {
    if (isNaN(value) || value === null || value === 'null') {
      this.nbPersonneVoiture = 1
    } else if (value === '0' || value === 0) {
      this.nbPersonneVoiture = parseInt(value)
      this.isValid = false
    } else {
      this.nbPersonneVoiture = parseInt(value)
    }
  }

  isEqualTo (mission) {
    let isEqual = false
    if (isNaN(mission.nbPersonneVoiture)) {
      mission.nbPersonneVoiture = 1
    }
    if (this.distance === mission.distance && this.isAllerRetour === mission.isAllerRetour &&
      this.villeDepart === mission.villeDepart && this.paysDepart === mission.paysDepart &&
      this.villeDestination === mission.villeDestination && this.paysDestination === mission.paysDestination &&
      this.missionType === mission.missionType && this.localisation === mission.localisation &&
      this.modeDeplacement === mission.modeDeplacement && this.nbPersonneVoiture === mission.nbPersonneVoiture &&
      this.motifDeplacement === mission.motifDeplacement && this.statut === mission.statut &&
      this.source === mission.source) {
      isEqual = true
    }
    return isEqual
  }

  add (idMission) {
    if (Array.isArray(idMission)) {
      this.idMission = this.idMission.concat(idMission)
      this.nbEffectue += idMission.length
    } else {
      this.idMission.unshift(idMission)
      this.nbEffectue += 1
    }
  }

  computeGeodesicDist (villeDepartLat, villeDepartLng, villeDestinationLat, villeDestinationLng) {
    this.villeDepartLat = villeDepartLat
    this.villeDepartLng = villeDepartLng
    this.villeDestinationLat = villeDestinationLat
    this.villeDestinationLng = villeDestinationLng
    if (villeDepartLat !== null && villeDepartLng !== null && villeDestinationLat !== null && villeDestinationLng !== null) {
      this.distance = Mission.geodesicDist(villeDepartLat, villeDepartLng, villeDestinationLat, villeDestinationLng)
    } else {
      this.distance = null
    }
  }

  computeType () {
    this.missionType = 'IN'
    if (this.paysDepart === 'FR' && this.paysDestination === 'FR') {
      this.missionType = 'NA'
    } else if (this.paysDepart === 'FR' || this.paysDestination === 'FR') {
      this.missionType = 'MX'
    }
  }

  getEmissionFactor (distance, bgesYear, reglementaire = true) {
    let ef = null
    if (reglementaire && this.modeDeplacement === MODE_PLANE) {
      if (distance < 1000) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['shorthaul'], bgesYear)
      } else if (distance < 3501) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['mediumhaul'], bgesYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['longhaul'], bgesYear)
      }
    } else if (!reglementaire && this.modeDeplacement === MODE_PLANE) {
      if (distance < 1000) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['shorthaul.contrails'], bgesYear)
      } else if (distance < 3501) {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['mediumhaul.contrails'], bgesYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['plane']['longhaul.contrails'], bgesYear)
      }
    } else if (this.modeDeplacement === Mission.MODE_CAR || this.modeDeplacement === Mission.MODE_CAB) {
      ef = filterEFByYear(VEHICLES_FACTORS['car']['unknown.engine'], bgesYear)
    } else if (this.modeDeplacement === Mission.MODE_BUS) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['bus']['bus.bigcity'], bgesYear)
    } else if (this.modeDeplacement === MODE_TRAIN) {
      if (this.missionType === 'NA') {
        if (distance < 200) {
          ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.shortdistance'], bgesYear)
        } else {
          ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.longdistance'], bgesYear)
        }
      } else if (this.missionType === 'MX') {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.mixed'], bgesYear)
      } else {
        ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['train.international'], bgesYear)
      }
    } else if (this.modeDeplacement === Mission.MODE_TRAM) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['tram.bigcity'], bgesYear)
    } else if (this.modeDeplacement === Mission.MODE_RER) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['rer'], bgesYear)
    } else if (this.modeDeplacement === Mission.MODE_SUBWAY) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['railway']['subway'], bgesYear)
    } else if (this.modeDeplacement === Mission.MODE_FERRY) {
      ef = filterEFByYear(TRANSPORTS_FACTORS['boat']['ferry'], bgesYear)
    }
    return ef
  }

  getCarbonIntensity (bgesYear) {
    let cdistance = this.distance
    if (this.modeDeplacement === MODE_PLANE) {
      cdistance += 95
    } else if (this.modeDeplacement === Mission.MODE_CAB) {
      cdistance = cdistance * (1 + 1 / this.nbPersonneVoiture)
    } else if (this.modeDeplacement === Mission.MODE_CAR) {
      cdistance = cdistance / this.nbPersonneVoiture
    }
    // facteur multiplicateur en fonction du mode de deplacement
    cdistance = cdistance * MODES_DEPLACEMENTS[this.modeDeplacement]
    // choix du fe d'emission selon la distance corrigée
    let ci = this.getEmissionFactor(cdistance, bgesYear)
    let cinr = this.getEmissionFactor(cdistance, bgesYear, false)
    if (this.isAllerRetour) {
      cdistance = cdistance * 2
    }
    // prend en compte le nombre de fois que ce deplacement a ete fait
    let corrDistance = cdistance
    cdistance = cdistance * this.nbEffectue
    let total = cdistance * ci.total.total
    let corrtotal = corrDistance * ci.total.total
    let totali = cdistance * ci.total.total * ci.total.uncertainty
    let corrtotali = corrDistance * ci.total.total * ci.total.uncertainty
    let totalnr = total
    let totalnri = totali
    // pour l'aviation, calcul avec et sans trainée
    if (this.modeDeplacement === MODE_PLANE) {
      totalnr = cdistance * cinr.total.total
      totalnri = cdistance * cinr.total.total * cinr.total.uncertainty
    }
    return {
      'total': total,
      'totali': totali,
      'totalnr': totalnr,
      'totalnri': totalnri,
      'distance': cdistance,
      'corrDistance': corrDistance,
      'corrtotal': corrtotal,
      'corrtotali': corrtotali
    }
  }

  toDatabase () {
    return {
      'idMission': this.idMission,
      'localisation': this.localisation,
      'modeDeplacement': this.modeDeplacement,
      'missionType': this.missionType,
      'nbPersonneVoiture': this.nbPersonneVoiture,
      'motifDeplacement': this.motifDeplacement,
      'distance': this.distance,
      'statut': this.statut,
      'isAllerRetour': this.isAllerRetour,
      'nbEffectue': this.nbEffectue
    }
  }

  isFromDatabase () {
    let isFromDatabase = this.villeDepart === undefined && this.villeDestination === undefined &&
      this.paysDepart === null && this.paysDestination === null &&
      this.villeDepartLat === null && this.villeDepartLng === null &&
      this.villeDestinationLat == null && this.villeDestinationLng == null
    return isFromDatabase
  }

  static get MODE_PLANE () {
    return MODE_PLANE
  }

  static get MODE_TRAIN () {
    return MODE_TRAIN
  }

  static get MODE_CAR () {
    return MODE_CAR
  }

  static get MODE_CAB () {
    return MODE_CAB
  }

  static get MODE_BUS () {
    return MODE_BUS
  }

  static get MODE_TRAM () {
    return MODE_TRAM
  }

  static get MODE_RER () {
    return MODE_RER
  }

  static get MODE_SUBWAY () {
    return MODE_SUBWAY
  }

  static get MODE_FERRY () {
    return MODE_FERRY
  }

  static get modesDeplacements () {
    return Object.keys(MODES_DEPLACEMENTS)
  }

  static get PURPOSE_FIELD_STUDY () {
    return PURPOSE_FIELD_STUDY
  }

  static get PURPOSE_CONFERENCE () {
    return PURPOSE_CONFERENCE
  }

  static get PURPOSE_SEMINAR () {
    return PURPOSE_SEMINAR
  }

  static get PURPOSE_TEACHING () {
    return PURPOSE_TEACHING
  }

  static get PURPOSE_COLLABORATION () {
    return PURPOSE_COLLABORATION
  }

  static get PURPOSE_VISIT () {
    return PURPOSE_VISIT
  }

  static get PURPOSE_RESEARCH_MANAGEMENT () {
    return PURPOSE_RESEARCH_MANAGEMENT
  }

  static get PURPOSE_OTHER () {
    return PURPOSE_OTHER
  }

  static get motifsDeplacements () {
    return [
      PURPOSE_FIELD_STUDY,
      PURPOSE_CONFERENCE,
      PURPOSE_SEMINAR,
      PURPOSE_TEACHING,
      PURPOSE_COLLABORATION,
      PURPOSE_VISIT,
      PURPOSE_RESEARCH_MANAGEMENT,
      PURPOSE_OTHER
    ]
  }

  static get STATUS_GUEST () {
    return STATUS_GUEST
  }

  static get STATUS_RESEARCHER () {
    return STATUS_RESEARCHER
  }

  static get STATUS_ENGINEER () {
    return STATUS_ENGINEER
  }

  static get STATUS_STUDENT () {
    return STATUS_STUDENT
  }

  static get statuts () {
    return [
      STATUS_GUEST,
      STATUS_RESEARCHER,
      STATUS_ENGINEER,
      STATUS_STUDENT
    ]
  }

  static getIcon (mode) {
    return ICONS[mode]
  }

  static getIconPack (mode) {
    return ICONS_PACK[mode]
  }

  static getTravelsMode (travelsMode) {
    let modifiedTravelsMode = Mission.removeAccents(travelsMode, false).toLowerCase()
    if (modifiedTravelsMode in MODE_DICTIONARY) {
      return MODE_DICTIONARY[modifiedTravelsMode]
    } else {
      return null
    }
  }

  static getTravelPurpose (purpose) {
    let modifiedPurpose = Mission.removeAccents(purpose, false).toLowerCase()
    if (modifiedPurpose in PURPOSE_DICTIONARY) {
      return PURPOSE_DICTIONARY[modifiedPurpose]
    } else {
      return null
    }
  }

  static getIsRoundTrip (roundTrip) {
    let modifiedRoundTrip = roundTrip.toLowerCase()
    if (modifiedRoundTrip in ROUND_TRIP_DICTIONARY) {
      return ROUND_TRIP_DICTIONARY[modifiedRoundTrip]
    } else {
      return roundTrip
    }
  }

  static getStatus (status) {
    let modifiedStatus = Mission.removeAccents(status, false).toLowerCase()
    if (modifiedStatus in STATUS_DICTIONARY) {
      return STATUS_DICTIONARY[modifiedStatus]
    } else {
      return null
    }
  }

  static geodesicDist (lat1, lon1, lat2, lon2) {
    if ((lat1 === lat2) && (lon1 === lon2)) {
      return 0
    } else {
      var radlat1 = Math.PI * lat1 / 180
      var radlat2 = Math.PI * lat2 / 180
      var theta = lon1 - lon2
      var radtheta = Math.PI * theta / 180
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
      if (dist > 1) {
        dist = 1
      }
      dist = Math.acos(dist)
      dist = dist * 180 / Math.PI
      dist = dist * 60 * 1.1515
      dist = dist * 1.609344
      return dist
    }
  }

  static removeAccents (str, tiret = true) {
    let fstr = str
    try {
      let accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž'
      let accentsOut = 'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz'
      str = str.split('')
      let strLen = str.length
      for (let i = 0; i < strLen; i++) {
        let x = accents.indexOf(str[i])
        if (x !== -1) {
          str[i] = accentsOut[x]
        }
      }
      if (tiret) {
        fstr = str.join('').replace('-', ' ')
      } else {
        fstr = str.join('')
      }
    } catch (error) {
      fstr = ''
    }
    return fstr
  }

  static paysISO3166 (pays) {
    let paysFinal = null
    if (typeof pays === 'string') {
      if (pays.length !== 2) {
        try {
          paysFinal = ISO3166.filter(obj => Mission.removeAccents(obj.nom.toLowerCase()) === Mission.removeAccents(pays.toLowerCase()))[0].iso
        } catch (e) {
          paysFinal = null
        }
      } else {
        try {
          paysFinal = ISO3166.filter(obj => Mission.removeAccents(obj.iso.toLowerCase()) === Mission.removeAccents(pays.toLowerCase()))[0].iso
        } catch (e) {
          paysFinal = null
        }
      }
    }
    return (paysFinal)
  }

  static initiStatutsTable (length) {
    return {
      [Mission.STATUS_RESEARCHER]: new Array(length).fill(0),
      [Mission.STATUS_ENGINEER]: new Array(length).fill(0),
      [Mission.STATUS_STUDENT]: new Array(length).fill(0),
      [Mission.STATUS_GUEST]: new Array(length).fill(0)
    }
  }

  static createFromObj (mission) {
    return new Mission(
      mission.idMission,
      mission.dateDepart,
      mission.villeDepart,
      mission.paysDepart,
      mission.villeDestination,
      mission.paysDestination,
      mission.localisation,
      mission.modeDeplacement,
      mission.nbPersonneVoiture,
      mission.motifDeplacement,
      mission.statut,
      mission.isAllerRetour,
      mission.villeDepartLat,
      mission.villeDepartLng,
      mission.villeDestinationLat,
      mission.villeDestinationLng,
      mission.distance,
      mission.missionType,
      mission.nbEffectue,
      mission.forceInvalid,
      mission.source
    )
  }

  static compute (missions, bgesYear) {
    let emissionsTotalStatuts = Mission.initiStatutsTable(Mission.modesDeplacements.length)
    let iemissionsTotalStatuts = Mission.initiStatutsTable(Mission.modesDeplacements.length)
    let distancesTotalStatuts = Mission.initiStatutsTable(Mission.modesDeplacements.length)
    let emissionsTotalMotifs = Mission.initiStatutsTable(Mission.motifsDeplacements.length)
    let iemissionsTotalMotifs = Mission.initiStatutsTable(Mission.motifsDeplacements.length)
    let distancesTotalMotifs = Mission.initiStatutsTable(Mission.motifsDeplacements.length)

    let total = 0
    let totali = 0
    let totalnr = 0
    let totalnri = 0
    let totalMode = new Array(Mission.modesDeplacements.length).fill(0)
    let distanceMode = new Array(Mission.modesDeplacements.length).fill(0)
    let totalModei = new Array(Mission.modesDeplacements.length).fill(0)
    let totalMotif = new Array(Mission.motifsDeplacements.length).fill(0)
    let distanceMotif = new Array(Mission.motifsDeplacements.length).fill(0)
    let totalMotifi = new Array(Mission.motifsDeplacements.length).fill(0)
    let meta = []
    for (let mission of missions) {
      let emissions = mission.getCarbonIntensity(bgesYear)
      let index = Mission.modesDeplacements.indexOf(mission.modeDeplacement)
      let indexm = Mission.motifsDeplacements.indexOf(mission.motifDeplacement)
      total += emissions.total
      totali += emissions.totali
      totalnr += emissions.totalnr
      totalnri += emissions.totalnri
      totalMode[index] += emissions.total
      distanceMode[index] += emissions.distance
      totalModei[index] += emissions.totali
      totalMotif[indexm] += emissions.total
      distanceMotif[indexm] += emissions.distance
      totalMotifi[indexm] += emissions.totali
      meta.unshift({
        'idMission': mission.idMission,
        'modeDeplacement': mission.modeDeplacement,
        'missionType': mission.missionType,
        'nbPersonneVoiture': mission.nbPersonneVoiture,
        'motifDeplacement': mission.motifDeplacement,
        'distance': mission.distance,
        'distanceTotal': emissions.distance,
        'corrDistance': emissions.corrDistance,
        'statut': mission.statut,
        'isAllerRetour': mission.isAllerRetour,
        'nbEffectue': mission.nbEffectue,
        'total': emissions.total,
        'totali': emissions.totali,
        'corrtotal': emissions.corrtotal,
        'corrtotali': emissions.corrtotali
      })
      if (mission.statut === Mission.STATUS_RESEARCHER) {
        emissionsTotalStatuts[Mission.STATUS_RESEARCHER][index] += emissions.total
        iemissionsTotalStatuts[Mission.STATUS_RESEARCHER][index] += emissions.totali
        distancesTotalStatuts[Mission.STATUS_RESEARCHER][index] += emissions.distance
        if (indexm !== -1) {
          emissionsTotalMotifs[Mission.STATUS_RESEARCHER][indexm] += emissions.total
          iemissionsTotalMotifs[Mission.STATUS_RESEARCHER][indexm] += emissions.totali
          distancesTotalMotifs[Mission.STATUS_RESEARCHER][indexm] += emissions.distance
        }
      } else if (mission.statut === Mission.STATUS_ENGINEER) {
        emissionsTotalStatuts[Mission.STATUS_ENGINEER][index] += emissions.total
        iemissionsTotalStatuts[Mission.STATUS_ENGINEER][index] += emissions.totali
        distancesTotalStatuts[Mission.STATUS_ENGINEER][index] += emissions.distance
        if (indexm !== -1) {
          emissionsTotalMotifs[Mission.STATUS_ENGINEER][indexm] += emissions.total
          iemissionsTotalMotifs[Mission.STATUS_ENGINEER][indexm] += emissions.totali
          distancesTotalMotifs[Mission.STATUS_ENGINEER][indexm] += emissions.distance
        }
      } else if (mission.statut === Mission.STATUS_STUDENT) {
        emissionsTotalStatuts[Mission.STATUS_STUDENT][index] += emissions.total
        iemissionsTotalStatuts[Mission.STATUS_STUDENT][index] += emissions.totali
        distancesTotalStatuts[Mission.STATUS_STUDENT][index] += emissions.distance
        if (indexm !== -1) {
          emissionsTotalMotifs[Mission.STATUS_STUDENT][indexm] += emissions.total
          iemissionsTotalMotifs[Mission.STATUS_STUDENT][indexm] += emissions.totali
          distancesTotalMotifs[Mission.STATUS_STUDENT][indexm] += emissions.distance
        }
      } else if (mission.statut === Mission.STATUS_GUEST) {
        emissionsTotalStatuts[Mission.STATUS_GUEST][index] += emissions.total
        iemissionsTotalStatuts[Mission.STATUS_GUEST][index] += emissions.totali
        distancesTotalStatuts[Mission.STATUS_GUEST][index] += emissions.distance
        if (indexm !== -1) {
          emissionsTotalMotifs[Mission.STATUS_GUEST][indexm] += emissions.total
          iemissionsTotalMotifs[Mission.STATUS_GUEST][indexm] += emissions.totali
          distancesTotalMotifs[Mission.STATUS_GUEST][indexm] += emissions.distance
        }
      }
    }
    return {
      'total': total,
      'totali': totali,
      'totalnr': totalnr,
      'totalnri': totalnri,
      'meta': meta,
      'modes': {
        'total': totalMode,
        'totali': totalModei,
        'distances': distanceMode
      },
      'motifs': {
        'total': totalMotif,
        'totali': totalMotifi,
        'distances': distanceMotif
      },
      'statuts': {
        'total': emissionsTotalStatuts,
        'totali': iemissionsTotalStatuts,
        'distances': distancesTotalStatuts
      },
      'statutsmotifs': {
        'total': emissionsTotalMotifs,
        'totali': iemissionsTotalMotifs,
        'distances': distancesTotalMotifs
      }
    }
  }
}
