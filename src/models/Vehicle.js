/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { filterEFByYear } from '@/mixins/utils'
import VEHICLES_FACTORS from '@/../data/vehiclesFactors.json'

const ICONS = {
  'car': 'car',
  'motorbike': 'motorcycle',
  'bike': 'bicycle',
  'scooter': 'electric-scooter',
  'aircraft': 'plane',
  'boat': 'ship'
}

const ICONS_PACK = {
  'car': 'sui',
  'motorbike': 'sui',
  'bike': 'sui',
  'scooter': 'icomoon',
  'aircraft': 'sui',
  'boat': 'sui'
}

export default class Vehicle {
  constructor (pk, typeVehicle, name, engine, consumption, unit, power, nbEngines, shp, operationalControl) {
    this.pk = pk
    this.nom = name
    this.typeVehicule = typeVehicle
    this.motorisation = engine
    this.consommation = consumption
    this.unite = unit
    this.puissance = power
    this.nbMoteurs = nbEngines
    this.shp = shp
    this.controleOperationnel = operationalControl
  }

  _getHelicopterTConsumption (_shp) {
    let cCarburant = 0
    if (this.shp > 1000) {
      cCarburant = 4.0539 * Math.pow(10, -18) * Math.pow(_shp, 5) - 3.16298 * Math.pow(10, -14) * Math.pow(_shp, 4)
      cCarburant = cCarburant + 9.2087 * Math.pow(10, -11) * Math.pow(_shp, 3) - 1.2156 * Math.pow(10, -7) * Math.pow(_shp, 2)
      cCarburant = cCarburant + 1.1476 * Math.pow(10, -4) * _shp + 0.01256
    } else if (this.shp > 600) {
      cCarburant = 3.3158 * Math.pow(10, -16) * Math.pow(_shp, 5) - 1.0175 * Math.pow(10, -12) * Math.pow(_shp, 4)
      cCarburant = cCarburant + 1.1627 * Math.pow(10, -9) * Math.pow(_shp, 3) - 5.9528 * Math.pow(10, -7) * Math.pow(_shp, 2)
      cCarburant = cCarburant + 1.8168 * Math.pow(10, -4) * _shp + 0.0062945
    } else {
      cCarburant = 2.197 * Math.pow(10, -15) * Math.pow(_shp, 5) - 4.4441 * Math.pow(10, -12) * Math.pow(_shp, 4)
      cCarburant = cCarburant + 3.4208 * Math.pow(10, -9) * Math.pow(_shp, 3) - 1.2138 * Math.pow(10, -6) * Math.pow(_shp, 2)
      cCarburant = cCarburant + 2.414 * Math.pow(10, -4) * _shp + 0.004583
    }
    let cTotale = this.getConsumption() * this.nbMoteurs * 3600 * cCarburant * 1.25
    return cTotale
  }

  _getHelicopterPConsumption (_shp) {
    let cCarburant = 1.9 * Math.pow(10, -12) * Math.pow(_shp, 4) - Math.pow(10, -9) * Math.pow(_shp, 3)
    cCarburant = cCarburant + 2.6 * Math.pow(10, -7) * Math.pow(_shp, 2) + 4 * Math.pow(10, -5) * _shp + 0.0006
    let cTotale = this.getConsumption() * this.nbMoteurs * 3600 * cCarburant * 1.39
    return cTotale
  }

  _getPlaneConsumption () {
    return this.getConsumption() * (0.22 * this.puissance + 0.54)
  }

  getConsumption () {
    let cTotale = 0
    if (this.consommation.isMensuelle) {
      cTotale += parseInt(this.consommation.cJanvier) + parseInt(this.consommation.cFevrier)
      cTotale += parseInt(this.consommation.cMars) + parseInt(this.consommation.cAvril)
      cTotale += parseInt(this.consommation.cMai) + parseInt(this.consommation.cJuin)
      cTotale += parseInt(this.consommation.cJuillet) + parseInt(this.consommation.cAout)
      cTotale += parseInt(this.consommation.cSeptembre) + parseInt(this.consommation.cOctobre)
      cTotale += parseInt(this.consommation.cNovembre) + parseInt(this.consommation.cDecembre)
    } else {
      cTotale = this.consommation.cTotale
    }
    return cTotale
  }

  getEmissionFactor (type, engine, year) {
    return filterEFByYear(VEHICLES_FACTORS[type][engine], year)
  }

  getCarbonIntensity (bgesYear) {
    let cTotale = this.getConsumption()
    let _shp = null
    if (this.motorisation === 'helicopter.pistons') {
      _shp = this.shp * 0.9
    } else if (this.motorisation === 'helicopter.turbines') {
      if (this.nbMoteurs === 1) {
        _shp = this.shp * 0.8
      } else {
        if (this.shp > 1000) {
          _shp = this.shp * 0.62
        } else {
          _shp = this.shp * 0.65
        }
      }
    } else {
      _shp = this.shp
    }
    if (this.typeVehicule === 'aircraft') {
      if (this.motorisation === 'helicopter.pistons') {
        cTotale = this._getHelicopterPConsumption(_shp)
      } else if (this.motorisation === 'helicopter.turbines') {
        cTotale = this._getHelicopterTConsumption(_shp)
      } else {
        cTotale = this._getPlaneConsumption()
      }
    }
    let ef = this.getEmissionFactor(
      this.typeVehicule,
      this.motorisation,
      bgesYear
    )
    return {
      'co2c': cTotale * ef.combustion.co2,
      'ch4c': cTotale * ef.combustion.ch4,
      'n2oc': cTotale * ef.combustion.n2o,
      'autrec': cTotale * ef.combustion.others,
      'totalc': cTotale * ef.combustion.total,
      'incertitudec': ef.combustion.uncertainty,
      'totala': cTotale * ef.upstream.total,
      'incertitudea': ef.upstream.uncertainty,
      'totalf': cTotale * ef.manufacturing.total,
      'incertitudef': ef.manufacturing.uncertainty
    }
  }

  static getIcon (type) {
    return ICONS[type]
  }

  static getIconPack (type) {
    return ICONS_PACK[type]
  }

  static getTypes () {
    let types = Object.keys(VEHICLES_FACTORS)
    return Array.from(new Set(types))
  }

  static getEngines (type) {
    let engines = Object.keys(VEHICLES_FACTORS[type])
    return Array.from(new Set(engines))
  }

  static getUnits (type, engine) {
    return VEHICLES_FACTORS[type][engine].unit
  }

  static createFromObj (vehicle) {
    return new Vehicle(
      vehicle.pk,
      vehicle.typeVehicule,
      vehicle.nom,
      vehicle.motorisation,
      vehicle.consommation,
      vehicle.unite,
      vehicle.puissance,
      vehicle.nbMoteurs,
      vehicle.shp,
      vehicle.controleOperationnel
    )
  }

  static compute (vehicles, bgesYear) {
    let controled = {
      'co2c': 0,
      'ch4c': 0,
      'n2oc': 0,
      'autrec': 0,
      'totalc': 0,
      'totalci': 0,
      'totala': 0,
      'totalai': 0,
      'totalf': 0,
      'totalfi': 0
    }
    let uncontroled = {
      'co2c': 0,
      'ch4c': 0,
      'n2oc': 0,
      'autrec': 0,
      'totalc': 0,
      'totalci': 0,
      'totala': 0,
      'totalai': 0,
      'totalf': 0,
      'totalfi': 0
    }
    let total = 0
    let totali = 0
    let meta = []
    for (let vehicle of vehicles) {
      let emissions = vehicle.getCarbonIntensity(bgesYear)
      total += emissions.totalc + emissions.totala + emissions.totalf
      totali += emissions.totalc * emissions.incertitudec + emissions.totala * emissions.incertitudea + emissions.totalf * emissions.incertitudef
      if (vehicle.controleOperationnel) {
        controled.co2c += emissions.co2c
        controled.ch4c += emissions.ch4c
        controled.n2oc += emissions.n2oc
        controled.autrec += emissions.autrec
        controled.totalc += emissions.totalc
        controled.totalci += emissions.totalc * emissions.incertitudec
        controled.totala += emissions.totala
        controled.totalai += emissions.totala * emissions.incertitudea
        controled.totalf += emissions.totalf
        controled.totalfi += emissions.totalf * emissions.incertitudef
      } else {
        uncontroled.co2c += emissions.co2c
        uncontroled.ch4c += emissions.ch4c
        uncontroled.n2oc += emissions.n2oc
        uncontroled.autrec += emissions.autrec
        uncontroled.totalc += emissions.totalc
        uncontroled.totalci += emissions.totalc * emissions.incertitudec
        uncontroled.totala += emissions.totala
        uncontroled.totalai += emissions.totala * emissions.incertitudea
        uncontroled.totalf += emissions.totalf
        uncontroled.totalfi += emissions.totalf * emissions.incertitudef
      }
      meta.unshift({
        'nom': vehicle.nom,
        'typeVehicule': vehicle.typeVehicule,
        'motorisation': vehicle.motorisation,
        'consommation': vehicle.getConsumption(),
        'unite': vehicle.unite,
        'totalc': emissions.totalc,
        'totala': emissions.totala,
        'totalf': emissions.totalf,
        'total': emissions.totalc + emissions.totala + emissions.totalf,
        'totalci': emissions.totalc * emissions.incertitudec,
        'totalai': emissions.totala * emissions.incertitudea,
        'totalfi': emissions.totalf * emissions.incertitudef,
        'totali': emissions.totalc * emissions.incertitudec + emissions.totala * emissions.incertitudea + emissions.totalf * emissions.incertitudef
      })
    }
    return {
      'controled': controled,
      'uncontroled': uncontroled,
      'total': total,
      'totali': totali,
      'meta': meta
    }
  }
}
