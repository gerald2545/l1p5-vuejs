import api from '@/services/api'

export default {
  getMembresLocations () {
    return api.get(`get_membres_locations/`)
      .then(response => response.data)
  },
  getMembresEquipes (payload) {
    return api.post(`get_membres_equipes/`, payload)
      .then(response => response.data)
  },
  getMembresDisciplines () {
    return api.get(`get_membres_disciplines/`)
      .then(response => response.data)
  },
  getMembre () {
    return api.get(`get_membre/`)
      .then(response => response.data)
  },
  addMembreNewsletter (payload) {
    return api.post(`add_membre_newsletter/`, payload)
      .then(response => response.data)
  },
  addMembreGDR (payload) {
    return api.post(`add_membre_gdr/`, payload)
      .then(response => response.data)
  },
  getAllGDRMembres () {
    return api.get(`get_all_gdr_membres/`)
      .then(response => response.data)
  },
  validateMembreGDR (payload) {
    return api.post(`validate_membre_gdr/`, payload)
      .then(response => response.data)
  },
  unacceptMembreGDR (payload) {
    return api.post(`unaccept_membre_gdr/`, payload)
      .then(response => response.data)
  },
  addMembreReflexion (payload) {
    return api.post(`add_membre_reflexion/`, payload)
      .then(response => response.data)
  },
  saveMembre (payload) {
    return api.post(`save_membre/`, payload)
      .then(response => response.data)
  },
  resetPassword (payload) {
    return api.post(`reset_password/`, payload)
      .then(response => response.data)
  },
  userExists (payload) {
    return api.post(`user_exists/`, payload)
      .then(response => response.data)
  },
  memberExists (payload) {
    return api.post(`member_exists/`, payload)
      .then(response => response.data)
  },
  isSuperUser () {
    return api.get(`is_super_user/`)
      .then(response => response.data)
  },
  desabonnementListe (payload) {
    return api.post(`desabonnement_liste/`, payload)
      .then(response => response.data)
  },
  askDataAccess (payload) {
    return api.post(`ask_data_access/`, payload)
      .then(response => response.data)
  },
  getDataAccess () {
    return api.get(`get_data_access/`)
      .then(response => response.data)
  },
  validateAccess (payload) {
    return api.post(`validate_access/`, payload)
      .then(response => response.data)
  }
}
