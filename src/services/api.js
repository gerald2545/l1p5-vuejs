import axios from 'axios'
import Cookies from 'js-cookie'
import store from '../store'

const http = axios.create({
  baseURL: '/api',
  timeout: 60 * 4 * 1000,
  headers: {
    'Content-Type': 'application/json',
    'X-CSRFToken': Cookies.get('csrftoken')
  }
})

http.interceptors.request.use(
  function (config) {
    const token = store.getters['authentication/token']
    if (token) config.headers.Authorization = `Bearer ${token}`
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

http.interceptors.response.use(undefined, function (error) {
  return new Promise(function (resolve, reject) {
    if (error.response.data.code === 'token_not_valid') {
      store.dispatch('authentication/authLogout')
    }
    throw error
  })
})

export default http
