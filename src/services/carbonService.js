import api from '@/services/api'

export default {
  saveBGES (payload) {
    return api.post(`save_bges/`, payload)
      .then(response => response.data)
  },
  deleteBGES (payload) {
    return api.post(`delete_bges/`, payload)
      .then(response => response.data)
  },
  getAllBGES () {
    return api.post(`get_all_bges/`)
      .then(response => response.data)
  },
  getAllBGESFull () {
    return api.get(`get_all_bges_full/`)
      .then(response => response.data)
  },
  saveVehicles (payload) {
    return api.post(`save_vehicules/`, payload)
      .then(response => response.data)
  },
  saveComputerDevices (payload) {
    return api.post(`save_computer_devices/`, payload)
      .then(response => response.data)
  },
  savePurchases (payload) {
    return api.post(`save_purchases/`, payload)
      .then(response => response.data)
  },
  saveBuildings (payload) {
    return api.post(`save_batiments/`, payload)
      .then(response => response.data)
  },
  saveCommutes (payload) {
    return api.post(`save_deplacements_dt/`, payload)
      .then(response => response.data)
  },
  saveCommute (payload) {
    return api.post(`save_deplacement_dt/`, payload)
      .then(response => response.data)
  },
  saveSurveyMessage (payload) {
    return api.post(`save_survey_message/`, payload)
      .then(response => response.data)
  },
  getBGESSurveyInfo (payload) {
    return api.post(`get_bges_survey_info/`, payload)
      .then(response => response.data)
  },
  saveMissions (payload) {
    return api.post(`save_missions/`, payload)
      .then(response => response.data)
  },
  getBGESConsommations (payload) {
    return api.post(`get_bges_consommations/`, payload)
      .then(response => response.data)
  },
  getAllVehicles () {
    return api.post(`get_all_vehicules/`)
      .then(response => response.data)
  },
  getAllBuildings () {
    return api.post(`get_all_batiments/`)
      .then(response => response.data)
  },
  getBGESInfos () {
    return api.get(`get_BGES_infos/`)
      .then(response => response.data)
  },
  getBGESAdmin (payload) {
    return api.post(`get_bges_admin/`, payload)
      .then(response => response.data)
  },
  submitData (payload) {
    return api.post(`submit_data/`, payload)
      .then(response => response.data)
  },
  activateBGESSurvey (payload) {
    return api.post(`activate_bges_survey/`, payload)
      .then(response => response.data)
  }
}
