import api from '@/services/api'

export default {
  saveLaboratoire (payload) {
    return api.post(`save_laboratoire/`, payload)
      .then(response => response.data)
  },
  getLaboratoire (payload) {
    return api.get(`get_laboratoire/`)
      .then(response => response.data)
  },
  getDisciplines () {
    return api.get(`get_disciplines/`)
      .then(response => response.data)
  },
  getStatuts () {
    return api.get(`get_statuts/`)
      .then(response => response.data)
  },
  getGenres () {
    return api.get(`get_genres/`)
      .then(response => response.data)
  },
  getUserDisciplines () {
    return api.get(`get_user_disciplines/`)
      .then(response => response.data)
  },
  addMessage (payload) {
    return api.post(`add_message/`, payload)
      .then(response => response.data)
  },
  addMessageDPD (payload) {
    return api.post(`add_message_dpd/`, payload)
      .then(response => response.data)
  },
  getTutelles () {
    return api.get(`get_tutelles/`)
      .then(response => response.data)
  },
  addTutelle (payload) {
    return api.post(`tutelles/`, payload)
      .then(response => response.data)
  },
  getInitiatives () {
    return api.get(`get_initiatives/`)
      .then(response => response.data)
  },
  getInitiativesAdmin () {
    return api.get(`get_initiatives_admin/`)
      .then(response => response.data)
  },
  validateInitiative (payload) {
    return api.post(`validate_initiative/`, payload)
      .then(response => response.data)
  },
  getInitiative () {
    return api.get(`get_initiative/`)
      .then(response => response.data)
  },
  saveInitiative (payload) {
    return api.post(`save_initiative/`, payload)
      .then(response => response.data)
  }
}
