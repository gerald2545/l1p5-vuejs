import Vue from 'vue'
import App from '@/App.vue'

import store from '@/store'
import router from '@/router'

import '@/utils/filters.js'
import '@/plugins/veevalidate.js'
import '@/plugins/buefy.js'
import { i18n } from '@/plugins/i18n.js'
import FlagIcon from 'vue-flag-icon'
import '@/assets/js/fresh.js'
import NProgress from 'vue-nprogress'

Vue.use(FlagIcon)

Vue.config.productionTip = false
Vue.use(NProgress)

const nprogress = new NProgress()

new Vue({
  nprogress,
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
