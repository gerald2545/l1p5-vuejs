# Informations générales

GES 1point5, développé par Labos 1point5, est un outil permettant de calculer l’empreinte carbone et de construire le bilan gaz à effet de serre (BGES) réglementaire de votre laboratoire.

A travers cet outil l'objectif est double : 

* Mener des études scientifiques relatives à l’empreinte carbone de la recherche publique française (notre champ d’investigation actuel est limité à la France, y compris les DOM-TOM). 

* Nourrir la réflexion sur les leviers d'actions permettant de réduire l’impact des activités de recherche sur les émissions de gaz à effet de serre, tant à l'échelle nationale que locale au laboratoire. 

# Technologies utilisées

* Django 3.0.8
* MariaDB
* VueJS

# Installation

## Information importante
Sur debian les paquets ```python3-dev default-libmysqlclient-dev build-essential``` sont nécessaires, avant l'installation de pip.

## pré-requis

Les paquets suivants sont nécessaires : 

* Python >= 3.6 & < 3.9
* pip 
* virutalenv 
* npm
* MariaDB
* Créer à la racine du repo un fichier ```log/labos1.5.log```

## Création de l'environnement virutel python
```
virtualenv -p python3 [nom de du virtualenv]
source [nom du virtualenv]/bin/activate
```

## Installation des dépendances

* PIP
```
pip install -r requirements.txt
```

Si vous rencontrez l'erreur : 
```
ERROR: Could not find a version that satisfies the requirement djangorestframework-simplejwt==4.4.0 
ERROR: No matching distribution found for djangorestframework-simplejwt==4.4.0
```
Il faut changer la version de ```djangorestframework-simplejwt``` dans le fichier ```requirements.txt``` comme suit :
```
djangorestframework-simplejwt==4.5.0
```
*  NPM
```
npm install
```

# Initialisation de la Base De Données

## Mise à jour des informations de connexion
Dans le fichier ```backend/settings/dev.py```, il faut modifier la section DATABASES Avec vos informations : 
```
DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql',
		'NAME': 'labo15',
		'USER': 'root',
		'PASSWORD': 'mypassword',
		'HOST': '/var/run/mysqld/mysqld.sock',
		'PORT': '3306',
	}
}
```
Ensuite il faut créer la base de données sur MariaDB avec ces mêmes informations.

## Migration des données vers la BDD avec Django
On  peut maintenant migrer les les données vers la BDD, via l'ORM de Django avec la commande :
```
python manage.py migrate
```

# Compilation

* Compilation et changements à chaud (hot-reloads) :
    ```
    npm run serve
    ```
* Compilation et minifications des ressources (prod) :

    ```
        npm run build
    ```
    
    ```
       python manage.py runserver
    ```
* Mettre en évidence les erreurs :
    ```
    npm run lint
    ```

# Documentations et ressources
* Documentation Django : [https://www.djangoproject.com](https://www.djangoproject.com/)
* Documentation MariaDB : [https://devdocs.io/mariadb/](https://devdocs.io/mariadb/)
* Documentation VueJS : [https://vuejs.org/guide/introduction.html](https://vuejs.org/guide/introduction.html)

