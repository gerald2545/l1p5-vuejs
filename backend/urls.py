"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.contrib import admin
from django.urls import path, include, re_path
import django.contrib.auth.views as auth_views
from rest_framework import routers

from .api import views as apiviews
from .carbon import views as carbonviews
from .users import views as usersviews

router = routers.DefaultRouter()

urlpatterns = [

    # http://localhost:8000/
    path('', apiviews.index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    path('api/', include(router.urls)),
    
    # user
    path('api/add_membre_newsletter/', usersviews.add_membre_newsletter, name='add_membre_newsletter'),
    path('api/add_membre_gdr/', usersviews.add_membre_gdr, name='add_membre_gdr'),
    path('api/add_membre_reflexion/', usersviews.add_membre_reflexion, name='add_membre_reflexion'),
    path('api/is_super_user/', usersviews.is_super_user, name='is_super_user'),
    path('api/save_membre/', usersviews.save_membre, name='save_membre'),
    path('api/get_membre/', usersviews.get_membre, name='get_membre'),
    path('api/get_membres_locations/', usersviews.get_membres_locations, name='get_membres_locations'),
    path('api/get_membres_equipes/', usersviews.get_membres_equipes, name='get_membres_equipes'),
    path('api/get_membres_disciplines/', usersviews.get_membres_disciplines, name='get_membres_disciplines'),
    path('api/auth/registration/', usersviews.RegisterView.as_view()),
    path('api/desabonnement_liste/', usersviews.desabonnement_liste, name='desabonnement_liste'),
    path('api/save_laboratoire/', apiviews.save_laboratoire, name='save_laboratoire'),
    path('api/get_laboratoire/', apiviews.get_laboratoire, name='get_laboratoire'),
    path('api/get_all_gdr_membres/', usersviews.get_all_gdr_membres, name='get_all_gdr_membres'),
    path('api/validate_membre_gdr/', usersviews.validate_membre_gdr, name='validate_membre_gdr'),
    path('api/unaccept_membre_gdr/', usersviews.unaccept_membre_gdr, name='unaccept_membre_gdr'),
    path('api/ask_data_access/', usersviews.ask_data_access, name='ask_data_access'),
    path('api/get_data_access/', usersviews.get_data_access, name='get_data_access'),
    path('api/validate_access/', usersviews.validate_access, name='validate_access'),

    # password reset
    path('api/user_exists/', usersviews.user_exists, name='user_exists'),
    path('api/member_exists/', usersviews.member_exists, name='member_exists'),
    path('api/reset_password/', usersviews.PasswordResetView.as_view(), name='rest_password_reset'),
    path('accounts/reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    # account activation
    path('accounts/activate_account/<uidb64>/<token>/', usersviews.activate_account, name='activate_account'),

    # appli carbone
    path('api/get_BGES_infos/', carbonviews.get_BGES_infos, name='get_BGES_infos'),
    path('api/save_bges/', carbonviews.save_bges, name='save_bges'),
    path('api/delete_bges/', carbonviews.delete_bges, name='delete_bges'),
    path('api/get_all_bges/', carbonviews.get_all_bges, name='get_all_bges'),
    path('api/get_all_bges_full/', carbonviews.get_all_bges_full, name='get_all_bges_full'),
    path('api/get_bges_admin/', carbonviews.get_bges_admin, name='get_bges_admin'),
    path('api/get_bges_consommations/', carbonviews.get_bges_consommations, name='get_bges_consommations'),
    path('api/save_vehicules/', carbonviews.save_vehicules, name='save_vehicules'),
    path('api/get_all_vehicules/', carbonviews.get_all_vehicules, name='get_all_vehicules'),
    path('api/get_all_batiments/', carbonviews.get_all_batiments, name='get_all_batiments'),
    path('api/save_batiments/', carbonviews.save_batiments, name='save_batiments'),
    path('api/save_deplacements_dt/', carbonviews.save_deplacements_dt, name='save_deplacements_dt'),
    path('api/save_deplacement_dt/', carbonviews.save_deplacement_dt, name='save_deplacement_dt'),
    path('api/save_missions/', carbonviews.save_missions, name='save_missions'),
    path('api/save_purchases/', carbonviews.save_purchases, name='save_purchases'),
    path('api/submit_data/', carbonviews.submit_data, name='submit_data'),
    path('api/save_computer_devices/', carbonviews.save_computer_devices, name='save_computer_devices'),

    path('api/get_bges_survey_info/', carbonviews.get_bges_survey_info, name='get_bges_survey_info'),
    path('api/activate_bges_survey/', carbonviews.activate_bges_survey, name='activate_bges_survey'),
    path('api/save_survey_message/', carbonviews.save_survey_message, name='save_survey_message'),
    
    # initiative
    path('api/get_initiatives/', apiviews.get_initiatives, name='get_initiatives'),
    path('api/get_initiatives_admin/', apiviews.get_initiatives_admin, name='get_initiatives_admin'),
    path('api/validate_initiative/', apiviews.validate_initiative, name='validate_initiative'),
    path('api/get_initiative/', apiviews.get_initiative, name='get_initiative'),
    path('api/save_initiative/', apiviews.save_initiative, name='save_initiative'),
    
    # autres
    path('api/add_message/', apiviews.add_message, name='add_message'),
    path('api/add_message_dpd/', apiviews.add_message_dpd, name='add_message_dpd'),
    path('api/get_tutelles/', apiviews.get_tutelles, name='get_tutelles'),
    path('api/get_disciplines/', apiviews.get_disciplines, name='get_disciplines'),
    path('api/get_user_disciplines/', usersviews.get_user_disciplines, name='get_user_disciplines'),
    path('api/get_genres/', usersviews.get_genres, name='get_genres'),
    path('api/get_statuts/', usersviews.get_statuts, name='get_statuts'),
    # admin
    path('api/admin/', admin.site.urls),

    # authentication and JWT Token
    path('api/token/', usersviews.L1P5TokenObtainPairView.as_view(), name='token_obtain_pair'),
    #path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # Entry point of the VueJS web application
    re_path(r'^.*$', apiviews.index_view, name='entry_point')
]
