import uuid
from django.db import models
from django.conf import settings


class BGES(models.Model):
    uuid = models.UUIDField(
        null=True,
        unique = True,
        editable = False
    )
    annee = models.IntegerField()
    created = models.DateField(auto_now_add=True)
    nbChercheurs = models.IntegerField(null=True)
    nbEnseignants = models.IntegerField(null=True)
    nbITA = models.IntegerField(null=True)
    nbPostDoc = models.IntegerField(null=True)
    budget = models.IntegerField(null=True)
    surveyMessage = models.CharField(max_length=500, null=True)
    surveyActive = models.BooleanField(default=False)
    laboratoire = models.ForeignKey('api.Laboratoire', related_name='bges', on_delete=models.CASCADE)
    buildingsSubmitted = models.BooleanField(default=False)
    commutesSubmitted = models.BooleanField(default=False)
    travelsSubmitted = models.BooleanField(default=False)
    vehiclesSubmitted = models.BooleanField(default=False)
    devicesSubmitted = models.BooleanField(default=False)
    purchasesSubmitted = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        ''' On save, update uuid and ovoid clash with existing code '''
        if not self.uuid:
            self.uuid = uuid.uuid4()
        return super(BGES, self).save(*args, **kwargs)

class DeplacementDT(models.Model):
    bges = models.ForeignKey(BGES, related_name='deplacementDT', on_delete=models.CASCADE)
    seqID = models.CharField(max_length=10, null=True)
    deleted = models.BooleanField(default=False)
    statut = models.CharField(max_length=250)
    nbJourTravail = models.IntegerField()
    message = models.CharField(max_length=500, null=True)

    marche = models.IntegerField(default=0)
    velo = models.IntegerField(default=0)
    veloElectrique = models.IntegerField(default=0)
    trotinetteElectrique = models.IntegerField(default=0)
    deRouesMotorise = models.IntegerField(default=0)
    voiture = models.IntegerField(default=0)
    bus = models.IntegerField(default=0)
    tramway = models.IntegerField(default=0)
    train = models.IntegerField(default=0)
    rer = models.IntegerField(default=0)
    metro = models.IntegerField(default=0)
    nbPersonne2roues = models.IntegerField(default=1)
    nbPersonneVoiture = models.IntegerField(default=1)
    motorisation = models.CharField(max_length=25, null=True)

    has2TypeDay = models.BooleanField(default=False)
    nbDayType2 = models.IntegerField(default=0)
    marche2 = models.IntegerField(default=0)
    velo2 = models.IntegerField(default=0)
    veloElectrique2 = models.IntegerField(default=0)
    trotinetteElectrique2 = models.IntegerField(default=0)
    deRouesMotorise2 = models.IntegerField(default=0)
    voiture2 = models.IntegerField(default=0)
    bus2 = models.IntegerField(default=0)
    tramway2 = models.IntegerField(default=0)
    train2 = models.IntegerField(default=0)
    rer2 = models.IntegerField(default=0)
    metro2 = models.IntegerField(default=0)
    nbPersonne2roues2 = models.IntegerField(default=0)
    nbPersonneVoiture2 = models.IntegerField(default=0)
    motorisation2 = models.CharField(max_length=25, null=True)

class IDMisson(models.Model):
    nom = models.CharField(max_length=20)
    mission = models.ForeignKey('carbon.Mission', related_name='idMission', on_delete=models.CASCADE)

class Mission(models.Model):
    bges = models.ForeignKey(BGES, related_name='mission', on_delete=models.CASCADE)
    distance = models.FloatField(null=True)
    missionType = models.CharField(max_length=2,choices=[('NA', 'Nationale'), ('IN', 'Internationale'), ('MX', 'Mixte')], default='NA')
    localisation = models.CharField(max_length=250, null=True)
    modeDeplacement = models.CharField(max_length=250)
    nbPersonneVoiture = models.IntegerField(null=True)
    motifDeplacement = models.CharField(max_length=250, null=True)
    statut = models.CharField(max_length=250, null=True)
    isAllerRetour = models.BooleanField(null=True)
    nbEffectue = models.IntegerField()

class Vehicule(models.Model):
    nom = models.CharField(max_length=250)
    typeVehicule = models.CharField(max_length=250)
    motorisation = models.CharField(max_length=250)
    unite = models.CharField(max_length=250)
    puissance = models.IntegerField(null=True)
    nbMoteurs = models.IntegerField(null=True)
    shp = models.IntegerField(null=True)
    controleOperationnel = models.BooleanField()
    laboratoire = models.ForeignKey('api.Laboratoire', related_name='vehicule', on_delete=models.CASCADE)

class Batiment(models.Model):
    nom = models.CharField(max_length=250)
    superficie = models.FloatField()
    occupation = models.FloatField()
    isOwnedByLab = models.BooleanField()
    chauffageElectrique = models.BooleanField()
    typeChauffage = models.CharField(max_length=250, null=True)
    reseauChauffageUrbain = models.CharField(max_length=250, null=True)
    autoProduction = models.BooleanField()
    laboratoire = models.ForeignKey('api.Laboratoire', related_name='batiment', on_delete=models.CASCADE)
    site = models.CharField(max_length=250, null=True)

class GazRefrigerant(models.Model):
    bges = models.ForeignKey(BGES, related_name='consommationGaz', on_delete=models.CASCADE)
    batimentGazRefrigerant = models.ForeignKey(Batiment, related_name='consommationGazRefrigerant', on_delete=models.CASCADE, null=True)
    nom = models.CharField(max_length=50)
    cTotale = models.FloatField(null=True)

class SelfConsumption(models.Model):
    ghgi = models.ForeignKey(BGES, related_name='selfproduction', on_delete=models.CASCADE)
    building = models.ForeignKey(Batiment, related_name='selfproduction', on_delete=models.CASCADE)
    total = models.IntegerField(null=True)

class Consommation(models.Model):
    bges = models.ForeignKey(BGES, related_name='consommation', on_delete=models.CASCADE)
    vehicule = models.ForeignKey(Vehicule, related_name='consommation', on_delete=models.CASCADE, null=True)
    batimentElectricite = models.ForeignKey(Batiment, related_name='consommationElectricite', on_delete=models.CASCADE, null=True)
    batimentChauffage = models.ForeignKey(Batiment, related_name='consommationChauffage', on_delete=models.CASCADE, null=True)
    cJanvier = models.IntegerField(null=True)
    cFevrier = models.IntegerField(null=True)
    cMars = models.IntegerField(null=True)
    cAvril = models.IntegerField(null=True)
    cMai = models.IntegerField(null=True)
    cJuin = models.IntegerField(null=True)
    cJuillet = models.IntegerField(null=True)
    cAout = models.IntegerField(null=True)
    cSeptembre = models.IntegerField(null=True)
    cOctobre = models.IntegerField(null=True)
    cNovembre = models.IntegerField(null=True)
    cDecembre = models.IntegerField(null=True)
    cTotale = models.IntegerField(null=True)
    isMensuelle = models.BooleanField()
 
class ComputerDevice(models.Model):
    bges = models.ForeignKey(BGES, related_name='device', on_delete=models.CASCADE)
    type = models.CharField(max_length=250)
    model = models.CharField(max_length=250, null=True)
    amount = models.IntegerField(null=True)
    acquisitionYear = models.IntegerField(null=True)

class Purchase(models.Model):
    bges = models.ForeignKey(BGES, related_name='purchase', on_delete=models.CASCADE)
    code = models.CharField(max_length=250)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
