import copy

from django.core.exceptions import ValidationError
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status

from .models import BGES, Vehicule, Batiment, Consommation, SelfConsumption, GazRefrigerant, DeplacementDT, Mission, IDMisson, ComputerDevice, Purchase
from .serializers import BGESSerializer, VehiculeSerializer, VehiculeSCSerializer, BatimentSerializer, BatimentSCSerializer, ConsommationSerializer, SelfConsumptionSerializer, GazRefrigerantSerializer, DeplacementDTSerializer, MissionSerializer, ComputerDeviceSerializer, PurchaseSerializer
from ..api.models import Laboratoire, Initiative, LaboratoireSousDomaine, SousDomaine
from ..api.serializers import LaboratoiresSerializer
from ..users.models import Membre, L1P5User

@api_view(['GET'])
@permission_classes([])
def get_BGES_infos(request):
    labs_pk = []
    nb_membres_reflexion = len(Membre.objects.filter(equipes__nom="Réflexion").distinct())
    nb_membres_gdr = len(Membre.objects.filter(equipes__nom__in=["Empreinte", "Enquête", "Expérimentation", "Communication", "Technique", "Enseignement"]).filter(valid=1).distinct())
    all_labs_with_bges = BGES.objects.only('laboratoire')
    for bges in all_labs_with_bges:
        labs_pk.append(bges.laboratoire.pk)
    return Response({'nbMembre': Membre.objects.count(), 'nbMembreReflexion': nb_membres_reflexion, 'nbMembreGDR': nb_membres_gdr, 'nbInitiative': Initiative.objects.filter(valid=1).count(), 'nbBGES': BGES.objects.count(), 'nbLaboratoire': len(set(labs_pk))}, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def save_bges(request):
    bges_id = request.data.pop('pk')
    try:
        laboratoire = Laboratoire.objects.get(referant_id=request.user.pk)
        request.data["laboratoire"] = laboratoire
        bges, created = BGES.objects.update_or_create(pk=bges_id, defaults=request.data)
        serializer = BGESSerializer(bges)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except Laboratoire.DoesNotExist as err:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def delete_bges(request):
    bges_id = request.data.pop('bges_id')
    try:
        bges = BGES.objects.get(pk=bges_id)
        bges.delete()
        laboratoire = Laboratoire.objects.get(referant_id=request.user.pk)
        allbges = BGES.objects.filter(laboratoire_id=laboratoire.pk).order_by('-annee')
        serializer = BGESSerializer(allbges, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except BGES.DoesNotExist as err:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def get_all_bges(request):
    try:
        laboratoire = Laboratoire.objects.get(referant_id=request.user.pk)
        bges = BGES.objects.filter(laboratoire_id=laboratoire.pk).order_by('-annee')
        serializer = BGESSerializer(bges, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def get_all_bges_full(request):
    if request.user.is_authenticated:
        bges_to_return = []
        try:
            laboratoire = Laboratoire.objects.get(referant_id=request.user.pk)
            lserializer = LaboratoiresSerializer(laboratoire)
            all_bges = BGES.objects.filter(laboratoire_id=laboratoire.pk).order_by('-annee')
            for bges in all_bges:
                all_data = get_bges_consommations_data(bges.pk)
                serializer = BGESSerializer(bges)
                final_bges = copy.deepcopy(serializer.data)
                final_bges["laboratoire"] = copy.deepcopy(lserializer.data)
                final_bges["vehicules"] = copy.deepcopy(all_data["vehicules"])
                final_bges["batiments"] = copy.deepcopy(all_data["batiments"])
                final_bges["deplacements_dt"] = copy.deepcopy(all_data["deplacements_dt"])
                final_bges["missions"] = copy.deepcopy(all_data["missions"])
                final_bges["devices"] = copy.deepcopy(all_data["devices"])
                final_bges["purchases"] = copy.deepcopy(all_data["purchases"])
                bges_to_return.append(final_bges)
            return Response(bges_to_return, status=status.HTTP_201_CREATED)
        except:
            return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def get_bges_admin(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            bges_to_return = []
            filters = {}
            if request.data['buildingsSubmitted']:
                filters['buildingsSubmitted'] = True
            if request.data['vehiclesSubmitted']:
                filters['vehiclesSubmitted'] = True
            if request.data['purchasesSubmitted']:
                filters['purchasesSubmitted'] = True
            if request.data['devicesSubmitted']:
                filters['devicesSubmitted'] = True
            if request.data['travelsSubmitted']:
                filters['travelsSubmitted'] = True
            if request.data['commutesSubmitted']:
                filters['commutesSubmitted'] = True
            all_bges = BGES.objects.filter(**filters)
            for bges in all_bges:
                laboratoire = Laboratoire.objects.get(pk=bges.laboratoire_id)
                lserializer = LaboratoiresSerializer(laboratoire)
                all_data = get_bges_consommations_data(bges.pk)
                serializer = BGESSerializer(bges)
                final_bges = copy.deepcopy(serializer.data)
                final_bges["laboratoire"] = copy.deepcopy(lserializer.data)
                disciplines = LaboratoireSousDomaine.objects.filter(laboratoire_id=laboratoire.pk)
                final_bges["laboratoire"]['disciplines'] = []
                for discipline in disciplines:
                    disc_nom = SousDomaine.objects.filter(pk=discipline.discipline_id)[0].nom
                    final_bges["laboratoire"]["disciplines"].append({
                        'nom': disc_nom,
                        'pourcentage': discipline.pourcentage
                    })
                final_bges["referant"] = L1P5User.objects.get(pk=laboratoire.referant_id).email
                final_bges["vehicules"] = copy.deepcopy(all_data["vehicules"])
                final_bges["batiments"] = copy.deepcopy(all_data["batiments"])
                final_bges["deplacements_dt"] = copy.deepcopy(all_data["deplacements_dt"])
                final_bges["missions"] = copy.deepcopy(all_data["missions"])
                final_bges["devices"] = copy.deepcopy(all_data["devices"])
                final_bges["purchases"] = copy.deepcopy(all_data["purchases"])
                bges_to_return.append(final_bges)
            return Response(bges_to_return, status=status.HTTP_201_CREATED)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def save_vehicules(request):
    laboratoire = Laboratoire.objects.get(referant_id=request.user.pk)
    bges_id = request.data.pop('bges_id')
    bges = BGES.objects.get(pk=bges_id)
    vehicules_to_return = []
    consommations_bges = []
    for vehicule_data in request.data['vehicules']:
        vehicule_id = vehicule_data.pop('pk')
        consommation_data = vehicule_data.pop('consommation')
        consommation_id = consommation_data.pop('pk')
        vehicule_data['laboratoire'] = laboratoire
        vehicule, created = Vehicule.objects.update_or_create(pk=vehicule_id, defaults=vehicule_data)
        consommation_data['bges'] = bges
        consommation_data['vehicule'] = vehicule
        consommation, created = Consommation.objects.update_or_create(pk=consommation_id, defaults=consommation_data)
        consommations_bges.append(consommation.pk)
        serializerv = VehiculeSerializer(vehicule)
        serializerc = ConsommationSerializer(consommation)        
        final_vehicule = copy.deepcopy(serializerv.data)
        final_vehicule['consommation'] = copy.deepcopy(serializerc.data)
        vehicules_to_return.append(final_vehicule)
    # supprimer les consommations plus présentent dans la sauvegarde
    consommations = Consommation.objects.filter(bges_id=bges_id, vehicule_id__isnull=False)
    for consommation in consommations:
        if not consommation.pk in consommations_bges:
            consommation.delete()
    # suprimer les vehicules plus attache a une conso, donc un bges
    vehicules = Vehicule.objects.filter(laboratoire_id=laboratoire.pk)
    for vehicule in vehicules:
        consommations = Consommation.objects.filter(vehicule_id=vehicule.pk)
        if len(consommations) == 0:
            vehicule.delete()
    return Response(vehicules_to_return, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def save_computer_devices(request):
    bges_id = request.data.pop('bges_id')
    ComputerDevice.objects.filter(bges_id=bges_id).delete()
    # ajouter les nouveaux
    for computer_device_data in request.data['devices']:
        computer_device_data['bges_id'] = bges_id
        computer_device = ComputerDevice.objects.create(**computer_device_data)
    computer_devices = ComputerDevice.objects.filter(bges_id=bges_id)
    serializer = ComputerDeviceSerializer(computer_devices, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def save_purchases(request):
    bges_id = request.data.pop('bges_id')
    Purchase.objects.filter(bges_id=bges_id).delete()
    # ajouter les nouveaux
    for purchase_data in request.data['purchases']:
        purchase_data['bges_id'] = bges_id
        purchase = Purchase.objects.create(**purchase_data)
    purchases = Purchase.objects.filter(bges_id=bges_id)
    serializer = PurchaseSerializer(purchases, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def get_all_vehicules(request):
    try:
        laboratoire = Laboratoire.objects.get(referant_id=request.user.pk)
        vehicules = Vehicule.objects.filter(laboratoire_id=laboratoire.pk)
        serializer = VehiculeSCSerializer(vehicules, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def save_batiments(request):
    laboratoire = Laboratoire.objects.get(referant_id=request.user.pk)
    bges_id = request.data.pop('bges_id')
    bges = BGES.objects.get(pk=bges_id)
    batiments_to_return = []
    consommations_bges = []
    for batiment_data in request.data['batiments']:
        batiment_id = batiment_data.pop('pk')
        electricite_data = batiment_data.pop('electricite')
        selfconsumption_data = batiment_data.pop('autoconsommation')
        electricite_id = electricite_data.pop('pk')
        chauffage_data = batiment_data.pop('chauffage')
        chauffage_id = chauffage_data.pop('pk')
        gazRefrigerant = batiment_data.pop('gazRefrigerant')
        batiment_data['laboratoire'] = laboratoire
        # create batiment if not exists
        batiment, created = Batiment.objects.update_or_create(pk=batiment_id, defaults=batiment_data)
        electricite_data['bges'] = bges
        electricite_data['batimentElectricite'] = batiment
        chauffage_data['bges'] = bges
        chauffage_data['batimentChauffage'] = batiment
        # create electricite and chauffage if not exists
        electricite, created = Consommation.objects.update_or_create(pk=electricite_id, defaults=electricite_data)
        consommations_bges.append(electricite.pk)
        chauffage, created = Consommation.objects.update_or_create(pk=chauffage_id, defaults=chauffage_data)
        consommations_bges.append(chauffage.pk)
        all_gaz = []
        all_gaz_ids = []
        for gaz_data in gazRefrigerant:
            gaz_id = gaz_data.pop('pk')
            gaz_data['bges'] = bges
            gaz_data['batimentGazRefrigerant'] = batiment
            gaz, created = GazRefrigerant.objects.update_or_create(pk=gaz_id, defaults=gaz_data)
            all_gaz.append(gaz)
            all_gaz_ids.append(gaz.pk)
        # create selfconsumption
        SelfConsumption.objects.filter(building=batiment, ghgi=bges).delete()
        selfconsumption = SelfConsumption.objects.create(
            building=batiment,
            ghgi=bges,
            total=selfconsumption_data
        )
        # supprimer tous les gazs du batiments à supprimer
        gazs = GazRefrigerant.objects.filter(bges_id=bges_id, batimentGazRefrigerant_id=batiment.pk)
        for cgaz in gazs:
            if not cgaz.pk in all_gaz_ids:
                cgaz.delete()
        # use serializer to return results
        serializerb = BatimentSerializer(batiment)
        serializere = ConsommationSerializer(electricite)
        serializerc = ConsommationSerializer(chauffage)
        serializerg = GazRefrigerantSerializer(all_gaz, many=True)
        serializersc = SelfConsumptionSerializer(selfconsumption)
        final_batiment = copy.deepcopy(serializerb.data)
        final_batiment['electricite'] = copy.deepcopy(serializere.data)
        final_batiment['chauffage'] = copy.deepcopy(serializerc.data)
        final_batiment['gazRefrigerant'] = copy.deepcopy(serializerg.data)
        final_batiment['autoconsommation'] = copy.deepcopy(serializersc.data['total'])
        batiments_to_return.append(final_batiment)
    # supprimer les consommations plus présentent dans la sauvegarde
    consommations = Consommation.objects.filter(bges_id=bges_id, batimentElectricite_id__isnull=False)
    for consommation in consommations:
        if not consommation.pk in consommations_bges:
            consommation.delete()
    consommations = Consommation.objects.filter(bges_id=bges_id, batimentChauffage_id__isnull=False)
    for consommation in consommations:
        if not consommation.pk in consommations_bges:
            consommation.delete()
    # suprimer les batiment plus attache a une conso, donc un bges
    batiments = Batiment.objects.filter(laboratoire_id=laboratoire.pk)
    for batiment in batiments:
        consommationsElectricite = Consommation.objects.filter(batimentElectricite_id=batiment.pk)
        consommationsChauffage = Consommation.objects.filter(batimentChauffage_id=batiment.pk)
        if len(consommationsElectricite) == 0 and len(consommationsChauffage) == 0:
            batiment.delete()
    return Response(batiments_to_return, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def get_all_batiments(request):
    try:
        laboratoire = Laboratoire.objects.get(referant_id=request.user.pk)
        batiments = Batiment.objects.filter(laboratoire_id=laboratoire.pk)
        serializer = BatimentSCSerializer(batiments, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except:
        return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def save_deplacements_dt(request):
    bges_id = request.data.pop('bges_id')
    bges = BGES.objects.get(pk=bges_id)
    # supprimer tous les deplacements
    DeplacementDT.objects.filter(bges_id=bges_id).delete()
    # ajouter les nouveaux
    for deplacement_data in request.data['deplacementsDT']:
        deplacement_data['bges_id'] = bges_id
        deplacement = DeplacementDT.objects.create(**deplacement_data)
    deplacements = DeplacementDT.objects.filter(bges_id=bges_id)
    serializer = DeplacementDTSerializer(deplacements, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes([])
def get_bges_survey_info(request):
    uuid = request.data.pop('uuid')
    try:
        bges = BGES.objects.get(uuid=uuid)
        return Response({
            'year': bges.annee,
            'surveyActive': bges.surveyActive,
            'surveyMessage': bges.surveyMessage,
            'contactEmail': bges.laboratoire.referant.email,
            'labName': bges.laboratoire.nom
            }, status=status.HTTP_201_CREATED)
    except ValidationError:
        return Response('Unvalid uuid', status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def save_survey_message(request):
    bges_id = request.data.pop('bges_id')
    surveyMessage = request.data.pop('surveyMessage')
    BGES.objects.filter(id=bges_id).update(surveyMessage=surveyMessage)
    return Response({}, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes([])
def save_deplacement_dt(request):
    uuid = request.data.pop('uuid')
    deplacementdt = request.data.pop('deplacementdt')
    bges = BGES.objects.get(uuid=uuid)
    # ajouter le nouveau deplacement
    deplacement = DeplacementDT.objects.create(bges_id=bges.pk,**deplacementdt)
    DeplacementDT.objects.filter(id=deplacement.id).update(seqID=deplacement.id)
    deplacement = DeplacementDT.objects.get(id=deplacement.id)
    serializer = DeplacementDTSerializer(deplacement)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def save_missions(request):
    bges_id = request.data.pop('bges_id')
    bges = BGES.objects.get(pk=bges_id)
    # supprimer tous les deplacements
    Mission.objects.filter(bges_id=bges_id).delete()
    # ajouter les nouveaux
    for mission_data in request.data['missions']:
        mission_data['bges_id'] = bges_id
        idMissions = mission_data.pop('idMission')
        mission = Mission.objects.create(**mission_data)
        for idMission in idMissions:
            i = IDMisson.objects.create(nom=idMission, mission=mission)
    missions = Mission.objects.filter(bges_id=bges_id)
    serializer = MissionSerializer(missions, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def submit_data(request):
    bges_id = request.data.pop('bges_id')
    module = request.data.pop('module')
    if module == 'buildings':
        BGES.objects.filter(pk=bges_id).update(buildingsSubmitted=True)
    elif module == 'devices':
        BGES.objects.filter(pk=bges_id).update(devicesSubmitted=True)
    elif module == 'commutes':
        BGES.objects.filter(pk=bges_id).update(commutesSubmitted=True)
    elif module == 'vehicles':
        BGES.objects.filter(pk=bges_id).update(vehiclesSubmitted=True)
    elif module == 'missions':
        BGES.objects.filter(pk=bges_id).update(travelsSubmitted=True)
    elif module == 'purchases':
        BGES.objects.filter(pk=bges_id).update(purchasesSubmitted=True)
    elif module == 'total':
        BGES.objects.filter(pk=bges_id).update(buildingsSubmitted=True)
        BGES.objects.filter(pk=bges_id).update(devicesSubmitted=True)
        BGES.objects.filter(pk=bges_id).update(commutesSubmitted=True)
        BGES.objects.filter(pk=bges_id).update(vehiclesSubmitted=True)
        BGES.objects.filter(pk=bges_id).update(travelsSubmitted=True)
        BGES.objects.filter(pk=bges_id).update(purchasesSubmitted=True)
    bges = BGES.objects.get(pk=bges_id)
    serializer = BGESSerializer(bges)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def activate_bges_survey(request):
    bges_id = request.data.pop('bges_id')
    bges = BGES.objects.get(pk=bges_id)
    BGES.objects.filter(pk=bges_id).update(surveyActive=not bges.surveyActive)
    bges = BGES.objects.get(pk=bges_id)
    serializer = BGESSerializer(bges)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

def get_bges_consommations_data(ghgi_id):
    
    consommations = Consommation.objects.filter(bges_id=ghgi_id)
    to_return = {
        'vehicules': [],
        'batiments': [],
        'deplacements_dt': [],
        'missions': [],
        'devices': [],
        'devices': [],
        'purchases': []
    }
    
    for consommation in consommations:
        if consommation.vehicule_id is not None:
            vehicule = Vehicule.objects.get(pk=consommation.vehicule_id)
            serializerv = VehiculeSerializer(vehicule)
            serializerc = ConsommationSerializer(consommation)
            final_vehicule = copy.deepcopy(serializerv.data)
            final_vehicule['consommation'] = copy.deepcopy(serializerc.data)
            to_return['vehicules'].append(final_vehicule)
        elif consommation.batimentElectricite_id is not None:
            batiment = Batiment.objects.get(pk=consommation.batimentElectricite_id)
            serializerc = ConsommationSerializer(consommation)
            # check if this batiment is already in results
            index = None
            for i, b in enumerate(to_return['batiments']):
                if b['pk'] == batiment.pk:
                    index = i
            if index is None:
                serializerb = BatimentSerializer(batiment)
                final_batiment = copy.deepcopy(serializerb.data)
                final_batiment['electricite'] = copy.deepcopy(serializerc.data)
                to_return['batiments'].append(final_batiment)
            else :
                to_return['batiments'][index]['electicite'] = copy.deepcopy(serializerc.data)
        elif consommation.batimentChauffage_id is not None:
            batiment = Batiment.objects.get(pk=consommation.batimentChauffage_id)
            serializerc = ConsommationSerializer(consommation)
            # check if this batiment is already in results
            index = None
            for i, b in enumerate(to_return['batiments']):
                if b['pk'] == batiment.pk:
                    index = i
            if index is None:
                serializerb = BatimentSerializer(batiment)
                final_batiment = copy.deepcopy(serializerb.data)
                final_batiment['chauffage'] = copy.deepcopy(serializerc.data)
                to_return['batiments'].append(final_batiment)
            else :
                to_return['batiments'][index]['chauffage'] = copy.deepcopy(serializerc.data)
    
    for i, batiment in enumerate(to_return['batiments']):
        gaz_refrigerant = GazRefrigerant.objects.filter(bges_id=ghgi_id, batimentGazRefrigerant_id=batiment['pk'])
        gserializer = GazRefrigerantSerializer(gaz_refrigerant, many=True)
        to_return['batiments'][i]['gazRefrigerant'] = gserializer.data
        try:
            self_consumption = SelfConsumption.objects.get(ghgi_id=ghgi_id, building_id=batiment['pk'])
            scserializer = SelfConsumptionSerializer(self_consumption)
            to_return['batiments'][i]['autoconsommation'] = scserializer.data['total']
        except SelfConsumption.DoesNotExist:
            to_return['batiments'][i]['autoconsommation'] = 0        
    
    # add commutes
    deplacementsDT = DeplacementDT.objects.filter(bges_id=ghgi_id)
    serializerddt = DeplacementDTSerializer(deplacementsDT, many=True)
    to_return['deplacements_dt'] = serializerddt.data
    
    # add travels
    missions = Mission.objects.filter(bges_id=ghgi_id)
    serializerdmissions = MissionSerializer(missions, many=True)
    to_return['missions'] = serializerdmissions.data

    # add computer devices
    computer_devices = ComputerDevice.objects.filter(bges_id=ghgi_id)
    serializercd = ComputerDeviceSerializer(computer_devices, many=True)
    to_return['devices'] = serializercd.data

    # add purchases
    purchases = Purchase.objects.filter(bges_id=ghgi_id)
    serializerp = PurchaseSerializer(purchases, many=True)
    to_return['purchases'] = serializerp.data

    return to_return

@api_view(['POST'])
def get_bges_consommations(request):
    bges_id = request.data.pop('bges_id')
    to_return = get_bges_consommations_data(bges_id)
    return Response(to_return, status=status.HTTP_201_CREATED)
