from rest_framework import serializers
from .models import BGES, Vehicule, Batiment, Consommation, SelfConsumption, GazRefrigerant, DeplacementDT, Mission, ComputerDevice, Purchase


class BGESSerializer(serializers.ModelSerializer):        
    class Meta:
        model = BGES
        fields = ['pk', 'uuid', 'created', 'annee', 'nbChercheurs', 'nbEnseignants',
                  'nbITA', 'nbPostDoc', 'budget', 'surveyActive', 'surveyMessage',
                  'buildingsSubmitted', 'commutesSubmitted', 'devicesSubmitted',
                  'travelsSubmitted', 'vehiclesSubmitted', 'purchasesSubmitted']

class ConsommationSerializer(serializers.ModelSerializer):        
    class Meta:
        model = Consommation
        fields = ['pk', 'cJanvier', 'cFevrier', 'cMars', 'cAvril', 'cMai', 'cJuin',
                  'cJuillet', 'cAout', 'cSeptembre', 'cOctobre', 'cNovembre', 'cDecembre',
                  'cTotale', 'isMensuelle']

class DeplacementDTSerializer(serializers.ModelSerializer):        
    class Meta:
        model = DeplacementDT
        fields = '__all__'

class MissionSerializer(serializers.ModelSerializer):
    idMission = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='nom'
     )
    class Meta:
        model = Mission
        fields = '__all__'

class GazRefrigerantSerializer(serializers.ModelSerializer):        
    class Meta:
        model = GazRefrigerant
        fields = ['pk', 'nom', 'cTotale']

class VehiculeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicule
        fields = ['pk', 'nom', 'typeVehicule', 'motorisation', 'unite', 'puissance', 'nbMoteurs', 'shp', 'controleOperationnel', 'consommation']

class PurchaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchase
        fields = ['pk', 'code', 'amount']

class VehiculeSCSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicule
        fields = ['pk', 'nom', 'typeVehicule', 'motorisation', 'unite', 'puissance', 'nbMoteurs', 'shp', 'controleOperationnel']

class ComputerDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComputerDevice
        fields = ['pk', 'type', 'model', 'amount', 'acquisitionYear']

class BatimentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Batiment
        fields = ['pk', 'nom', 'superficie', 'occupation', 'isOwnedByLab', 'chauffageElectrique', 'typeChauffage',
                  'reseauChauffageUrbain', 'autoProduction', 'consommationElectricite', 'consommationChauffage',
                  'site']

class SelfConsumptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SelfConsumption
        fields = '__all__'

class BatimentSCSerializer(serializers.ModelSerializer):
    class Meta:
        model = Batiment
        fields = ['pk', 'nom', 'superficie', 'occupation', 'isOwnedByLab', 'chauffageElectrique', 'typeChauffage',
                  'reseauChauffageUrbain', 'autoProduction', 'site']
