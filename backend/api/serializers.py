from rest_framework import serializers
from .models import Initiative, Laboratoire, Tutelle, Message, DomaineScientifique, DomaineDisciplinaire, SousDomaine, Site


class SitesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = ['nom']


class TutellesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tutelle
        fields = ['pk', 'nom']


class DisciplinesSerializer(serializers.ModelSerializer):
    class Meta:
        model = SousDomaine
        fields = ['pk', 'nom']


class InitiativesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Initiative
        fields = ['pk', 'created', 'last_update', 'description', 'initiative_institutionnelle',
                  'groupe_description', 'deplacementspro_description', 'deplacementsdt_description',
                  'actrecherche_description', 'batiments_description', 'numerique_description', 'ecoresponsable',
                  'sociale', 'enquetes', 'communication', 'autres_initiatives', 'valid', 'BGESext',
                  'BGES_annee', 'engagements']


class LaboratoiresSerializer(serializers.ModelSerializer):
    tutelles = TutellesSerializer(many=True)
    sites = SitesSerializer(many=True)
    disciplines = DisciplinesSerializer(many=True)
    class Meta:
        model = Laboratoire
        fields = ['pk', 'nom', 'tutelles', 'sitePrincipal', 'pays', 'sites', 'disciplines',
                  'adminName', 'tailleAglomeration', 'latitude', 'longitude']


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
