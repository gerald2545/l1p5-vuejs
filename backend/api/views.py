from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework import viewsets
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from django.core.mail import send_mail
from django.conf import settings
import copy

from ..users.models import Membre
from .models import Initiative, Laboratoire, Message, Tutelle, SousDomaine, Site, LaboratoireSousDomaine
from .serializers import InitiativesSerializer, MessageSerializer, TutellesSerializer, DisciplinesSerializer, LaboratoiresSerializer
from ..users.models import Membre, L1P5User


# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))

@api_view(['POST'])
@permission_classes([])
def save_initiative(request):
    if request.user.is_authenticated:
        initiative_id = request.data.pop('pk')
        initiative, created = Initiative.objects.update_or_create(pk=initiative_id, defaults=request.data)
        laboratoire, created = Laboratoire.objects.update_or_create(referant_id=request.user.pk, defaults={'initiative_id': initiative.pk})
        serializer = InitiativesSerializer(initiative)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_initiative(request):
    if request.user.is_authenticated:
        try:
            laboratoire = Laboratoire.objects.get(referant_id=request.user.pk)
            if laboratoire.initiative != None :
                serializer = InitiativesSerializer(laboratoire.initiative)
                return Response(serializer.data)
            else:
                return Response(Initiative.objects.none())
        except:
            return Response(Initiative.objects.none())
    return Response(Initiative.objects.none())

@api_view(['GET'])
@permission_classes([])
def get_initiatives(request):
    initatives = Initiative.objects.filter(valid=1)
    serializer = InitiativesSerializer(initatives, many=True)
    initatives_to_return = []
    for initiative in serializer.data:
        initative_to_return = copy.deepcopy(initiative)
        laboratoire = Laboratoire.objects.get(initiative_id=initative_to_return['pk'])
        membre = Membre.objects.get(user_id=laboratoire.referant_id)
        serializer = LaboratoiresSerializer(laboratoire)
        tutelles = []
        for tutelle in serializer.data["tutelles"]:
            tutelles.append(tutelle["nom"])
        initative_to_return["laboratoire"] = {"nom": laboratoire.nom, "longitude": laboratoire.longitude, "latitude": laboratoire.latitude, "tutelle": tutelles}
        initative_to_return["membre"] = {"nom": membre.nom, "prenom": membre.prenom, "email": membre.email}
        initatives_to_return.append(initative_to_return)
    return Response(initatives_to_return)

@api_view(['GET'])
def get_initiatives_admin(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            initatives = Initiative.objects.filter(valid=0)
            serializer = InitiativesSerializer(initatives, many=True)
            initatives_to_return = []
            for initiative in serializer.data:
                initative_to_return = copy.deepcopy(initiative)
                laboratoire = Laboratoire.objects.get(initiative_id=initative_to_return['pk'])
                membre = Membre.objects.get(user_id=laboratoire.referant_id)
                serializer = LaboratoiresSerializer(laboratoire)
                tutelles = []
                for tutelle in serializer.data["tutelles"]:
                    tutelles.append(tutelle["nom"])
                initative_to_return["laboratoire"] = {"nom": laboratoire.nom, "longitude": laboratoire.longitude, "latitude": laboratoire.latitude, "tutelle": tutelles}
                initative_to_return["membre"] = {"nom": membre.nom, "prenom": membre.prenom, "email": membre.email}
                initatives_to_return.append(initative_to_return)
            return Response(initatives_to_return)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
def validate_initiative(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            initative = Initiative.objects.update_or_create(pk=request.data["pk"], defaults={"valid":1})
            send_mail(
                  'Votre initiative Labos 1point5',
                  '''Bonjour {0} {1},

merci pour l'initiative que vous avez proposé, elle est maintenant visible
sur notre site depuis la page https://labos1point5.org/les-initiatives.

A très bientôt,
le collectif Labos 1point5'''.format(request.data["membre"]["prenom"], request.data["membre"]["nom"]),
                  getattr(settings, 'DEFAULT_FROM_EMAIL'),
                  [request.data["membre"]["email"]],
                  fail_silently=False
                  )
            initatives = Initiative.objects.filter(valid=0)
            serializer = InitiativesSerializer(initatives, many=True)
            initatives_to_return = []
            for initiative in serializer.data:
                initative_to_return = copy.deepcopy(initiative)
                laboratoire = Laboratoire.objects.get(initiative_id=initative_to_return['pk'])
                membre = Membre.objects.get(user_id=laboratoire.referant_id)
                serializer = LaboratoiresSerializer(laboratoire)
                tutelles = []
                for tutelle in serializer.data["tutelles"]:
                    tutelles.append(tutelle["nom"])
                initative_to_return["laboratoire"] = {"nom": laboratoire.nom, "longitude": laboratoire.longitude, "latitude": laboratoire.latitude, "tutelle": tutelles}
                initative_to_return["membre"] = {"nom": membre.nom, "prenom": membre.prenom, "email": membre.email}
                initatives_to_return.append(initative_to_return)
            return Response(initatives_to_return)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

@api_view(['POST'])
@permission_classes([])
def add_message(request):
    serializer = MessageSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        # email envoye a l'utilisateur
        send_mail(
                  'Votre message à Labos 1point5',
                  '''Bonjour {0} {1},

nous avons bien reçu votre message et vous contacterons le plus rapidement possible.

A très bientôt,
le collectif Labos 1point5'''.format(request.data['prenom'], request.data['nom']),
                  getattr(settings, 'DEFAULT_FROM_EMAIL'),
                  [request.data['email']],
                  fail_silently=False
                  )
        # email envoye a contact
        send_mail(
                  'Contact',
                  '''Prenom: {0}
Nom: {1}
Email: {2}
Message: {3}
'''.format(request.data['prenom'], request.data['nom'], request.data['email'], request.data['message']),
                  request.data['email'],
                  [getattr(settings, 'DEFAULT_FROM_EMAIL')],
                  fail_silently=False
                  )
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@permission_classes([])
def add_message_dpd(request):
    serializer = MessageSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        # email envoye a l'utilisateur
        send_mail(
                  'Votre message au DPD de Labos 1point5',
                  '''Bonjour {0} {1},

nous avons bien reçu votre message et vous contacterons le plus rapidement possible.

A très bientôt,
le délégué à la protection des données de Labos 1point5'''.format(request.data['prenom'], request.data['nom']),
                  getattr(settings, 'DEFAULT_FROM_EMAIL'),
                  [request.data['email']],
                  fail_silently=False
                  )
        # email envoye au DPD
        send_mail(
                  'Contact',
                  '''Prenom: {0}
Nom: {1}
Email: {2}
Message: {3}
'''.format(request.data['prenom'], request.data['nom'], request.data['email'], request.data['message']),
                  request.data['email'],
                  ['dpd@labos1point5.org'],
                  fail_silently=False
                  )
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes([])
def get_tutelles(request):
    tutelles = Tutelle.objects.all()
    serializer = TutellesSerializer(tutelles, many=True)
    return Response(serializer.data)

@api_view(['GET'])
@permission_classes([])
def get_disciplines(request):
    sous_domaines = SousDomaine.objects.all()
    serializer = DisciplinesSerializer(sous_domaines, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def save_laboratoire(request):
    if request.user.is_authenticated:
        # sub objects data
        sites_data = request.data.pop('sites')
        tutelles_data = request.data.pop('tutelles')
        disciplines_data = request.data.pop('disciplines')
        request.data["referant"] = request.user
        # build the laboratoire object if exists or create it
        laboratoire = Laboratoire.objects.filter(referant=request.user)
        lid = None
        if len(laboratoire) == 1:
            lid = laboratoire[0].pk
        laboratoire, created = Laboratoire.objects.update_or_create(pk=lid, defaults=request.data)
        laboratoire.tutelles.clear()
        for tutelle_data in tutelles_data:
            if (type(tutelle_data) is str) :
                t  = Tutelle.objects.create(nom=tutelle_data)
                laboratoire.tutelles.add(t)
            else :
                t  = Tutelle.objects.get(pk=tutelle_data['pk'])
                laboratoire.tutelles.add(t)
        laboratoire.disciplines.clear()
        for discipline_data in disciplines_data:
            d, created = LaboratoireSousDomaine.objects.update_or_create(
                discipline_id=discipline_data['pk'],
                laboratoire_id=laboratoire.pk,
                pourcentage=discipline_data['pourcentage']
            )
        lab_sites = Site.objects.filter(laboratoire_id=laboratoire.pk)
        for lab_site in lab_sites:
            lab_site.delete()
        for site_data in sites_data:
            s = Site.objects.create(nom=site_data, laboratoire=laboratoire)
            s.save()
        serializer = LaboratoiresSerializer(laboratoire)
        disciplines = LaboratoireSousDomaine.objects.filter(laboratoire_id=laboratoire.pk)
        to_return = copy.deepcopy(serializer.data)
        to_return['disciplines'] = []
        for discipline in disciplines:
            disc_nom = SousDomaine.objects.filter(pk=discipline.discipline_id)[0].nom
            to_return["disciplines"].append({
                'pk': discipline.discipline_id,
                'nom': disc_nom,
                'pourcentage': discipline.pourcentage
            })
        return Response(to_return, status=status.HTTP_201_CREATED)
    return Response(Laboratoire.objects.none())

@api_view(['GET'])
def get_laboratoire(request):
    if request.user.is_authenticated:
        laboratoire = Laboratoire.objects.filter(referant=request.user)
        if laboratoire.exists():
            serializer = LaboratoiresSerializer(laboratoire[0])
            to_return = copy.deepcopy(serializer.data)
            disciplines = LaboratoireSousDomaine.objects.filter(laboratoire_id=laboratoire[0].pk)
            to_return["disciplines"] = []
            for discipline in disciplines:
                disc_nom = SousDomaine.objects.filter(pk=discipline.discipline_id)[0].nom
                to_return["disciplines"].append({
                    'pk': discipline.discipline_id,
                    'nom': disc_nom,
                    'pourcentage': discipline.pourcentage
                })
            return Response(to_return)
        else :
            return Response(None)
    return Response(None, status=status.HTTP_400_BAD_REQUEST)
