from django.db import models
from django.conf import settings


class Initiative(models.Model):
    created = models.DateField(auto_now_add=True)
    last_update = models.DateField(auto_now=True)
    description = models.CharField(max_length=1000, null=True, blank=True)
    initiative_institutionnelle = models.BooleanField(default=False)
    BGESext = models.IntegerField(null=True)
    BGES_annee = models.CharField(max_length=50, null=True, blank=True)
    engagements = models.CharField(max_length=1000, null=True, blank=True)
    groupe_description = models.CharField(max_length=1000, null=True, blank=True)
    deplacementspro_description = models.CharField(max_length=1000, null=True, blank=True)
    deplacementsdt_description = models.CharField(max_length=1000, null=True, blank=True)
    actrecherche_description = models.CharField(max_length=1000, null=True, blank=True)
    batiments_description = models.CharField(max_length=1000, null=True, blank=True)
    numerique_description = models.CharField(max_length=1000, null=True, blank=True)
    ecoresponsable = models.CharField(max_length=1000, null=True, blank=True)
    sociale = models.CharField(max_length=1000, null=True, blank=True)
    enquetes = models.CharField(max_length=1000, null=True, blank=True)
    communication = models.CharField(max_length=1000, null=True, blank=True)
    autres_initiatives = models.CharField(max_length=1000, null=True, blank=True)
    valid = models.BooleanField(default=False)

class Tutelle(models.Model):
    nom = models.CharField(max_length=100)

class DomaineScientifique(models.Model):
    nom = models.CharField(max_length=50)

class DomaineDisciplinaire(models.Model):
    nom = models.CharField(max_length=200)
    domaine_scientifique = models.ForeignKey(DomaineScientifique, related_name='domaine_disciplinaire', on_delete=models.CASCADE)

class SousDomaine(models.Model):
    nom = models.CharField(max_length=100)
    domaine_disciplinaire = models.ForeignKey(DomaineDisciplinaire, related_name='sous_domaine', on_delete=models.CASCADE)

class Laboratoire(models.Model):
    referant = models.ForeignKey(settings.AUTH_USER_MODEL,
                                 on_delete=models.CASCADE,
                                 related_name='initiative')
    nom = models.CharField(max_length=50)
    tutelles = models.ManyToManyField(Tutelle, related_name="laboratoires")
    disciplines = models.ManyToManyField(SousDomaine, through='LaboratoireSousDomaine')
    sitePrincipal = models.CharField(max_length=50)
    pays = models.CharField(max_length=50)
    adminName = models.CharField(max_length=50)
    tailleAglomeration = models.IntegerField(null=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=7)
    longitude = models.DecimalField(max_digits=10, decimal_places=7)
    initiative = models.ForeignKey(Initiative, related_name='laboratoire', on_delete=models.CASCADE, null=True)

class LaboratoireSousDomaine(models.Model):
    discipline = models.ForeignKey(SousDomaine, on_delete=models.CASCADE)
    laboratoire = models.ForeignKey(Laboratoire, on_delete=models.CASCADE)
    pourcentage = models.IntegerField()

class Site(models.Model):
    nom = models.CharField(max_length=50)
    laboratoire = models.ForeignKey(Laboratoire, related_name='sites', on_delete=models.CASCADE)
    
class Message(models.Model):
    date = models.DateField(auto_now_add=True)
    email = models.EmailField()
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    message = models.CharField(max_length=1500)
