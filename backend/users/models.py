from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, AbstractUser
from .managers import UserManager
from django.conf import settings
from ..api.models import Tutelle


class L1P5User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(max_length=190, unique=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(('active'), default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'

class Discipline(models.Model):
    nom = models.CharField(max_length=150)

class Statut(models.Model):
    nom = models.CharField(max_length=50)

class Genre(models.Model):
    nom = models.CharField(max_length=50)

class Membre(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             related_name='membre',
                             blank=True, null=True)
    date = models.DateField(auto_now_add=True)
    email = models.EmailField(max_length=190, unique=True)
    nom = models.CharField(max_length=50, null=True)
    prenom = models.CharField(max_length=50, null=True)
    ville = models.CharField(max_length=50, null=True)
    pays = models.CharField(max_length=50, null=True)
    discipline = models.CharField(max_length=100, null=True)
    disciplineCNU = models.ForeignKey(Discipline, related_name='membre', on_delete=models.CASCADE, blank=True, null=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=7, null=True)
    longitude = models.DecimalField(max_digits=10, decimal_places=7, null=True)
    valid = models.BooleanField(default=False)
    unaccepted = models.BooleanField(default=False)

    # info membre equipe
    genre = models.ForeignKey(Genre, related_name='membre', on_delete=models.CASCADE, blank=True, null=True)
    anneeNaissance = models.CharField(max_length=4, blank=True)
    statut = models.ForeignKey(Statut, related_name='membre', on_delete=models.CASCADE, blank=True, null=True)
    percETP =  models.IntegerField(null=True)
    laboratoire = models.CharField(max_length=50, blank=True)
    tutelles = models.ManyToManyField(Tutelle, related_name="membre")
    commentaire = models.CharField(max_length=1500, blank=True)
    engagementAssociatif = models.CharField(max_length=500, blank=True)
    engagementInstitutionel = models.CharField(max_length=500, blank=True)
    engagementAutres = models.CharField(max_length=500, blank=True)
    msgEmpreinte = models.CharField(max_length=500, blank=True)
    msgEnquete = models.CharField(max_length=500, blank=True)
    msgReflexion = models.CharField(max_length=500, blank=True)
    msgExperimentation = models.CharField(max_length=500, blank=True)
    msgCommunication = models.CharField(max_length=500, blank=True)
    msgTechnique = models.CharField(max_length=500, blank=True)
    msgEnseignement = models.CharField(max_length=500, blank=True)

    # info annuaire
    isInAnnuaire = models.BooleanField(default=False)

    # info access data GES 1point5
    askedDataAccess = models.BooleanField(default=False)
    dataAccessMsg = models.CharField(max_length=500, blank=True)
    hasDataAccess = models.BooleanField(default=False)

class Equipe(models.Model):
    nom = models.CharField(max_length=25)
    details = models.CharField(max_length=50)
    membres = models.ManyToManyField(Membre, related_name="equipes")
