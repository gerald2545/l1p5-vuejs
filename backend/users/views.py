import copy
from datetime import date

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.generics import CreateAPIView
from .serializers import L1P5UserRegistrationSerializer
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.serializers import TokenObtainSerializer, TokenObtainPairSerializer
from rest_framework.decorators import api_view, permission_classes
from rest_framework_simplejwt.views import TokenObtainPairView
from django.core.mail import send_mail
from rest_framework import generics, status, viewsets, filters
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_decode
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.core.mail import EmailMessage
from django.shortcuts import render

from .models import Membre, Equipe, L1P5User, Discipline, Genre, Statut
from ..api.models import Tutelle
from .serializers import L1P5TokenObtainPairSerializer, MembreSerializer, MembresEquipesSerializer, MembresLocationsSerializer, EquipesSerializer, PasswordResetSerializer, DisciplinesSerializer, GenresSerializer, StatutsSerializer


class PasswordResetView(generics.GenericAPIView):
    """
    Calls Django Auth PasswordResetForm save method.
    Accepts the following POST parameters: email
    Returns the success/fail message.
    """
    serializer_class = PasswordResetSerializer
    permission_classes = (AllowAny,)
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            {"detail": "L'email a été envoyé."},
            status=status.HTTP_200_OK
        )

class RegisterView(CreateAPIView):
    serializer_class = L1P5UserRegistrationSerializer
    permission_classes = (AllowAny,)
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            final_user = copy.deepcopy(self.request.data)
            final_user["is_active"] = False
            user = serializer.save(final_user)
            data = {'email': self.request.data['email'],
                    'password': self.request.data['password1']}
            headers = self.get_success_headers(serializer.data)
            mail_subject = 'Activation compte Labos 1point5'
            current_site = get_current_site(request)
            message = render_to_string('registration/acc_active_email.html', {
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.id)),
                'user': user,
                'token': default_token_generator.make_token(user),
            })
            email = EmailMessage(mail_subject, message, to=[user.email])
            email.send()
            return Response(status=status.HTTP_201_CREATED,
                            headers=headers)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

class L1P5TokenObtainPairView(TokenObtainPairView):
    serializer_class = L1P5TokenObtainPairSerializer

def activate_account(request, uidb64, token):
    """
    View to activate account
    """
    validlink = False
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = L1P5User.objects.get(id=uid)
    except(TypeError, ValueError, OverflowError, L1P5User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        validlink = True
        user.is_active = True
        user.save()
    return render(request,
                  'registration/acc_activated.html',
                  {'validlink': validlink})

@api_view(['POST'])
@permission_classes([])
def user_exists(request):
    if L1P5User.objects.filter(email=request.data['email']).exists():
        return Response({'exists': True}, status=status.HTTP_201_CREATED)
    else:
        return Response({'exists': False}, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes([])
def member_exists(request):
    if Membre.objects.filter(email=request.data['email']).exists():
        return Response({'exists': True}, status=status.HTTP_201_CREATED)
    else:
        return Response({'exists': False}, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes([])
def add_membre_newsletter(request):
    equipes_data = request.data.pop('equipes')
    tutelles_data = request.data.pop('tutelles')
    discipline_data = request.data.pop('disciplineCNU')
    genre_data = request.data.pop('genre')
    statut_data = request.data.pop('statut')
    if Membre.objects.filter(email=request.data['email']).exists():
        Membre.objects.filter(email=request.data['email']).update(**request.data)
        membre = Membre.objects.get(email=request.data['email'])
    else :
        membre = Membre.objects.create(**request.data)
    for equipe_data in equipes_data:
        e  = Equipe.objects.get(nom=equipe_data)
        membre.equipes.add(e)
    if discipline_data is not None:
        if (isinstance(discipline_data, str)):
            membre.disciplineCNU  = Discipline.objects.get(nom=discipline_data)
        else:
            membre.disciplineCNU  = Discipline.objects.get(pk=discipline_data['pk'])
    membre.save()
    serializer = MembreSerializer(membre)
    send_mail(
                'Rejoindre le collectif Labos 1point5',
                '''Bonjour,

Nous avons bien reçu et vous remercions de votre inscription à la lettre d'information de Labos 1point5.
Nous vous tiendrons regulièrement informé.e des évolutions du collectif.

à très bientôt !
le collectif Labos 1point5''',
                getattr(settings, 'DEFAULT_FROM_EMAIL'),
                [request.data['email']],
                fail_silently=False
                )
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def unaccept_membre_gdr(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            Membre.objects.update_or_create(email=request.data['email'], defaults={"unaccepted":1, "valid":0})
            all_membres = Membre.objects.all()
            all_membres_gdr_ids = []
            mserializer = MembreSerializer(all_membres, many=True)
            for membre in mserializer.data:
                if "Réflexion" in membre['equipes'] :
                    if  len(membre['equipes']) > 1:
                        all_membres_gdr_ids.append(membre["pk"])
                elif len(membre['equipes']) > 0:
                    all_membres_gdr_ids.append(membre["pk"])
            all_membres_gdr = Membre.objects.filter(pk__in=all_membres_gdr_ids)
            serializer = MembreSerializer(all_membres_gdr, many=True)
            return Response(serializer.data)

@api_view(['POST'])
def validate_membre_gdr(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            Membre.objects.update_or_create(email=request.data['email'], defaults={"valid":1, "unaccepted":0})
            membre = Membre.objects.get(email=request.data['email'])
            les_equipes = ''
            for e in membre.equipes.all() :
                les_equipes += '  - « ' + e.details + ' », liste : '
                if e.nom == 'Empreinte':
                    les_equipes += '1p5-empreinte@le-pic.org ;'
                elif e.nom == 'Enquête':
                    les_equipes += '1p5-enquete@le-pic.org ;'
                elif e.nom == 'Expérimentation':
                    les_equipes += '1p5-experimentation@le-pic.org ;'
                elif e.nom == 'Communication':
                    les_equipes += '1p5-com-veille@le-pic.org ;'
                elif e.nom == 'Technique':
                    les_equipes += '1p5-technique@le-pic.org ;'
                elif e.nom == 'Enseignement':
                    les_equipes += '1p5-enseignement@le-pic.org ;'
                les_equipes += '\n'
            send_mail(
            'Validation inscription au GDR Labos 1point5',
            '''Bonjour {0} {1},

Nous vous remercions de votre volonté de vous impliquer dans les travaux de recherche
du Groupement de Recherche (GDR) Labos 1point5.

Vous êtes inscrit.e sur la liste mail de l’axe / équipe :
{2}
Vous pouvez désormais participer aux échanges en utilisant la liste de l'axe / équipe
que vous souhaitez contacter.

Vous pouvez également rejoindre les canaux de discussion du collectif (https://team.picasoft.net/labos-1point5) en suivant ce lien d'invitation :
https://team.picasoft.net/signup_user_complete/?id=6r7cx4f1ufbu3n7ie95fytod3y.
Vous trouverez de l'aide pour l'utilisation de cet outil de communication à l'adresse : https://doc.picasoft.net/modules/mattermost01

à très bientôt !
le GDR Labos 1point5'''.format(membre.prenom, membre.nom, les_equipes),
                    getattr(settings, 'DEFAULT_FROM_EMAIL'),
                    [request.data['email']],
                    fail_silently=False
                    )
            all_membres = Membre.objects.all()
            all_membres_gdr_ids = []
            mserializer = MembreSerializer(all_membres, many=True)
            for membre in mserializer.data:
                if "Réflexion" in membre['equipes'] :
                    if  len(membre['equipes']) > 1:
                        all_membres_gdr_ids.append(membre["pk"])
                elif len(membre['equipes']) > 0:
                    all_membres_gdr_ids.append(membre["pk"])
            all_membres_gdr = Membre.objects.filter(pk__in=all_membres_gdr_ids)
            serializer = MembreSerializer(all_membres_gdr, many=True)
            return Response(serializer.data)
    return Response(Membre.objects.none())

@api_view(['POST'])
@permission_classes([])
def add_membre_gdr(request):

    equipes_data = request.data.pop('equipes')
    tutelles_data = request.data.pop('tutelles')
    discipline_data = request.data.pop('disciplineCNU')
    genre_data = request.data.pop('genre')
    statut_data = request.data.pop('statut')

    # test if membre already in GDR
    is_gdr_member = False
    if Membre.objects.filter(email=request.data['email']).exists():
        membre = Membre.objects.get(email=request.data['email'])
        mserializer = MembreSerializer(membre)
        if "Réflexion" in mserializer.data['equipes'] :
            if  len(mserializer.data['equipes']) > 1:
                is_gdr_member = True
        elif len(mserializer.data['equipes']) > 0:
            is_gdr_member = True

    if is_gdr_member:
        return Response({"error": "Member already registred."})
    else :
        if Membre.objects.filter(email=request.data['email']).exists():
            Membre.objects.filter(email=request.data['email']).update(date=date.today(), **request.data)
            membre = Membre.objects.get(email=request.data['email'])
        else :
            membre = Membre.objects.create(**request.data)
        for equipe_data in equipes_data:
            e  = Equipe.objects.get(nom=equipe_data)
            membre.equipes.add(e)
        if discipline_data is not None:
            if (isinstance(discipline_data, str)):
                membre.disciplineCNU  = Discipline.objects.get(nom=discipline_data)
            else:
                membre.disciplineCNU  = Discipline.objects.get(pk=discipline_data['pk'])
        if len(equipes_data) > 0 :
            membre.genre = Genre.objects.get(pk=genre_data['pk'])
            membre.statut = Statut.objects.get(pk=statut_data['pk'])
            for tutelle_data in tutelles_data:
                if (type(tutelle_data) is str) :
                    t  = Tutelle.objects.create(nom=tutelle_data)
                    membre.tutelles.add(t)
                else :
                    t  = Tutelle.objects.get(pk=tutelle_data['pk'])
                    membre.tutelles.add(t)
        membre.save()
        serializer = MembreSerializer(membre)
        send_mail(
                'Rejoindre le GDR Labos 1point5',
                '''Bonjour {0} {1},

Nous vous remercions de votre volonté de vous impliquer dans les travaux de recherche
du Groupement de Recherche (GDR) Labos 1point5.

Plus de details vous seront communiqués par email lorsque votre inscription sera validée
par la direction du GDR.

à très bientôt !
le GDR Labos 1point5'''.format(request.data['prenom'], request.data['nom']),
                getattr(settings, 'DEFAULT_FROM_EMAIL'),
                [request.data['email']],
                fail_silently=False
                )
        return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes([])
def add_membre_reflexion(request):
    equipes_data = request.data.pop('equipes')
    tutelles_data = request.data.pop('tutelles')
    discipline_data = request.data.pop('disciplineCNU')
    genre_data = request.data.pop('genre')
    statut_data = request.data.pop('statut')
    if Membre.objects.filter(email=request.data['email']).exists():
        Membre.objects.filter(email=request.data['email']).update(**request.data)
        membre = Membre.objects.get(email=request.data['email'])
    else :
        membre = Membre.objects.create(**request.data)
    for equipe_data in equipes_data:
        e  = Equipe.objects.get(nom=equipe_data)
        membre.equipes.add(e)
    if discipline_data is not None:
        if (isinstance(discipline_data, str)):
            membre.disciplineCNU  = Discipline.objects.get(nom=discipline_data)
        else:
            membre.disciplineCNU  = Discipline.objects.get(pk=discipline_data['pk'])
    if len(equipes_data) > 0 :
        membre.genre = Genre.objects.get(pk=genre_data['pk'])
        membre.statut = Statut.objects.get(pk=statut_data['pk'])
        for tutelle_data in tutelles_data:
            if (type(tutelle_data) is str) :
                t  = Tutelle.objects.create(nom=tutelle_data)
                membre.tutelles.add(t)
            else :
                t  = Tutelle.objects.get(pk=tutelle_data['pk'])
                membre.tutelles.add(t)
    membre.save()
    serializer = MembreSerializer(membre)
    send_mail(
            'Rejoindre l\'équipe réflexion de Labos 1point5',
            '''Bonjour {0} {1},

Nous vous remercions de votre volonté de vous impliquer dans l'équipe réflexion de
Labos 1point5.

Vous êtes inscrit.e sur la liste email : 1p5-reflexion@le-pic.org
Vous pouvez désormais participer aux échanges de l'équipe en utilisant cette liste.

à très bientôt !
l'équipe réflexion Labos 1point5'''.format(request.data['prenom'], request.data['nom']),
            getattr(settings, 'DEFAULT_FROM_EMAIL'),
            [request.data['email']],
            fail_silently=False
            )
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def save_membre(request):
    if request.user.is_authenticated:
        membre_id = request.data.pop('pk')
        equipes_data = request.data.pop('equipes')
        tutelles_data = request.data.pop('tutelles')
        discipline_data = request.data.pop('disciplineCNU')
        genre_data = request.data.pop('genre')
        statut_data = request.data.pop('statut')
        request.data["user"] = request.user
        membre, created = Membre.objects.update_or_create(pk=membre_id, defaults=request.data)
        membre.equipes.clear()
        for equipe_data in equipes_data:
            e  = Equipe.objects.get(nom=equipe_data)
            membre.equipes.add(e)
        if (isinstance(discipline_data, str)):
            membre.disciplineCNU  = Discipline.objects.get(nom=discipline_data)
        else:
            membre.disciplineCNU  = Discipline.objects.get(pk=discipline_data['pk'])
        if len(equipes_data) > 0 :
            membre.genre = Genre.objects.get(pk=genre_data['pk'])
            membre.statut = Statut.objects.get(pk=statut_data['pk'])
            for tutelle_data in tutelles_data:
                if (type(tutelle_data) is str) :
                    t  = Tutelle.objects.create(nom=tutelle_data)
                    membre.tutelles.add(t)
                else :
                    t  = Tutelle.objects.get(pk=tutelle_data['pk'])
                    membre.tutelles.add(t)
        membre.save()
        serializer = MembreSerializer(membre)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(Membre.objects.none())

@api_view(['POST'])
@permission_classes([])
def desabonnement_liste(request):
    liste = request.data.pop('liste')
    if Membre.objects.filter(email=request.data['email']).exists():
        membre = Membre.objects.get(email=request.data['email'])
        if liste == '1p5-equipes':
            membre.equipes.clear()
        elif liste == '1p5-newsletter':
            membre.delete()
        elif liste == '1p5-com-veille':
            equipe = Equipe.objects.get(nom='Communication')
            membre.equipes.remove(equipe)
        elif liste == '1p5-empreinte':
            equipe = Equipe.objects.get(nom='Empreinte')
            membre.equipes.remove(equipe)
        elif liste == '1p5-enquete':
            equipe = Equipe.objects.get(nom='Enquête')
            membre.equipes.remove(equipe)
        elif liste == '1p5-experimentation':
            equipe = Equipe.objects.get(nom='Expérimentation')
            membre.equipes.remove(equipe)
        elif liste == '1p5-reflexion':
            equipe = Equipe.objects.get(nom='Réflexion')
            membre.equipes.remove(equipe)
        elif liste == '1p5-technique':
            equipe = Equipe.objects.get(nom='Technique')
            membre.equipes.remove(equipe)
        elif liste == '1p5-enseignement':
            equipe = Equipe.objects.get(nom='Enseignement')
            membre.equipes.remove(equipe)
        return Response({"desabonnement": True})
    return Response(Membre.objects.none())

@api_view(['GET'])
def get_all_gdr_membres(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            all_membres = Membre.objects.filter(equipes__nom__in=["Empreinte", "Enquête", "Expérimentation", "Communication", "Technique", "Enseignement"]).distinct()
            serializer = MembreSerializer(all_membres, many=True)
            return Response(serializer.data)
    return Response(Membre.objects.none())

@api_view(['GET'])
def get_membre(request):
    if request.user.is_authenticated:
        membre = Membre.objects.filter(email=request.user)
        serializer = MembreSerializer(membre, many=True)
        if len(serializer.data) == 0:
            return Response([{"email": request.user.email}])
        else :
            return Response(serializer.data)
    return Response(Membre.objects.none())

@api_view(['POST'])
@permission_classes([])
def ask_data_access(request):
    membres = Membre.objects.filter(email=request.data['email'])
    if len(membres) == 0:
        return Response({'exists': False}, status=status.HTTP_201_CREATED)
    else:
        membres.update(askedDataAccess=True, dataAccessMsg=request.data['message'])
        return Response({'exists': True}, status=status.HTTP_201_CREATED)

@api_view(['GET'])
@permission_classes([])
def get_data_access(request):
    membres = Membre.objects.filter(askedDataAccess=1)
    serializer = MembreSerializer(membres, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def validate_access(request):
    Membre.objects.filter(email=request.data['email']).update(hasDataAccess=True)
    membre = Membre.objects.get(email=request.data['email'])
    send_mail(
            'Demande d\'accès aux données GES 1point5',
            '''Bonjour {0} {1},

Les données stockées dans GES 1point5 pour mener des travaux de recherche sont disponibles sur
le répertoire git framagit. Pour y accéder, vous devez créer un compte à partir de l'adresse
suivante : https://framagit.org/users/sign_in?redirect_to_referer=yes.
Une fois votre compte créé, envoyez nous votre identifiant à l'email suivant : {2}.
Nous vous ajouterons alors en tant que membre du répertoire git distant pour que vous puissiez y accéder.

Le contenu de la base de données (BDD) :
  - Les données des BGES soumis par les laboratoires avec GES 1point5. Plus précisément, les
données saisies nécessaires aux calculs des émissions, les données calculées par l'outil GES 1point5
et les données socio-démographiques (ou méta-données).
  - Les BGES ne contiennent aucune donnée personnelle.
  - Ces données sont des données d'activité, i.e. les activités du personnel de la recherche
et de soutien à la recherche pour réaliser les travaux de recherche et d’enseignement.
  - Les données qui sont mises à disposition ne contiennent pas le nom des laboratoires.

Pour rappel, la charte d'utilisation de la base de données GES 1point5 est disponible à l'adresse
suivante : https://labos1point5.org/terms-of-use.

à très bientôt !
l'équipe Labos 1point5'''.format(membre.prenom, membre.nom, getattr(settings, 'DEFAULT_FROM_EMAIL')),
            getattr(settings, 'DEFAULT_FROM_EMAIL'),
            [request.data['email']],
            fail_silently=False
            )
    membres = Membre.objects.filter(askedDataAccess=1)
    serializer = MembreSerializer(membres, many=True)
    return Response(serializer.data)

@api_view(['GET'])
@permission_classes([])
def is_super_user(request):
    if request.user.is_authenticated:
        user = L1P5User.objects.get(email=request.user)
        return Response({"is_super_user": user.is_superuser})
    return Response(Membre.objects.none())

@api_view(['GET'])
@permission_classes([])
def get_membres_locations(request):
    membres = Membre.objects.filter(valid=1)
    serializer = MembresLocationsSerializer(membres, many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([])
def get_membres_disciplines(request):
    membres = Membre.objects.filter(valid=1)
    all_disciplines = []
    count = []
    for m in membres:
        discipline = m.discipline.lower().capitalize()
        try:
            count[all_disciplines.index(discipline)]['value'] += 1
        except:
            all_disciplines.append(discipline)
            count.append({ 'name': discipline, 'value': 1 })
    to_return = []
    for disc in count:
        if disc['value'] > 2:
            to_return.append(disc)
    return Response(to_return)


@api_view(['POST'])
@permission_classes([])
def get_membres_equipes(request):
    equipe = Equipe.objects.get(nom=request.data['equipe'])
    if (request.data['equipe'] == "Réflexion"):
        membres = Membre.objects.filter(equipes__in=[equipe.pk])
    else:
        membres = Membre.objects.filter(equipes__in=[equipe.pk]).filter(valid=1)
    serializer = MembresEquipesSerializer(membres, many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([])
def get_user_disciplines(request):
    disciplines = Discipline.objects.all()
    serializer = DisciplinesSerializer(disciplines, many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([])
def get_genres(request):
    genres = Genre.objects.all()
    serializer = GenresSerializer(genres, many=True)
    return Response(serializer.data)


@api_view(['GET'])
@permission_classes([])
def get_statuts(request):
    statuts = Statut.objects.all()
    serializer = StatutsSerializer(statuts, many=True)
    return Response(serializer.data)
