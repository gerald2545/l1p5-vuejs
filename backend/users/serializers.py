from rest_framework import serializers
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm
from backend.users.models import L1P5User, Membre, Equipe, Discipline, Genre, Statut
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from ..api.models import Laboratoire
from ..api.serializers import TutellesSerializer
from django.conf import settings

class PasswordResetSerializer(serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """
    email = serializers.EmailField()

    def validate_email(self, value):
        # Create PasswordResetForm with the serializer
        self.reset_form = PasswordResetForm(data=self.initial_data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError(self.reset_form.errors)

        return value

    def save(self):
        request = self.context.get('request')
        # Set some values to trigger the send_email method.
        opts = {
            'use_https': request.is_secure(),
            'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),
            'request': request,
        }

        self.reset_form.save(**opts)

class L1P5UserRegistrationSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    class Meta:
        model = L1P5User
        fields = ('email', 'password1', 'password2', 'is_active')

    def validate_email(self, email):
        if email and L1P5User.objects.filter(email=email).exists():
            raise serializers.ValidationError(
                    "Un utilisateur est déjà enregistré avec cette adresse e-mail")
        return email

    def validate(self, data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError(
                "Les deux mots de passes ne sont pas identiques")
        return data

    def save(self, request):
        cleaned_data = {
            'email': self.validated_data.get('email', ''),
            'password': self.validated_data.get('password1', ''),
        }
        user = L1P5User.objects.create_user(**cleaned_data)
        # select the member and update the user info
        try:
            membre = Membre.objects.get(email=user.email)
            Membre.objects.update_or_create(pk=membre.pk, defaults={"user": user})
        except:
            pass
        return user


class L1P5UserLoginSerializer(serializers.ModelSerializer):

    class Meta:
        model = L1P5User
        fields = ('email', 'password', 'is_active')

class L1P5TokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        return data
    
class EquipesSerializer(serializers.ModelSerializer):
    
    def to_representation(self, obj):
        return obj.nom

    class Meta:
        model = Equipe
        fields = ['nom']


class DisciplinesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Discipline
        fields = ['pk', 'nom']


class GenresSerializer(serializers.ModelSerializer):

    class Meta:
        model = Genre
        fields = ['pk', 'nom']


class StatutsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Statut
        fields = ['pk', 'nom']


class MembreSerializer(serializers.ModelSerializer):

    equipes = EquipesSerializer(many=True, read_only=True)
    disciplineCNU = DisciplinesSerializer(many=False, read_only=True)
    genre = GenresSerializer(many=False, read_only=True)
    statut = StatutsSerializer(many=False, read_only=True)
    tutelles = TutellesSerializer(many=True, read_only=True)

    class Meta:
        model = Membre
        fields = ['pk', 'date', 'email', 'nom', 'prenom', 'ville', 'pays', 'percETP',
                  'discipline', 'latitude', 'longitude', 'equipes', 'disciplineCNU',
                  'genre', 'anneeNaissance', 'statut', 'laboratoire', 'tutelles',
                  'engagementAssociatif', 'engagementInstitutionel', 'engagementAutres',
                  'msgEmpreinte', 'msgEnquete', 'msgReflexion', 'msgExperimentation',
                  'msgCommunication', 'msgTechnique', 'msgEnseignement',
                  'isInAnnuaire', 'valid', 'unaccepted', 'askedDataAccess', 'dataAccessMsg',
                  'hasDataAccess']


class MembresLocationsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Membre
        fields = ['latitude', 'longitude']


class MembresEquipesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Membre
        fields = ['nom', 'prenom']
