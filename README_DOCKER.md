#Générer l'image  
docker build -t labo15wsgi:latest .  
#executer l'image obtenue avec docker build.  
docker run -it -d -v C:\Users\gsalin\Documents\l1p5-vuejs2\public:/home/labo15-au1/www:z -v C:\Users\gsalin\Documents\l1p5-vuejs2:/home/labo15-au1/priv/wsgi-scripts:z -v C:\temp\docker_labos1.5_le-pic\web\log:/home/labo15-au1/log:z -p 80:80 labo15wsgi:latest  
* Volume -v C:\Users\gsalin\Documents\l1p5-vuejs2\public:/home/labo15-au1/www : contient les fichiers statiques (css, images...)  
* Volume -v C:\Users\gsalin\Documents\l1p5-vuejs2:/home/labo15-au1/priv/wsgi-scripts:z : contient les sources de l'applications wsgi  
* Volume -v C:\temp\docker_labos1.5_le-pic\web\log:/home/labo15-au1/log:z : contient les logs access et error d'apache 

#exécuter la migration de la base de données dans le container docker  
#rentrer dans la VM  
sudo docker exec -it labo15 /bin/sh  
bash  
cd /home/labo15-au1/priv/wsgi-scripts  
pip3 install virtualenv  
virtualenv -p python3 envl1p5  
source envl1p5/bin/activate  
python manage.py migrate  
