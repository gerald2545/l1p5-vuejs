FROM debian:buster-slim

RUN apt-get update && apt-get dist-upgrade -y && DEBIAN_FRONTEND=non-interactive apt-get install -y apache2
RUN apt-get install -y libapache2-mod-wsgi-py3
RUN apt-get install -y python3-pip
RUN apt-get install -y libmariadb-dev

RUN mkdir /var/run/mysqld
COPY requirements.txt .
RUN python3 -m pip install -r requirements.txt

#Install node.js and npm
RUN apt-get install -y curl software-properties-common
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get -y install nodejs

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

EXPOSE 80

RUN a2enmod rewrite remoteip
COPY remoteip.conf /etc/apache2/conf-available
COPY labo15-au1.conf /etc/apache2/conf-available
RUN a2enconf remoteip
RUN a2enconf labo15-au1
RUN a2enmod wsgi
ENTRYPOINT /bin/sh /home/labo15-au1/priv/wsgi-scripts/docker_start.sh ; apache2ctl -D FOREGROUND
#CMD ["apache2ctl","-D", "FOREGROUND"]